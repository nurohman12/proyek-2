<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\DashController;
use App\Http\Controllers\DrafController;
use App\Http\Controllers\KembaliController;
use App\Http\Controllers\LaporanController;
use App\Http\Controllers\PengembalianController;
use App\Http\Controllers\PinjamController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\SarprasController;
use App\Http\Controllers\SarprasKeluarController;
use App\Http\Controllers\SarprasMasukController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [DashController::class, 'welcome']);
Route::get('/barang', [DashController::class, 'barang']);
Route::post('/barang/search', [DashController::class, 'barang_search']);
Route::get('/barang_fatch_data', [DashController::class, 'fetch_data']);
Route::get('/ruangan', [DashController::class, 'ruangan']);
Route::post('/ruangan/search', [DashController::class, 'ruangan_search']);
Route::get('/ruangan_fatch_data', [DashController::class, 'fetch_data_']);
Route::get('/sarpras_show/{id}', [DashController::class, 'sarpras_show']);

Route::post('/draf', [DrafController::class, 'store']);

Route::get('/persiapan', [DashController::class, 'persiapan']);

Route::get('/alur', [DashController::class, 'alur']);

Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login');
Route::post('/login', [LoginController::class, 'login'])->name('login');
Route::post('/logout', [LoginController::class, 'logout'])->name('logout');

Route::group(['middleware' => 'auth'], function () {

    Route::get('/dashboard', [DashController::class, 'index']);

    Route::get('/user', [UserController::class, 'index']);
    Route::get('/user/create', [UserController::class, 'create']);
    Route::get('/user/export', [UserController::class, 'export']);
    Route::post('/user', [UserController::class, 'store']);
    Route::get('/user/{id}', [UserController::class, 'show']);
    Route::get('/user/{id}/edit', [UserController::class, 'edit']);
    Route::put('/user/{id}', [UserController::class, 'update']);
    Route::delete('/user/{id}', [UserController::class, 'destroy']);
    Route::post('/user/import', [UserController::class, 'import']);

    Route::get('/sarpras', [SarprasController::class, 'index']);
    Route::get('/sarpras/create', [SarprasController::class, 'create']);
    Route::post('/sarpras', [SarprasController::class, 'store']);
    Route::get('/sarpras/{id}', [SarprasController::class, 'show']);
    Route::get('/sarpras/{id}/edit', [SarprasController::class, 'edit']);
    Route::put('/sarpras/{id}', [SarprasController::class, 'update']);
    Route::delete('/sarpras/{id}', [SarprasController::class, 'destroy']);

    Route::get('/sarpras_masuk', [SarprasMasukController::class, 'index']);
    Route::get('/sarpras_masuk/create', [SarprasMasukController::class, 'create']);
    Route::post('/sarpras_masuk', [SarprasMasukController::class, 'store']);
    Route::get('/sarpras_masuk/{id}', [SarprasMasukController::class, 'show']);
    Route::put('/sarpras_masuk', [SarprasMasukController::class, 'update']);
    Route::delete('/sarpras_masuk/{id}', [SarprasMasukController::class, 'destroy']);

    Route::get('/sarpras_keluar', [SarprasKeluarController::class, 'index']);
    Route::get('/sarpras_keluar/create', [SarprasKeluarController::class, 'create']);
    Route::post('/sarpras_keluar', [SarprasKeluarController::class, 'store']);
    Route::get('/sarpras_keluar/{id}', [SarprasKeluarController::class, 'show']);
    Route::put('/sarpras_keluar', [SarprasKeluarController::class, 'update']);
    Route::delete('/sarpras_keluar/{id}', [SarprasKeluarController::class, 'destroy']);

    Route::get('/pinjam', [PinjamController::class, 'index_all']);
    Route::get('/pinjam/before', [PinjamController::class, 'index_before']);
    Route::get('/pinjam/after', [PinjamController::class, 'index_after']);
    Route::get('/before_validasi', [PinjamController::class, 'before_validasi']);
    Route::get('/after_validasi', [PinjamController::class, 'after_validasi']);
    Route::get('/pinjam/create', [PinjamController::class, 'create']);
    Route::post('/pinjam', [PinjamController::class, 'store']);
    Route::get('/pinjam/{id}', [PinjamController::class, 'show']);
    Route::put('/pinjam/{id}', [PinjamController::class, 'update_status']);
    Route::get('/pinjam/{id}/edit', [PinjamController::class, 'edit']);
    Route::put('/pinjam/update/{id}', [PinjamController::class, 'update']);
    Route::delete('/pinjam/{id}', [PinjamController::class, 'destroy']);
    Route::get('/pinjam/{id}/draf', [PinjamController::class, 'draf_pinjam']);
    Route::post('/pinjam/draf', [PinjamController::class, 'add_draf_pinjam']);
    Route::post('/pinjaman/draf', [PinjamController::class, 'add_draf_pinjaman']);
    Route::put('/pinjam/{id}/draf', [PinjamController::class, 'update_draf']);
    Route::put('/peminjaman/{id}', [PinjamController::class, 'update_draf_']);
    Route::delete('/draf/{id}/pinjam', [PinjamController::class, 'destroy_draf']);
    Route::delete('/peminjamandraf/{id}', [PinjamController::class, 'destroy_draf_']);

    Route::get('/kembali', [KembaliController::class, 'index']);
    Route::post('/tanggungan', [KembaliController::class, 'store']);
    Route::get('/kembali/{id}', [KembaliController::class, 'show']);
    Route::get('/tanggungan/{id}', [KembaliController::class, 'show_tanggungan']);
    Route::get('/dikembalikan/{id}', [KembaliController::class, 'show_dikembalikan']);
    Route::post('/verifikasi', [KembaliController::class, 'verifikasi']);
    Route::post('/tanggungan/checklist', [KembaliController::class, 'checklist']);
    Route::post('/tanggungan/rusak', [KembaliController::class, 'rusak']);
    Route::delete('/tanggungan/{id}', [KembaliController::class, 'destroy']);

    Route::get('/pengembalian', [PengembalianController::class, 'index']);
    Route::delete('/pengembalian/{id}', [PengembalianController::class, 'destroy']);

    Route::get('/ketersediaan', [LaporanController::class, 'ketersediaan']);
    Route::get('/dipinjam', [LaporanController::class, 'dipinjam']);
    Route::get('/hilang', [LaporanController::class, 'hilang']);

    Route::get('/draf', [DrafController::class, 'index']);
    Route::put('/draf', [DrafController::class, 'update']);
    Route::get('/count_draf', [DrafController::class, 'count_draf']);
    Route::delete('/draf/{id}', [DrafController::class, 'destroy']);
    Route::post('/draf/print', [DrafController::class, 'print']);

    Route::get('/profile', [ProfileController::class, 'index']);
    Route::post('/profile', [ProfileController::class, 'store']);
    Route::get('/profile/{id}', [ProfileController::class, 'show']);
    Route::put('/profile/{id}/draf', [ProfileController::class, 'update_draf']);
    Route::delete('/profile/{id}/draf', [ProfileController::class, 'destroy_draf']);
    Route::get('/profile/{id}/edit', [ProfileController::class, 'edit']);
    Route::put('/profile/{id}', [ProfileController::class, 'update']);
    Route::delete('/profile/{id}', [ProfileController::class, 'destroy']);
});


// Auth::routes();

/*
|--------------------------------------------------------------------------
| Notes Bang
|--------------------------------------------------------------------------
|
| Pada tabel banyak menggunakan kondisi jika nilai 0 atau 1, hal ini tertujuan untuk mempersingkat codingan
| untuk sekarang tgl 14 November 2021 jam 18.25 belum ada kesulitan semoga tetap tidak ada :)
|
|
| Sistem register dimatikan karena menyesuaikan dengan rancangan program
| untuk mencoba melakukan register silahkan uncomment pada route Auth::routes() 
|
|
|
|   php artisan storage:link
|
*/