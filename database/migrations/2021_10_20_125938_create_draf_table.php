<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDrafTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('draf', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->integer('sarpras_id');
            $table->integer('qty');
            $table->integer('pinjam_id')->default(0);
            $table->integer('kondisi')->default(0);
            $table->text('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('draf');
    }
}
