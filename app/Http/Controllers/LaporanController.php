<?php

namespace App\Http\Controllers;

use App\Models\Kembali;
use App\Models\Pinjam;
use App\Models\Sarpras;
use App\Models\Sarpras_Keluar;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class LaporanController extends Controller
{
    public function ketersediaan()
    {
        if (!Auth::user()) {
            return view('/');
        } elseif (Auth::user()->roles == 'Mahasiswa') {
            return view('/');
        }
        $ketersediaan = Sarpras::whereNotIn('jumlah', [0])->get();

        return view('pages.laporan.ketersediaan', compact('ketersediaan'));
    }
    public function dipinjam()
    {
        if (!Auth::user()) {
            return view('/');
        } elseif (Auth::user()->roles == 'Mahasiswa') {
            return view('/');
        }
        $kembali = Pinjam::where('status', 1)->get();

        return view('pages.laporan.dipinjam', compact('kembali'));
    }
    public function hilang()
    {
        if (!Auth::user()) {
            return view('/');
        } elseif (Auth::user()->roles == 'Mahasiswa') {
            return view('/');
        }

        $hilang = Sarpras_Keluar::whereNotIn('keterangan', [''])->get();

        return view('pages.laporan.hilang', compact('hilang'));
    }
}
