<?php

namespace App\Http\Controllers;

use App\Models\Draf;
use App\Models\Kembali;
use App\Models\Pinjam;
use App\Models\Sarpras_Masuk;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PengembalianController extends Controller
{
    public function index()
    {
        $pengembalian = Kembali::where('status', 1)->orderBy('date_kembali', 'asc')->get();

        return view('pages.kembali.index2', compact('pengembalian'));
    }
    public function destroy($id)
    {
        $kembali = Kembali::where('id', $id)->first();

        $pinjam = Pinjam::where('id', $kembali->pinjam_id)->first();

        $draf = Draf::where('pinjam_id', $pinjam->id)->get();

        foreach ($draf as $data) {
            foreach ($data->sarpras_keluar as $item) {
                Draf::where('id', $data->id)
                    ->update([
                        'qty' => $item::where('draf_id', $data->id)->sum('jumlah')
                    ]);
            }
            DB::table('sarpras_masuk')->where('draf_id', $data->id)->delete();
            DB::table('sarpras_detail')->where('sarpras_id', $data->sarpras_id)->delete();
            Draf::where('id', $data->id)
                ->update([
                    'kondisi' => 0
                ]);
        }

        Kembali::where('id', $id)
            ->update([
                'status' => 0
            ]);

        Toastr::success('Berhasil menghapus pengambilan', 'Delete', ["positionClass" => "toast-top-right"]);
        return redirect('/pengembalian');
    }
}
