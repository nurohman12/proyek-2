<?php

namespace App\Http\Controllers;

use App\Models\Kembali;
use App\Models\Pinjam;
use App\Models\Sarpras;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashController extends Controller
{
    public function welcome()
    {
        if (Auth::user()) {
            if (Auth::user()->roles != 'Mahasiswa') {
                return redirect('/dashboard');
            } else {
                return view('welcome');
            }
        } else {
            return view('welcome');
        }
    }
    public function index()
    {
        if (Auth::user()->roles == 'BMN') {


            $count_tunggu = count(Pinjam::where('validasi_bmn', 0)->where('validasi_koor', 1)->where('validasi_ktu', 1)->get());

            $count_kembali = count(Kembali::all());

            $count_ruangan = count(Sarpras::where('jenis', 'Ruangan')->get());

            $count_barang = count(Sarpras::where('jenis', 'Barang')->get());

            return view('pages.dashadmin', compact('count_tunggu', 'count_kembali', 'count_ruangan', 'count_barang'));
        } elseif (Auth::user()->roles == 'KTU') {

            $pinjam = Pinjam::orderBy('tanggal_pinjam', 'asc')->get();

            $count_tunggu = count(Pinjam::where('validasi_ktu', 0)->get());

            $count_setuju = count(Pinjam::where('validasi_ktu', 1)->get());

            $count_tolak = count(Pinjam::where('validasi_ktu', 2)->get());

            return view('pages.dashktu', compact('pinjam', 'count_tunggu', 'count_setuju', 'count_tolak'));
        } elseif (Auth::user()->roles == 'Koordinator') {

            $pinjam = Pinjam::where('validasi_ktu', 1)->orderBy('tanggal_pinjam', 'asc')->get();

            $count_tunggu = count(Pinjam::where('validasi_koor', 0)->where('validasi_ktu', 1)->get());

            $count_setuju = count(Pinjam::where('validasi_koor', 1)->where('validasi_ktu', 1)->get());

            $count_tolak = count(Pinjam::where('validasi_koor', 2)->where('validasi_ktu', 1)->get());

            return view('pages.dashkoor', compact('pinjam', 'count_tunggu', 'count_setuju', 'count_tolak'));
        } elseif (Auth::user()->roles == 'Mahasiswa') {
            return view('welcome');
        }
    }
    public function barang()
    {
        if (Auth::user()) {
            if (Auth::user()->roles != 'Mahasiswa') {
                return redirect('/dashboard');
            } else {
                $barang = Sarpras::where('jenis', 'Barang')->whereNotIn('jumlah', [0])->orderBy('nama', 'asc')->paginate(6);

                return view('pages.dashboard.barang', compact('barang'));
            }
        } else {
            $barang = Sarpras::where('jenis', 'Barang')->whereNotIn('jumlah', [0])->orderBy('nama', 'asc')->paginate(6);

            return view('pages.dashboard.barang', compact('barang'));
        }
    }
    public function barang_search(Request $request)
    {
        $input = $request->input('value');

        if (Auth::user()) {
            if (Auth::user()->roles != 'Mahasiswa') {
                return redirect('/dashboard');
            } else {
                $barang = Sarpras::where('jenis', 'Barang')
                    ->where('nama', 'LIKE', '%' . $input . '%')->whereNotIn('jumlah', [0])->orderBy('nama', 'asc')->paginate(6);

                return view('pages.dashboard.cardBrg', compact('barang'));
            }
        } else {
            $barang = Sarpras::where('jenis', 'Barang')
                ->where('nama', 'LIKE', '%' . $input . '%')->whereNotIn('jumlah', [0])->orderBy('nama', 'asc')->paginate(6);

            return view('pages.dashboard.cardBrg', compact('barang'));
        }
    }
    public function fetch_data(Request $request)
    {
        if ($request->ajax()) {
            $barang = Sarpras::where('jenis', 'Barang')->whereNotIn('jumlah', [0])->orderBy('nama', 'asc')->paginate(6);

            return view('pages.dashboard.cardBrg', compact('barang'))->render();
        }
    }
    public function ruangan()
    {
        if (Auth::user()) {
            if (Auth::user()->roles != 'Mahasiswa') {
                return redirect('/dashboard');
            } else {
                $ruangan = Sarpras::where('jenis', 'Ruangan')->whereNotIn('jumlah', [0])->orderBy('nama', 'asc')->paginate(6);

                return view('pages.dashboard.ruangan', compact('ruangan'));
            }
        } else {
            $ruangan = Sarpras::where('jenis', 'Ruangan')->whereNotIn('jumlah', [0])->orderBy('nama', 'asc')->paginate(6);

            return view('pages.dashboard.ruangan', compact('ruangan'));
        }
    }
    public function ruangan_search(Request $request)
    {
        $input = $request->input('value');

        if (Auth::user()) {
            if (Auth::user()->roles != 'Mahasiswa') {
                return redirect('/dashboard');
            } else {
                $ruangan = Sarpras::where('jenis', 'Ruangan')
                    ->where('nama', 'LIKE', '%' . $input . '%')->whereNotIn('jumlah', [0])->orderBy('nama', 'asc')->paginate(6);

                return view('pages.dashboard.cardRugn', compact('ruangan'));
            }
        } else {
            $ruangan = Sarpras::where('jenis', 'Ruangan')
                ->where('nama', 'LIKE', '%' . $input . '%')->whereNotIn('jumlah', [0])->orderBy('nama', 'asc')->paginate(6);

            return view('pages.dashboard.cardRugn', compact('ruangan'));
        }
    }
    public function fetch_data_(Request $request)
    {
        if ($request->ajax()) {
            $ruangan = Sarpras::where('jenis', 'Ruangan')->whereNotIn('jumlah', [0])->orderBy('nama', 'asc')->paginate(6);

            return view('pages.dashboard.cardRugn', compact('ruangan'))->render();
        }
    }
    public function sarpras_show($id)
    {
        if (Auth::user()) {
            if (Auth::user()->roles != 'Mahasiswa') {
                return redirect('/dashboard');
            } else {
                $sarpras = Sarpras::where('id', $id)->first();

                return view('pages.dashboard.show', compact('sarpras'));
            }
        } else {
            $sarpras = Sarpras::where('id', $id)->first();

            return view('pages.dashboard.show', compact('sarpras'));
        }
    }
    public function alur()
    {
        if (Auth::user()) {
            if (Auth::user()->roles != 'Mahasiswa') {
                return redirect('/dashboard');
            } else {
                return view('pages.dashboard.alur');
            }
        } else {
            return view('pages.dashboard.alur');
        }
    }
    public function persiapan()
    {
        if (Auth::user()) {
            if (Auth::user()->roles != 'Mahasiswa') {
                return redirect('/dashboard');
            } else {
                $barang = Sarpras::where('jenis', 'Barang')->get()->take(5);

                $ruangan = Sarpras::where('jenis', 'Ruangan')->get()->take(5);

                return view('pages.dashboard.persiapan', compact('barang', 'ruangan'));
            }
        } else {
            $barang = Sarpras::where('jenis', 'Barang')->get()->take(5);

            $ruangan = Sarpras::where('jenis', 'Ruangan')->get()->take(5);

            return view('pages.dashboard.persiapan', compact('barang', 'ruangan'));
        }
    }
}
