<?php

namespace App\Http\Controllers;

use App\Models\Draf;
use App\Models\Sarpras;
use App\Models\Sarpras_Keluar;
use App\Models\SarprasDetail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\DB;

class SarprasKeluarController extends Controller
{
    public function index()
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $user = User::where('roles', 'BMN')->first();

        $sarpras_keluar = Sarpras_Keluar::where('user_id', $user->id)->get();

        return view('pages.sarpras.keluar.index', compact('sarpras_keluar'));
    }
    public function create()
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $user = User::all();

        $sarpras = Sarpras::where('jenis', 'Barang')->get();

        return view('pages.sarpras.keluar.create', compact('user', 'sarpras'));
    }
    public function store(Request $request)
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }

        $request->validate([
            'sarpras' => 'required',
            'tanggal' => 'required',
            'jumlah' => 'required'
        ]);

        $admin = User::where('roles', 'BMN')->first();

        $draf = new Draf();
        $draf->user_id = $admin->id;
        $draf->sarpras_id = $request->sarpras;
        $draf->qty = $request->jumlah;
        $draf->kondisi = 2;
        if ($request->keterangan) {
            $draf->keterangan = $request->keterangan;
        } else {
            $draf->keterangan = 'Admin mengurangi stok';
        }
        $draf->save();

        $get_max_id_draf = Draf::all()->max();

        $sarpras_keluar = new Sarpras_Keluar();
        $sarpras_keluar->sarpras_id = $request->sarpras;
        $sarpras_keluar->user_id = $admin->id;
        $sarpras_keluar->draf_id = $get_max_id_draf->id;
        $sarpras_keluar->tanggal_keluar = $request->tanggal;
        $sarpras_keluar->jumlah = $request->jumlah;
        $sarpras_keluar->save();

        $sarpras = Sarpras::where('id', $request->sarpras)->first();

        $jumlah_a = $sarpras->jumlah;
        $jumlah_b = $request->jumlah;
        $jumlah_c = $jumlah_a - $jumlah_b;

        Sarpras::where('id', $request->sarpras)
            ->update([
                'jumlah' => $jumlah_c
            ]);

        $get_max_id = Sarpras_Keluar::all()->max();

        $sarpras_detail =  new SarprasDetail();
        $sarpras_detail->sarpras_id = $request->sarpras;
        $sarpras_detail->name = $admin->name;
        $sarpras_detail->sarpras_keluar_id = $get_max_id->id;
        $sarpras_detail->save();

        Toastr::success('Berhasil menambah pengeluaran', 'Insert', ["positionClass" => "toast-top-right"]);

        return redirect('/sarpras_keluar');
    }
    public function show($id)
    {
        $sarpras_keluar = Sarpras_Keluar::where('id', $id)->first();

        return view('pages.sarpras.keluar.show', compact('sarpras_keluar'));
    }
    public function getmore(Request $request)
    {
        if ($request->ajax()) {
            $sarpras_keluar = Sarpras_Keluar::paginate(5);
        }

        return view('pages.sarpras.keluar.table', compact('sarpras_keluar'))->render();
    }
    public function update(Request $request)
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }

        $id = $request->input('id');
        $sarpras_id = $request->input('sarpras_id');
        $draf_id = $request->input('draf_id');
        $tanggal = $request->input('tanggal');
        $jumlah_b = $request->input('jumlah');
        $old_jumlah = $request->input('old_jumlah');
        $keterangan = $request->input('keterangan');

        Draf::where('id', $draf_id)
            ->update([
                'qty' => $jumlah_b,
                'keterangan' => $keterangan
            ]);

        $sarpras = Sarpras::where('id', $sarpras_id)->first();
        $jumlah_a = $sarpras->jumlah + $old_jumlah;
        $jumlah_c = $jumlah_a - $jumlah_b;

        Sarpras::where('id', $sarpras_id)
            ->update([
                'jumlah' => $jumlah_c
            ]);

        Sarpras_Keluar::where('id', $id)
            ->update([
                'tanggal_keluar' => $tanggal,
                'jumlah' => $jumlah_b
            ]);

        $sarpras_keluar = Sarpras_Keluar::paginate(5);

        Toastr::success('Berhasil mengubah pengeluaran', 'Update', ["positionClass" => "toast-top-right"]);

        return view('pages.sarpras.keluar.table', compact('sarpras_keluar'));
    }
    public function destroy($id)
    {
        $sarpras_keluar = Sarpras_Keluar::where('id', $id)->first();
        $sarpras = Sarpras::where('id', $sarpras_keluar->sarpras_id)->first();
        $jumlah = $sarpras->jumlah + $sarpras_keluar->jumlah;

        Sarpras::where('id', $sarpras_keluar->sarpras_id)
            ->update([
                'jumlah' => $jumlah
            ]);

        Draf::destroy($sarpras_keluar->draf_id);
        DB::table('sarpras_detail')->where('sarpras_keluar_id', $sarpras_keluar->id)->delete();
        Sarpras_Keluar::destroy($sarpras_keluar->id);

        $sarpras_keluar = Sarpras_Keluar::paginate(5);

        Toastr::success('Berhasil mengahapus pengeluaran', 'Delete', ["positionClass" => "toast-top-right"]);

        return redirect('/sarpras_keluar');
    }
}
