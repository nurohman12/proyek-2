<?php

namespace App\Http\Controllers;

use App\Models\Draf;
use App\Models\Pinjam;
use App\Models\Sarpras;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;


class DrafController extends Controller
{
    public function index()
    {
        if (Auth::user()->roles != 'Mahasiswa') {
            return redirect('/dashboard');
        }
        $draf = Draf::where('pinjam_id', 0)->where('user_id', Auth::user()->id)->get();

        return view('pages.draf.index', compact('draf'));
    }
    public function store(Request $request)
    {
        $sarpras_id = $request->input('sarpras_id');
        $sarpras_qty = $request->input('sarpras_qty');

        if (Auth::check()) {
            $cek = Sarpras::where('id', $sarpras_id)->first();
            if ($cek) {
                if (Draf::where('sarpras_id', $sarpras_id)->where('user_id', Auth::user()->id)->where('pinjam_id', 0)->exists()) {
                    Draf::where('sarpras_id', $sarpras_id)
                        ->where('user_id', Auth::user()->id)
                        ->where('pinjam_id', 0)
                        ->update([
                            'qty' => $sarpras_qty
                        ]);
                    return response()->json(['status' => "Berhasil mengubah jumlah " . $cek->nama . " pada draft"]);
                } else {
                    $item = new Draf();
                    $item->user_id = Auth::user()->id;
                    $item->sarpras_id = $sarpras_id;
                    $item->qty = $sarpras_qty;
                    $item->save();

                    return response()->json(['status' => $cek->nama . " ditambahkan ke draft"]);
                }
            }
        } else {
            return response()->json(['status' => "Login untuk lanjut"]);
        }
    }
    public function update(Request $request)
    {
        Draf::where('user_id', Auth::user()->id)->where('id', $request->draf_id)->where('sarpras_id', $request->sarpras_id)
            ->update([
                'qty' => $request->qty
            ]);


        Toastr::success('Jumlah sarpras di ubah', 'Update', ["positionClass" => "toast-top-right"]);

        return redirect('/draf');
    }
    public function update_(Request $request, $id)
    {
    }
    public function count_draf()
    {
        $count_draf = Draf::where('pinjam_id', 0)
            ->where('user_id', Auth::user()->id)
            ->count();

        echo json_encode($count_draf);
    }
    public function destroy($id)
    {
        Draf::destroy($id);

        Toastr::success('Berhasil menghapus draf', 'Hapus', ["positionClass" => "toast-top-right"]);

        return redirect('/draf');
    }
    public function print(Request $request)
    {
        if (Auth::user()->roles == 'Mahasiswa' | Auth::user()->roles == 'BMN') {
            $pinjam = Pinjam::where('id', $request->id)->first();

            return view('pages.draf.print', compact('pinjam'));
        }
        return abort(403);
    }
}
