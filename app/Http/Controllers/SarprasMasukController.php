<?php

namespace App\Http\Controllers;

use App\Models\Draf;
use App\Models\Sarpras;
use App\Models\Sarpras_Masuk;
use App\Models\SarprasDetail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\DB;

class SarprasMasukController extends Controller
{
    public function index()
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $user = User::where('roles', 'BMN')->first();

        $sarpras_masuk = Sarpras_Masuk::where('user_id', $user->id)->orderBy('tanggal_masuk', 'asc')->get();

        return view('pages.sarpras.masuk.index', compact('sarpras_masuk'));
    }
    public function create()
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $user = User::all();

        $sarpras = Sarpras::where('jenis', 'Barang')->get();

        return view('pages.sarpras.masuk.create', compact('user', 'sarpras'));
    }
    public function store(Request $request)
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }

        $request->validate([
            'sarpras' => 'required',
            'tanggal' => 'required',
            'jumlah' => 'required'
        ]);

        $admin = User::where('roles', 'BMN')->first();

        $draf = new Draf();
        $draf->user_id = $admin->id;
        $draf->sarpras_id = $request->sarpras;
        $draf->qty = $request->jumlah;
        $draf->kondisi = 1;
        if ($request->keterangan) {
            $draf->keterangan = $request->keterangan;
        } else {
            $draf->keterangan = 'Admin menambah stok';
        }
        $draf->save();

        $get_max_id_draf = Draf::all()->max();

        $sarpras_masuk = new Sarpras_Masuk();
        $sarpras_masuk->sarpras_id = $request->sarpras;
        $sarpras_masuk->user_id = $admin->id;
        $sarpras_masuk->draf_id = $get_max_id_draf->id;
        $sarpras_masuk->tanggal_masuk = $request->tanggal;
        $sarpras_masuk->jumlah = $request->jumlah;
        $sarpras_masuk->save();

        $sarpras = Sarpras::where('id', $request->sarpras)->first();

        $jumlah_a = $sarpras->jumlah;
        $jumlah_b = $request->jumlah;
        $jumlah_c = $jumlah_a + $jumlah_b;

        Sarpras::where('id', $request->sarpras)
            ->update([
                'jumlah' => $jumlah_c
            ]);

        $get_max_id = Sarpras_Masuk::all()->max();

        $sarpras_detail =  new SarprasDetail();
        $sarpras_detail->sarpras_id = $request->sarpras;
        $sarpras_detail->name = $admin->name;
        $sarpras_detail->sarpras_masuk_id = $get_max_id->id;
        $sarpras_detail->save();

        Toastr::success('Berhasil menambah pemasukan', 'Insert', ["positionClass" => "toast-top-right"]);

        return redirect('/sarpras_masuk');
    }
    public function show($id)
    {
        $sarpras_masuk = Sarpras_Masuk::where('id', $id)->first();

        return view('pages.sarpras.masuk.show', compact('sarpras_masuk'));
    }
    public function update(Request $request)
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }

        $id = $request->input('id');
        $sarpras_id = $request->input('sarpras_id');
        $draf_id = $request->input('draf_id');
        $tanggal = $request->input('tanggal');
        $jumlah_b = $request->input('jumlah');
        $old_jumlah = $request->input('old_jumlah');
        $keterangan = $request->input('keterangan');

        Draf::where('id', $draf_id)
            ->update([
                'qty' => $jumlah_b,
                'keterangan' => $keterangan
            ]);

        $sarpras = Sarpras::where('id', $sarpras_id)->first();
        $jumlah_a = $sarpras->jumlah - $old_jumlah;
        $jumlah_c = $jumlah_a + $jumlah_b;

        Sarpras::where('id', $sarpras_id)
            ->update([
                'jumlah' => $jumlah_c
            ]);

        Sarpras_Masuk::where('id', $id)
            ->update([
                'tanggal_masuk' => $tanggal,
                'jumlah' => $jumlah_b
            ]);

        $sarpras_masuk = Sarpras_Masuk::all();

        Toastr::success('Berhasil mengubah pemasukan', 'Update', ["positionClass" => "toast-top-right"]);

        return view('pages.sarpras.masuk.table', compact('sarpras_masuk'));
    }
    public function destroy($id)
    {
        $sarpras_masuk = Sarpras_Masuk::where('id', $id)->first();
        $sarpras = Sarpras::where('id', $sarpras_masuk->sarpras_id)->first();
        $jumlah = $sarpras->jumlah - $sarpras_masuk->jumlah;

        Sarpras::where('id', $sarpras_masuk->sarpras_id)
            ->update([
                'jumlah' => $jumlah
            ]);

        Draf::destroy($sarpras_masuk->draf_id);
        DB::table('sarpras_detail')->where('sarpras_masuk_id', $sarpras_masuk->id)->delete();
        Sarpras_Masuk::destroy($sarpras_masuk->id);

        $sarpras_masuk = Sarpras_Masuk::all();

        Toastr::success('Berhasil mengahapus pengeluaran', 'Delete', ["positionClass" => "toast-top-right"]);

        return redirect('/sarpras_masuk');
    }
}
