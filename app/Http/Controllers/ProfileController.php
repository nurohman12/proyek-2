<?php

namespace App\Http\Controllers;

use App\Models\Draf;
use App\Models\Kembali;
use App\Models\Pinjam;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Brian2694\Toastr\Facades\Toastr;

class ProfileController extends Controller
{
    public function index()
    {
        if (Auth::user()->roles != 'Mahasiswa') {
            return redirect('/dashboard');
        }
        $permohonan = Pinjam::where('user_id', Auth::user()->id)->where('status', 0)->paginate(3);

        $bawa = kembali::where('user_id', Auth::user()->id)->whereNotIn('status', [1])->paginate(3);

        $pengembalian = Kembali::where('user_id', Auth::user()->id)->where('status', 1)->paginate(3);

        return view('pages.profile.index', compact('permohonan', 'bawa', 'pengembalian'));
    }
    public function store(Request $request)
    {
        $request->validate([
            'tanggal_pinjam' => 'required',
            'keperluan' => 'required|min:10',
            'proposal' => 'required|mimes:pdf,doc,docx'
        ]);

        $pinjam = new Pinjam();
        $pinjam->user_id = Auth::user()->id;
        $pinjam->keperluan = $request->keperluan;
        $pinjam->proposal = $request->file('proposal')->store('proposal');
        $pinjam->tanggal_pinjam = $request->tanggal_pinjam;

        $pinjam->save();

        $pin = Pinjam::all('id')->max();

        Draf::where('user_id', Auth::user()->id)
            ->where('pinjam_id', 0)
            ->update([
                'pinjam_id' => $pin->id
            ]);

        Toastr::success('Berhasil permohonan pinjaman', 'Menambah', ["positionClass" => "toast-top-right"]);

        return redirect('/draf');
    }
    public function show($id)
    {
        if (Auth::user()->roles != 'Mahasiswa') {
            return redirect('/dashboard');
        }
        $pinjam = Pinjam::where('id', $id)->first();

        return view('pages.profile.show', compact('pinjam'));
    }
    public function edit($id)
    {
        if (Auth::user()->roles != 'Mahasiswa') {
            return redirect('/dashboard');
        }
        $pinjam = Pinjam::where('id', $id)->first();

        return view('pages.profile.edit', compact('pinjam'));
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'tanggal_pinjam' => 'required',
            'keperluan' => 'required|min:10',
            'proposal' => 'mimes:pdf,doc,docx'
        ]);

        if ($request->keperluan) {
            Pinjam::where('id', $id)
                ->update([
                    'tanggal_pinjam' => $request->tanggal_pinjam,
                    'keperluan' => $request->keperluan
                ]);
        }

        if ($request->file('proposal')) {
            Storage::delete($request->old_proposal);
            Pinjam::where('id', $id)
                ->update([
                    'proposal' => $request->file('proposal')->store('proposal')
                ]);
        }
        Toastr::success('Berhasil ubah permohonan', 'Update', ["positionClass" => "toast-top-right"]);

        return redirect('/profile');
    }
    public function destroy(Request $request, $id)
    {
        if (Auth::user()->roles != 'Mahasiswa') {
            return redirect('/dashboard');
        }
        $draf = Draf::where('pinjam_id', $id)->get();

        foreach ($draf as $data) {
            Draf::destroy($data->id);
        }

        Storage::delete($request->proposal);

        Pinjam::destroy($id);

        Toastr::success('Berhasil hapus permohonan', 'Hapus', ["positionClass" => "toast-top-right"]);

        return redirect('/profile');
    }
    public function update_draf(Request $request, $id)
    {
        Draf::where('user_id', Auth::user()->id)
            ->where('id', $request->draf_id)
            ->where('sarpras_id', $request->sarpras_id)
            ->where('pinjam_id', $id)
            ->update([
                'qty' => $request->qty
            ]);

        Toastr::success('Berhasil mengubah data draf', 'Update', ["positionClass" => "toast-top-right"]);

        return redirect('/profile' . '/' . $id . '/edit');
    }
    public function destroy_draf(Request $request, $id)
    {
        if (Auth::user()->roles != 'Mahasiswa') {
            return redirect('/dashboard');
        }
        Draf::destroy($id);

        return redirect('/profile' . '/' . $request->pinjam_id);
    }
}
