<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Exports\UsersExport;
use App\Imports\UsersImport;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;

class UserController extends Controller
{
    public function index()
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $user = User::orderBy('name', 'asc')->get();

        return view('pages.user.index', compact('user'));
    }
    public function create()
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        return view('pages.user.create');
    }
    public function store(Request $request)
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'nim_nidn' => ['required', 'integer', 'min:8'],
            'roles' => 'required'
        ]);

        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->nim_nidn = $request->nim_nidn;
        $user->roles = $request->roles;
        $user->status_mhs = $request->status_mhs;
        $user->jenis_kelamin = $request->jenis_kelamin;
        $user->alamat = $request->alamat;
        $user->rt = $request->rt;
        $user->rw = $request->rw;
        $user->desa = $request->desa;
        $user->kota = $request->kota;
        $user->no_telp = $request->no_telp;

        $user->save();

        return redirect('/user');
    }
    public function show($id)
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $user = User::where('id', $id)->first();

        return view('pages.user.show', compact('user'));
    }
    public function edit($id)
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $user = User::where('id', $id)->first();

        return view('pages.user.edit', compact('user'));
    }
    public function update(Request $request, $id)
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'nim_nidn' => ['required', 'integer', 'min:8'],
            'roles' => 'required'
        ]);

        User::where('id', $id)
            ->update([
                'name' => $request->name,
                'email' => $request->email,
                'nim_nidn' => $request->nim_nidn,
                'roles' => $request->roles,
                'status_mhs' => $request->status_mhs,
                'jenis_kelamin' => $request->jenis_kelamin,
                'alamat' => $request->alamat,
                'rt' => $request->rt,
                'rw' => $request->rw,
                'desa' => $request->desa,
                'kota' => $request->kota,
                'no_telp' => $request->no_telp
            ]);

        return redirect('/user');
    }
    public function destroy($id)
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        User::destroy($id);

        return redirect('/user');
    }
    public function import(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:xlsx'
        ]);
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $file = $request->file;

        Excel::import(new UsersImport, $file);

        return redirect('/user');
    }
    public function export()
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        return Excel::download(new UsersExport, 'users.xlsx');
    }
}
