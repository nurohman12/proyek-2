<?php

namespace App\Http\Controllers;

use App\Models\Kembali;
use App\Models\Sarpras;
use App\Models\SarprasDetail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SarprasController extends Controller
{
    public function index()
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $sarpras = Sarpras::all();

        return view('pages.sarpras.index', compact('sarpras'));
    }
    public function create()
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        return view('pages.sarpras.create');
    }
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'jenis' => 'required',
            'fungsi' => 'required',
            'deskripsi' => 'required',
            'photo' => 'required|image|file|max:8192'
        ]);

        $sarpras = new Sarpras();
        $sarpras->nama = $request->nama;
        $sarpras->jenis = $request->jenis;
        if ($request->jenis == 'Barang') {
            $sarpras->jumlah = 0;
        } else {
            $sarpras->jumlah = 1;
        }
        $sarpras->fungsi = $request->fungsi;
        $sarpras->deskripsi = $request->deskripsi;
        $sarpras->photo = $request->file('photo')->store('sarpras');
        $sarpras->save();

        Toastr::success('Berhasil menambah sarpras', 'Insert', ["positionClass" => "toast-top-right"]);
        return redirect('/sarpras');
    }
    public function show($id)
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $sarpras = Sarpras::where('id', $id)->first();

        $sarpras_detail = SarprasDetail::where('sarpras_id', $sarpras->id)->get();

        return view('pages.sarpras.show', compact('sarpras', 'sarpras_detail'));
    }
    public function edit($id)
    {
        $sarpras = Sarpras::where('id', $id)->first();

        return view('pages.sarpras.edit', compact('sarpras'));
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'nama' => 'required',
            'jenis' => 'required',
            'fungsi' => 'required',
            'deskripsi' => 'required',
            'photo' => 'image|file|max:8192'
        ]);
        Sarpras::where('id', $id)
            ->update([
                'nama' => $request->nama,
                'jenis' => $request->jenis,
                'fungsi' => $request->fungsi,
                'deskripsi' => $request->deskripsi
            ]);
        if ($request->file('photo')) {
            //hapus file lama di storage
            Storage::delete($request->old_photo);
            //update nama file pada storage dengan database 
            Sarpras::where('id', $id)
                ->update([
                    'photo' => $request->file('photo')->store('sarpras')
                ]);
        }
        Toastr::success('Berhasil mengubah sarpras', 'Update', ["positionClass" => "toast-top-right"]);

        return redirect('/sarpras');
    }
    public function destroy($id)
    {
        $sarpras = Sarpras::where('id', $id)->first();

        Storage::delete($sarpras->photo);

        Sarpras::destroy($id);

        DB::table('sarpras_detail')->where('sarpras_id', $id)->delete();
        DB::table('sarpras_masuk')->where('sarpras_id', $id)->delete();
        DB::table('sarpras_keluar')->where('sarpras_id', $id)->delete();

        Toastr::success('Berhasil mengubah sarpras', 'Update', ["positionClass" => "toast-top-right"]);

        return redirect('/sarpras');
    }
}
