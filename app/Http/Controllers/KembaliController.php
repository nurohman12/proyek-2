<?php

namespace App\Http\Controllers;

use App\Models\Draf;
use App\Models\Kembali;
use App\Models\Pinjam;
use App\Models\Sarpras;
use App\Models\Sarpras_Keluar;
use App\Models\Sarpras_Masuk;
use App\Models\SarprasDetail;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\DB;

class KembaliController extends Controller
{
    public function index()
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $tanggungan = Kembali::where('status', 0)->orWhere('status', 2)->orderBy('date_ambil', 'asc')->get();

        return view('pages.kembali.index', compact('tanggungan'));
    }
    public function store(Request $request)
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $pinjam_id = $request->pinjam_id;

        $kembali = Kembali::where('pinjam_id', $pinjam_id)->first();

        $draf = Draf::where('pinjam_id', $pinjam_id)->get();

        $user = User::where('id', $request->user_id)->first();
        // Cek Apakah sudah ada id pinjam di table kembali
        if ($kembali == null) {
            // Cek jika ada barang yang jumlahnya kurang menampilkan pesan Toastr Error 
            // dan langsung di redirect ke view jika ada
            foreach ($draf as $data) {

                $sarpras = Sarpras::where('id', $data->sarpras_id)->first();

                $jumlah_akhir = $sarpras->jumlah - $data->qty;

                if ($jumlah_akhir < 0) {
                    Toastr::error('Jumlah ' . $sarpras->nama . ' tidak cukup', 'Error', ["positionClass" => "toast-top-right"]);

                    return redirect('/pinjam/after');
                }
            }

            $tanggungan = new Kembali();
            $tanggungan->pinjam_id = $pinjam_id;
            $tanggungan->user_id = $request->user_id;
            $tanggungan->date_ambil = date('Y-m-d');
            $tanggungan->save();

            Pinjam::where('id', $pinjam_id)
                ->update([
                    'status' => 1
                ]);

            // Mengupdate jumlah sarpras
            foreach ($draf as $data) {

                $sarpras = Sarpras::where('id', $data->sarpras_id)->first();

                $jumlah_akhir = $sarpras->jumlah - $data->qty;

                Sarpras::where('id', $data->sarpras_id)
                    ->update([
                        'jumlah' => $jumlah_akhir
                    ]);

                $sarpras_keluar =  new Sarpras_Keluar();
                $sarpras_keluar->sarpras_id = $data->sarpras_id;
                $sarpras_keluar->user_id = $data->user_id;
                $sarpras_keluar->draf_id = $data->id;
                $sarpras_keluar->tanggal_keluar = date('Y-m-d');
                $sarpras_keluar->jumlah = $data->qty;
                $sarpras_keluar->save();

                $get_max_id = Sarpras_Keluar::all()->max();

                $sarpras_detail =  new SarprasDetail();
                $sarpras_detail->sarpras_id = $data->sarpras_id;
                $sarpras_detail->name = $user->name;
                $sarpras_detail->sarpras_keluar_id = $get_max_id->id;
                $sarpras_detail->save();
            }

            Toastr::success('Berhasil menambah pengambilan', 'Insert', ["positionClass" => "toast-top-right"]);

            return redirect('/pinjam/after');
        } else {
            Toastr::info('Data sudah di ambil', 'Info', ["positionClass" => "toast-top-right"]);

            return redirect('/pinjam/after');
        }
    }
    public function show_tanggungan($id)
    {
        $tanggungan = Kembali::where('id', $id)->first();

        return view('pages.profile.show_tanggungan', compact('tanggungan'));
    }
    public function show_dikembalikan($id)
    {
        $dikembalikan = Kembali::where('id', $id)->first();

        return view('pages.profile.show_kembali', compact('dikembalikan'));
    }
    public function verifikasi(Request $request)
    {
        $id = $request->input('draf_id');

        $kembali_id = $request->input('kembali_id');

        $keterangan = $request->input('keterangan');

        $draf = Draf::where('id', $id)->first();

        $user = User::where('id', $draf->user_id)->first();

        $sarpras_masuk = Sarpras_Masuk::where('draf_id', $id)->where('user_id', $draf->user_id)->where('sarpras_id', $draf->sarpras_id)->first();

        if ($request->sesuai && $request->tidak) {
            if ($draf->qty < $request->sesuai + $request->tidak) {
                Toastr::error('Jumlah input melebihi jumlah pinjam', 'Error', ["positionClass" => "toast-top-right"]);
                return redirect('/kembali' . '/' . $kembali_id);
            }
        }
        if ($draf->qty < $request->sesuai) {
            Toastr::error('Masukkan jumlah dengan benar', 'Error', ["positionClass" => "toast-top-right"]);
            return redirect('/kembali' . '/' . $kembali_id);
        }
        if ($sarpras_masuk == null && $draf->qty < $request->tidak) {
            Toastr::error('Masukkan jumlah dengan benar', 'Error', ["positionClass" => "toast-top-right"]);
            return redirect('/kembali' . '/' . $kembali_id);
        }
        if ($sarpras_masuk != null && $sarpras_masuk->jumlah < $request->rusak) {
            Toastr::error('Masukkan jumlah dengan benar', 'Error', ["positionClass" => "toast-top-right"]);
            return redirect('/kembali' . '/' . $kembali_id);
        }

        if ($request->sesuai < 0) {
            Toastr::error('Masukkan jumlah dengan benar', 'Error', ["positionClass" => "toast-top-right"]);
            return redirect('/kembali' . '/' . $kembali_id);
        }
        if ($request->tidak < 0) {
            Toastr::error('Masukkan jumlah dengan benar', 'Error', ["positionClass" => "toast-top-right"]);
            return redirect('/kembali' . '/' . $kembali_id);
        }
        // cek tidak rusak 
        if ($request->sesuai) {

            $sarpras_masuk = Sarpras_Masuk::where('draf_id', $id)->where('user_id', $draf->user_id)->where('sarpras_id', $draf->sarpras_id)->first();
            $sarpras_keluar = Sarpras_Keluar::where('draf_id', $id)->where('user_id', $draf->user_id)->where('sarpras_id', $draf->sarpras_id)->first();

            if ($sarpras_masuk != null) {
                if ($draf->qty == $request->sesuai) {
                    Sarpras_Masuk::where('draf_id', $id)->where('user_id', $draf->user_id)->where('sarpras_id', $draf->sarpras_id)
                        ->update([
                            'jumlah' => $sarpras_masuk->jumlah + $request->sesuai,
                            'keterangan' => null
                        ]);

                    Sarpras_Keluar::where('id', $sarpras_keluar->id)
                        ->update([
                            'rusak' => 0
                        ]);
                } else {
                    Sarpras_Masuk::where('draf_id', $id)->where('user_id', $draf->user_id)->where('sarpras_id', $draf->sarpras_id)
                        ->update([
                            'jumlah' => $sarpras_masuk->jumlah + $request->sesuai,
                            'keterangan' => 'Dikembalikan ' . $request->sesuai . ' dari ' . $draf->qty
                        ]);
                }
            } else {
                $sarpras_masuk =  new Sarpras_Masuk();
                $sarpras_masuk->sarpras_id = $draf->sarpras_id;
                $sarpras_masuk->user_id = $draf->user_id;
                $sarpras_masuk->draf_id = $draf->id;
                $sarpras_masuk->tanggal_masuk = date('Y-m-d');
                $sarpras_masuk->jumlah = $request->sesuai;
                if ($draf->qty == $request->sesuai) {
                    $sarpras_masuk->keterangan = null;
                } else {
                    $sarpras_masuk->keterangan = 'Dikembalikan ' . $request->sesuai . ' dari ' . $draf->qty;
                }
                $sarpras_masuk->save();

                $get_max_id = Sarpras_Masuk::all()->max();

                $sarpras_detail =  new SarprasDetail();
                $sarpras_detail->sarpras_id = $draf->sarpras_id;
                $sarpras_detail->name = $user->name;
                $sarpras_detail->sarpras_masuk_id = $get_max_id->id;
                $sarpras_detail->save();
            }
            $sarpras = Sarpras::where('id', $draf->sarpras_id)->first();

            $jumlah_akhir = $draf->qty + $sarpras->jumlah;

            $jumlah_qty = $draf->qty - $request->sesuai;

            Sarpras::where('id', $draf->sarpras_id)
                ->update([
                    'jumlah' => $jumlah_akhir
                ]);

            Draf::where('id', $id)
                ->update([
                    'qty' => $jumlah_qty
                ]);

            $new_draf = Draf::where('id', $id)->first();
            $sarpras_keluar = Sarpras_Keluar::where('draf_id', $id)->where('user_id', $draf->user_id)->where('sarpras_id', $draf->sarpras_id)->first();
            // return $sarpras_keluar_->rusak;
            if ($sarpras_keluar->rusak > 0) {
                Sarpras_Keluar::where('id', $sarpras_keluar->id)
                    ->update([
                        'rusak' => $sarpras_keluar->rusak - $request->sesuai
                    ]);
            }
            if ($new_draf->qty == 0) {
                Draf::where('id', $id)
                    ->update([
                        'kondisi' => 1,
                        'keterangan' => null
                    ]);

                Sarpras_Keluar::where('id', $sarpras_keluar->id)
                    ->update([
                        'rusak' => 0,
                        'keterangan' => null
                    ]);
            }
            $new_draf_ = Draf::where('id', $id)->first();

            $data_m = Draf::where('pinjam_id', $new_draf_->pinjam_id)->whereNotIn('kondisi', [1])->first();

            if (!$data_m) {
                Kembali::where('pinjam_id', $new_draf->pinjam_id)
                    ->update([
                        'date_kembali' => date('Y-m-d'),
                        'status' => 1
                    ]);
            }
        }
        // cek rusak
        if ($request->tidak) {

            $sarpras_masuk = Sarpras_Masuk::where('draf_id', $id)->where('user_id', $draf->user_id)->where('sarpras_id', $draf->sarpras_id)->first();
            $sarpras_keluar = Sarpras_Keluar::where('draf_id', $id)->where('user_id', $draf->user_id)->where('sarpras_id', $draf->sarpras_id)->first();
            $new_draf__ = Draf::where('id', $id)->first();

            if ($sarpras_masuk != null) {

                if ($sarpras_masuk != null && $sarpras_masuk->jumlah < $request->tidak && $new_draf__->qty == 0) {
                    Toastr::error('Inputan Anda terlalu banyak', 'Error', ["positionClass" => "toast-top-right"]);
                    return redirect('/kembali' . '/' . $kembali_id);
                } elseif ($sarpras_masuk != null && $sarpras_masuk->jumlah < $request->tidak) {
                    Toastr::error('Inputan Anda terlalu banyak', 'Error', ["positionClass" => "toast-top-right"]);
                    return redirect('/kembali' . '/' . $kembali_id);
                } else {
                    $keluar = Sarpras_Keluar::where('draf_id', $id)->sum('jumlah');

                    if ($new_draf__->qty + $request->tidak <= $keluar) {
                        Draf::where('id', $id)
                            ->update([
                                'qty' => $new_draf__->qty + $request->tidak,
                            ]);

                        Sarpras_Masuk::where('id', $sarpras_masuk->id)
                            ->update([
                                'jumlah' => $sarpras_masuk->jumlah - $request->tidak
                            ]);

                        $sarpras_masuk_ = Sarpras_Masuk::where('draf_id', $id)->where('user_id', $draf->user_id)->where('sarpras_id', $draf->sarpras_id)->first();

                        if ($sarpras_masuk_->jumlah == 0) {
                            Sarpras_Masuk::destroy($sarpras_masuk->id);
                            DB::table('sarpras_detail')->where('sarpras_masuk_id', $sarpras_masuk->id)->delete();
                        }
                    }
                }
            }

            $sarpras_keluar = Sarpras_Keluar::where('draf_id', $id)->where('user_id', $draf->user_id)->where('sarpras_id', $draf->sarpras_id)->first();
            if ($sarpras_keluar->jumlah < $sarpras_keluar->rusak + $request->tidak) {
                Toastr::error('Inputan Anda terlalu banyak', 'Error', ["positionClass" => "toast-top-right"]);
                return redirect('/kembali' . '/' . $kembali_id);
            }
            if ($keterangan) {
                Sarpras_Keluar::where('id', $sarpras_keluar->id)
                    ->update([
                        'rusak' => $request->tidak + $sarpras_keluar->rusak,
                        'keterangan' => $keterangan
                    ]);

                Draf::where('id', $id)
                    ->update([
                        'kondisi' => 2,
                        'keterangan' => $keterangan
                    ]);
            } else {
                if ($sarpras_keluar->keterangan == null) {
                    Sarpras_Keluar::where('id', $sarpras_keluar->id)
                        ->update([
                            'rusak' => $request->tidak + $sarpras_keluar->rusak,
                            'keterangan' => 'rusak'
                        ]);

                    Draf::where('id', $id)
                        ->update([
                            'kondisi' => 2,
                            'keterangan' => 'rusak'
                        ]);
                } else {
                    Sarpras_Keluar::where('id', $sarpras_keluar->id)
                        ->update([
                            'rusak' => $request->tidak + $sarpras_keluar->rusak
                        ]);

                    Draf::where('id', $id)
                        ->update([
                            'kondisi' => 2
                        ]);
                }
            }
            $new_draf = Draf::where('id', $id)->first();

            $data_r = Draf::where('pinjam_id', $new_draf->pinjam_id)->whereNotIn('kondisi', [2])->first();
            if ($data_r) {
                Kembali::where('pinjam_id', $new_draf->pinjam_id)
                    ->update([
                        'date_kembali' => date('Y-m-d'),
                        'status' => 2
                    ]);
            }
        }

        return redirect('/kembali' . '/' . $request->kembali_id);
    }
    public function print()
    {
        if (Auth::user()->roles != 'Mahasiswa') {
            return redirect('/dashboard');
        }
        return view('pages.kembali.print');
    }
    public function show($id)
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $tanggungan = Kembali::where('id', $id)->first();

        return view('pages.kembali.show', compact('tanggungan'));
    }
    public function destroy($id)
    {
        $kembali = Kembali::where('id', $id)->first();

        foreach ($kembali->pinjam->draf as $item) {
            $sarpras_masuk = Sarpras_Masuk::where('draf_id', $item->id)->where('user_id', $item->user_id)->where('sarpras_id', $item->sarpras_id)->first();
            if ($sarpras_masuk != null) {
                Sarpras_Masuk::destroy($sarpras_masuk->id);
                DB::table('sarpras_detail')->where('sarpras_masuk_id', $sarpras_masuk->id)->delete();
            }
            $sarpras_keluar = Sarpras_Keluar::where('draf_id', $item->id)->where('user_id', $item->user_id)->where('sarpras_id', $item->sarpras_id)->first();
            if ($sarpras_keluar != null) {
                Sarpras_Keluar::destroy($sarpras_keluar->id);
                DB::table('sarpras_detail')->where('sarpras_keluar_id', $sarpras_keluar->id)->delete();
            }
            Draf::where('id', $item->id)
                ->update([
                    'kondisi' => 0,
                    'keterangan' => null
                ]);
            $sarpras = Sarpras::where('id', $item->sarpras_id)->first();
            $jumlah_akhir = $item->qty + $sarpras->jumlah;
            Sarpras::where('id', $item->sarpras_id)
                ->update([
                    'jumlah' => $jumlah_akhir
                ]);
        }

        Pinjam::where('id', $kembali->pinjam_id)
            ->update([
                'status' => 0
            ]);

        Kembali::destroy($id);

        Toastr::success('Berhasil menghapus peminjaman', 'Delete', ["positionClass" => "toast-top-right"]);
        return redirect('/kembali');
    }
}
