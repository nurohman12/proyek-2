<?php

namespace App\Http\Controllers;

use App\Models\Draf;
use App\Models\Kembali;
use App\Models\Pinjam;
use App\Models\Sarpras;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Brian2694\Toastr\Facades\Toastr;

class PinjamController extends Controller
{
    public function index_all()
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $pinjam = Pinjam::orderBy('tanggal_pinjam', 'asc')->get();

        return view('pages.pinjam.index_all', compact('pinjam'));
    }
    public function index_before()
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $pinjam = Pinjam::where('validasi_ktu', 1)->where('validasi_koor', 1)->where('validasi_bmn', 0)->orderBy('tanggal_pinjam', 'asc')->get();;

        return view('pages.pinjam.index_before', compact('pinjam'));
    }
    public function index_after()
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $lolos = Pinjam::where('validasi_ktu', 1)->where('validasi_koor', 1)->where('validasi_bmn', 1)->orderBy('tanggal_pinjam', 'asc')->get();

        $tidak = Pinjam::where('validasi_ktu', 1)->where('validasi_koor', 1)->where('validasi_bmn', 2)->orderBy('tanggal_pinjam', 'asc')->get();

        return view('pages.pinjam.index_after', compact('lolos', 'tidak'));
    }
    public function before_validasi()
    {
        if (Auth::user()->roles == 'KTU') {

            $pinjam = Pinjam::where('validasi_ktu', 0)->orderBy('tanggal_pinjam', 'asc')->get();

            return view('pages.pinjam.before_validasi', compact('pinjam'));
        } elseif (Auth::user()->roles == 'Koordinator') {

            $pinjam = Pinjam::where('validasi_koor', 0)->orderBy('tanggal_pinjam', 'asc')->where('validasi_ktu', !0)->get();

            return view('pages.pinjam.before_validasi', compact('pinjam'));
        } else {
            return redirect('/dashboard');
        }
    }
    public function after_validasi()
    {
        if (Auth::user()->roles == 'KTU') {

            $lolos = Pinjam::where('validasi_ktu', 1)->orderBy('tanggal_pinjam', 'asc')->get();

            $tidak = Pinjam::where('validasi_ktu', 2)->orderBy('tanggal_pinjam', 'asc')->get();

            return view('pages.pinjam.after_validasi', compact('lolos', 'tidak'));
        } elseif (Auth::user()->roles == 'Koordinator') {

            $lolos = Pinjam::where('validasi_koor', 1)->orderBy('tanggal_pinjam', 'asc')->get();

            $tidak = Pinjam::where('validasi_koor', 2)->orderBy('tanggal_pinjam', 'asc')->get();

            return view('pages.pinjam.after_validasi', compact('lolos', 'tidak'));
        } else {
            return redirect('/dashboard');
        }
    }
    public function create()
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $user = User::where('roles', 'Mahasiswa')->get();

        $draf = Draf::where('user_id', 0)->where('pinjam_id', 0)->get();

        $sarpras = Sarpras::all();

        return view('pages.pinjam.create', compact('user', 'draf', 'sarpras'));
    }
    public function store(Request $request)
    {
        $request->validate([
            'pengguna' => 'required',
            'tanggal_kegiatan' => 'required',
            'keperluan' => 'required',
            'proposal' => 'required|mimes:pdf,doc,docx'
        ]);
        $draf = Draf::where('user_id', 0)->where('pinjam_id', 0)->first();
        if ($draf == null) {
            Toastr::error('Tambahkan draf terlebih dahulu', 'Error', ["positionClass" => "toast-top-right"]);
            return redirect('/pinjam/create');
        } else {
            $pinjam = new Pinjam();
            $pinjam->user_id = $request->pengguna;
            $pinjam->keperluan = $request->keperluan;
            $pinjam->proposal = $request->file('proposal')->store('proposal');
            $pinjam->tanggal_pinjam = $request->tanggal_kegiatan;

            $pinjam->save();

            $pin = Pinjam::all('id')->max();

            Draf::where('user_id', 0)
                ->where('pinjam_id', 0)
                ->update([
                    'user_id' => $request->pengguna,
                    'pinjam_id' => $pin->id
                ]);

            Toastr::success('Berhasil menambah peminjaman', 'Insert', ["positionClass" => "toast-top-right"]);
            return redirect('/pinjam');
        }
    }
    public function show($id)
    {
        if (Auth::user()->roles == 'Mahasiswa') {
            return redirect('/dashboard');
        }
        $pinjam = Pinjam::where('id', $id)->first();

        return view('pages.pinjam.show', compact('pinjam'));
    }
    public function update_status(Request $request, $id)
    {
        if ($request->validasi_ktu) {
            Pinjam::where('id', $id)
                ->update([
                    'validasi_ktu' => $request->validasi_ktu
                ]);

            Toastr::info('Berhasil validasi', 'Update', ["positionClass" => "toast-top-right"]);

            return redirect('/dashboard');
        } elseif ($request->validasi_koor) {
            Pinjam::where('id', $id)
                ->update([
                    'validasi_koor' => $request->validasi_koor
                ]);

            Toastr::info('Berhasil validasi', 'Update', ["positionClass" => "toast-top-right"]);

            return redirect('/dashboard');
        } elseif ($request->before_validasi_ktu) {
            Pinjam::where('id', $id)
                ->update([
                    'validasi_ktu' => $request->before_validasi_ktu
                ]);

            Toastr::info('Berhasil validasi', 'Update', ["positionClass" => "toast-top-right"]);

            return redirect('/before_validasi');
        } elseif ($request->before_validasi_koor) {
            Pinjam::where('id', $id)
                ->update([
                    'validasi_koor' => $request->before_validasi_koor
                ]);

            Toastr::info('Berhasil validasi', 'Update', ["positionClass" => "toast-top-right"]);

            return redirect('/before_validasi');
        } elseif ($request->after_validasi_ktu) {
            Pinjam::where('id', $id)
                ->update([
                    'validasi_ktu' => $request->after_validasi_ktu
                ]);

            Toastr::info('Berhasil validasi', 'Update', ["positionClass" => "toast-top-right"]);

            return redirect('/after_validasi');
        } elseif ($request->after_validasi_koor) {
            Pinjam::where('id', $id)
                ->update([
                    'validasi_koor' => $request->after_validasi_koor
                ]);

            Toastr::info('Berhasil validasi', 'Update', ["positionClass" => "toast-top-right"]);

            return redirect('/after_validasi');
        } elseif ($request->before_validasi_bmn) {
            Pinjam::where('id', $id)
                ->update([
                    'validasi_bmn' => $request->before_validasi_bmn
                ]);

            Toastr::info('Berhasil validasi', 'Update', ["positionClass" => "toast-top-right"]);

            return redirect('/pinjam/before');
        } elseif ($request->after_validasi_bmn) {
            Pinjam::where('id', $id)
                ->update([
                    'validasi_bmn' => $request->after_validasi_bmn
                ]);

            Toastr::info('Berhasil validasi', 'Update', ["positionClass" => "toast-top-right"]);

            return redirect('/pinjam/after');
        }
    }
    public function edit($id)
    {
        $pinjam = Pinjam::where('id', $id)->first();

        return view('pages.pinjam.edit', compact('pinjam'));
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'keperluan' => 'required',
            'tanggal_pinjam' => 'required',
            'proposal' => 'mimes:pdf,doc,docx'
        ]);

        if ($request->keperluan) {
            Pinjam::where('id', $id)
                ->update([
                    'keperluan' => $request->keperluan,
                    'tanggal_pinjam' => $request->tanggal_pinjam
                ]);
        }
        if ($request->file('proposal')) {
            Storage::delete($request->old_proposal);
            Pinjam::where('id', $id)
                ->update([
                    'proposal' => $request->file('proposal')->store('proposal')
                ]);
        }
        Toastr::success('Berhasil mengubah peminjaman', 'Update', ["positionClass" => "toast-top-right"]);
        return redirect('/pinjam');
    }
    public function destroy(Request $request, $id)
    {
        $draf = Draf::where('pinjam_id', $id)->get();

        foreach ($draf as $data) {
            Draf::destroy($data->id);
        }

        Storage::delete($request->proposal);

        Pinjam::destroy($id);

        Toastr::success('Berhasil menghapus pengambilan', 'Delete', ["positionClass" => "toast-top-right"]);

        return redirect('/pinjam');
    }
    public function draf_pinjam($id)
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $pinjam = Pinjam::where('id', $id)->first();
        $sarpras = Sarpras::all();

        return view('pages.pinjam.add_draf', compact('sarpras', 'id', 'pinjam'));
    }
    public function add_draf_pinjam(Request $request)
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $sarpras_id = $request->input('sarpras_id');
        $sarpras_qty = $request->input('sarpras_qty');
        $pinjam_id = $request->input('pinjam_id');
        $user_id = $request->input('user_id');

        $cek = Sarpras::where('id', $sarpras_id)->first();
        if ($cek) {
            if (Draf::where('sarpras_id', $sarpras_id)->where('user_id', $user_id)->where('pinjam_id', $pinjam_id)->exists()) {
                Draf::where('sarpras_id', $sarpras_id)
                    ->where('user_id', $user_id)
                    ->where('pinjam_id', $pinjam_id)
                    ->update([
                        'qty' => $sarpras_qty
                    ]);
                return response()->json(['status' => "Berhasil mengubah jumlah " . $cek->nama . " pada draft"]);
            } else {
                $item = new Draf();
                $item->user_id = $user_id;
                $item->sarpras_id = $sarpras_id;
                $item->qty = $sarpras_qty;
                $item->pinjam_id = $pinjam_id;
                $item->save();

                return response()->json(['status' => $cek->nama . " ditambahkan ke draft"]);
            }
        }
    }
    public function add_draf_pinjaman(Request $request)
    {
        if (Auth::user()->roles != 'BMN') {
            return redirect('/dashboard');
        }
        $sarpras_id = $request->input('sarpras_id');
        $sarpras_qty = $request->input('sarpras_qty');

        $cek = Sarpras::where('id', $sarpras_id)->first();
        if ($cek) {
            if (Draf::where('sarpras_id', $sarpras_id)->where('user_id', 0)->where('pinjam_id', 0)->exists()) {
                Draf::where('sarpras_id', $sarpras_id)
                    ->where('user_id', 0)
                    ->where('pinjam_id', 0)
                    ->update([
                        'qty' => $sarpras_qty
                    ]);
                return response()->json(['status' => "Berhasil mengubah jumlah " . $cek->nama . " pada draft"]);
            } else {
                $item = new Draf();
                $item->user_id = 0;
                $item->sarpras_id = $sarpras_id;
                $item->qty = $sarpras_qty;
                $item->pinjam_id = 0;
                $item->save();

                return response()->json(['status' => $cek->nama . " ditambahkan ke draft"]);
            }
        }
    }
    public function update_draf(Request $request, $id)
    {
        Draf::where('id', $request->draf_id)
            ->update([
                'qty' => $request->qty
            ]);

        Toastr::success('Berhasil mengubah jumlah', 'Update', ["positionClass" => "toast-top-right"]);

        return redirect('/pinjam' . '/' . $id . '/edit');
    }
    public function update_draf_(Request $request, $id)
    {
        Draf::where('id', $id)
            ->update([
                'qty' => $request->qty
            ]);

        Toastr::success('Berhasil mengubah jumlah', 'Update', ["positionClass" => "toast-top-right"]);
        return redirect('/pinjam/create');
    }
    public function destroy_draf(Request $request, $id)
    {
        $pinjam_id = $request->pinjam_id;

        Draf::destroy($id);

        Toastr::success('Berhasil menghapus draf', 'Delete', ["positionClass" => "toast-top-right"]);
        return redirect('/pinjam' . '/' . $pinjam_id . '/edit');
    }
    public function destroy_draf_($id)
    {
        Draf::destroy($id);

        Toastr::success('Berhasil menghapus draf', 'Delete', ["positionClass" => "toast-top-right"]);

        return redirect('/pinjam/create');
    }
}
