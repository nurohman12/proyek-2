<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'nim_nidn',
        'roles',
        'status_mhs',
        'jenis_kelamin',
        'alamat',
        'rt',
        'rw',
        'desa',
        'kota',
        'no_telp',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    public function draf()
    {
        return $this->hasMany(Draf::class);
    }
    public function pinjam()
    {
        return $this->hasMany(Pinjam::class);
    }
    public function kembali()
    {
        return $this->hasMany(Kembali::class);
    }
}
