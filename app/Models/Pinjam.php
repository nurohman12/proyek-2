<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pinjam extends Model
{
    use HasFactory;

    protected $table = 'pinjam';

    public function draf()
    {
        return $this->hasMany(Draf::class);
    }
    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function kembali()
    {
        return $this->hasOne(Kembali::class);
    }
}
