<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Draf extends Model
{
    use HasFactory;

    protected $table = 'draf';

    public function user()
    {
        return $this->belongsTo(User::class);
    }
    public function sarpras()
    {
        return $this->belongsTo(Sarpras::class);
    }
    public function pinjam()
    {
        return $this->belongsTo(Pinjam::class);
    }
    public function sarpras_detail()
    {
        return $this->belongsTo(SarprasDetail::class);
    }
    public function sarpras_keluar()
    {
        return $this->hasMany(Sarpras_Keluar::class);
    }
    public function sarpras_masuk()
    {
        return $this->hasMany(Sarpras_Masuk::class);
    }
}
