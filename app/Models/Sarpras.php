<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sarpras extends Model
{
    use HasFactory;

    protected $table = 'sarpras';

    public function draf()
    {
        return $this->hasMany(Draf::class);
    }
    public function sarpras_detail()
    {
        return $this->hasMany(SarprasDetail::class);
    }
}
