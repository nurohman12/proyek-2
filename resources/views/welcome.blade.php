@extends('layout.index')

@push('title', 'Beranda')

@section('content')
<main>
    <section>
        <div class="container wow fadeIn">
            <div id="oleezLandingCarousel" class="oleez-landing-carousel carousel slide" data-ride="carousel">
                <div class="carousel-inner" role="listbox">
                    <div class="carousel-item active">
                        <img src="{{ url('/menu/images/2.jpg') }}" alt="First slide" class="w-100">
                        <div class="carousel-caption">
                            <h2 data-animation="animated fadeInRight"><span>POLINEMA</span></h2>
                            <h2 data-animation="animated fadeInRight"><span>PSDKU Kediri</span></h2>
                            <a href="#!" class="carousel-item-link" data-animation="animated fadeInRight">PENGELOLA BMN</a>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src="{{ url('/menu/images/3.jpg') }}" alt="Second slide" class="w-100">
                        <div class="carousel-caption">
                            <h2 data-animation="animated fadeInRight"><span>POLINEMA</span></h2>
                            <h2 data-animation="animated fadeInRight"><span>PSDKU Kediri</span></h2>
                            <a href="#!" class="carousel-item-link" data-animation="animated fadeInRight">PENGELOLA BMN</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="oleez-landing-section oleez-landing-section-about">
        <div class="container">
            <div class="oleez-landing-section-content">
                <div class="oleez-landing-section-verticals wow fadeIn">
                    <p style="font-weight: bold;"><span class="number">01</span>PSDKU Kediri</p>
                </div>
                <div class="row">
                    <div class="col-xl-4 col-lg-5"></div>
                    <div class="col-xl-8 col-lg-7">
                        <style>
                            .feature-icon {
                                font-size: 50px;
                                color: #fd7e14;
                                display: inline-block;
                            }

                            [class^="ti-"],
                            [class*=" ti-"] {
                                font-family: 'themify';
                                speak-as: none;
                                font-style: normal;
                                font-variant: normal;
                                text-transform: none;
                                line-height: 1;
                                -webkit-font-smoothing: analialised;
                                -moz-osx-font-smoothing: grayscale;
                            }
                        </style>
                        <div class="row feature-blocks bg-gray justify-content-between">
                            <div class="col-sm-6 col-xl-5 mb-xl-5 mb-lg-3 mb-4 text-center text-sm-left">
                                <i class="ti-email mb-xl-4 mb-lg-3 mb-4 feature-icon"></i>
                                <h3 class="mb-xl-4 mb-lg-3 mb-4">Permohonan Akun Email</h3>
                                <p>Email resmi POLINEMA dengan alamat @polinema.ac.id yang disediakan bagi para dosen, staf, jurusan, unit kerja dan lembaga resmi kampus.</p>
                            </div>
                            <div class="col-sm-6 col-xl-5 mb-xl-5 mb-lg-3 mb-4 text-center text-sm-left">
                                <i class="ti-world mb-xl-4 mb-lg-3 mb-4 feature-icon"></i>
                                <h3 class="mb-xl-4 mb-lg-3 mb-4">Permohonan Akun Internet</h3>
                                <p>Permohonan akun internet yang disediakan bagi para dosen, staf, jurusan, unit kerja dan lembaga resmi kampus.</p>
                            </div>
                            <div class="col-sm-6 col-xl-5 mb-xl-5 mb-lg-3 mb-4 text-center text-sm-left">
                                <i class="ti-blackboard mb-xl-4 mb-lg-3 mb-4 feature-icon"></i>
                                <h3 class="mb-xl-4 mb-lg-3 mb-4">Permohonan Web Hosting</h3>
                                <p>Layanan untuk mengonlinekan website atau aplikasi web di internet bagi para dosen, staf, jurusan, dan unit kerja.</p>
                            </div>
                            <div class="col-sm-6 col-xl-5 mb-xl-5 mb-lg-3 mb-4 text-center text-sm-left">
                                <i class="ti-agenda mb-xl-4 mb-lg-3 mb-4 feature-icon"></i>
                                <h3 class="mb-xl-4 mb-lg-3 mb-4">Pengarsipan Dokumen</h3>
                                <p>Sarana yang dapat digunakan untuk pengolahan data dokumen-dokumen staff di unit kerja UPT Pusat Komputer.</p>
                            </div>
                            <div class="col-sm-12  text-center text-sm-left">
                                <i class="ti-write mb-xl-4 mb-lg-3 mb-4 feature-icon"></i>
                                <h3 class="mb-xl-4 mb-lg-3 mb-4">Komplain (Pengaduan)</h3>
                                <p>Pengaduan LMS dan Umum. Sarana yang dapat digunakan untuk menyampaikan kendala-kendala yang dialami oleh dosen, staf, dan mahasiswa terhadap gangguan pada sistem LMS dan gangguan-gangguan terhadap fasilitas yang diberikan seperti jaringan internet.</p>
                                <br><br><br>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="oleez-landing-section oleez-landing-section-testimonials">
        <div class="container">
            <div class="oleez-landing-section-content">
                <div class="oleez-landing-section-verticals wow fadeIn">
                    <p style="font-weight: bold;"><span class="number">02</span>PSDKU Kediri</p>
                </div>
                <div class="d-md-flex justify-content-between wow fadeInUp">
                    <div class="testimonial-section-content">
                        <h1 class="section-title">SIPUSKOM</h1>
                        <p class="section-subtitle">Sistem Informasi UPT Pusat Komputer (SIPUSKOM) merupakan aplikasi perangkat lunak berbasis web yang digunakan untuk melakukan penyimpanan dan pengolahan data-data Permohonan Akun Internet, Permohonan Web Hosting, dan Manajemen Komplain bagi staf atau dosen di Politeknik Negeri Malang serta digunakan untuk menyimpan dan pengelolaan Pengarsipan Dokumen untuk staf UPT Pusat Komputer.</p>
                    </div>
                </div>
            </div>
    </section>
</main>
@endsection