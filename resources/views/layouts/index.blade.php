    <!-- menampilkan header -->
    @include('layouts.header')
    <!-- menampilkan sidebar  -->
    @include('layouts.sidebar')
    <!-- menampilkan navbar -->
    @include('layouts.navbar')
    <!-- menampilkan content -->
    @yield('content')
    <!-- menampilkan footer -->
    @include('layouts.footer')

    @stack('script')