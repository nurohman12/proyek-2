<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Masukkan File Excel</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="/user/import" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="rorm-group">
                        <input type="file" name="file" class="form-control" required>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                        <button class="btn btn-primary" type="submit">Simpan</button>
                    </div>
                </form>
            </div>

        </div>
    </div>
</div>

<footer class="footer pt-0">
    <div class="row align-items-center justify-content-lg-between">
        <div class="col-lg-6">
            <div class="copyright text-center text-lg-left text-muted">
                &copy; 2019 <a href="https://www.creative-tim.com" class="font-weight-bold ml-1" target="_blank">Creative Tim</a>
            </div>
        </div>
        <div class="col-lg-6">
            <ul class="nav nav-footer justify-content-center justify-content-lg-end">
                <li class="nav-item">
                    <a href="https://www.creative-tim.com" class="nav-link" target="_blank">Creative Tim</a>
                </li>
                <li class="nav-item">
                    <a href="https://www.creative-tim.com/presentation" class="nav-link" target="_blank">About Us</a>
                </li>
                <li class="nav-item">
                    <a href="http://blog.creative-tim.com" class="nav-link" target="_blank">Blog</a>
                </li>
                <li class="nav-item">
                    <a href="https://www.creative-tim.com/license" class="nav-link" target="_blank">License</a>
                </li>
            </ul>
        </div>
    </div>
</footer>
</div>
</div>
<!-- Argon Scripts -->
<!-- Core -->
<script src="{{url('/assets/vendor/jquery/dist/jquery.min.js')}}"></script>
<script src="{{url('/assets/vendor/bootstrap/dist/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{url('/assets/vendor/js-cookie/js.cookie.js')}}"></script>
<script src="{{url('/assets/vendor/jquery.scrollbar/jquery.scrollbar.min.js')}}"></script>
<script src="{{url('/assets/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js')}}"></script>
<!-- Optional JS -->
<script src="{{url('/assets/vendor/chart.js/dist/Chart.min.js')}}"></script>
<script src="{{url('/assets/vendor/chart.js/dist/Chart.extension.js')}}"></script>
<!-- Optional JS -->
<script src="{{url('/assets/vendor/datatables.net/js/jquery.dataTables.min.js')}}"></script>
<script src="{{url('/assets/vendor/datatables.net-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
<script src="{{url('/assets/vendor/datatables.net-buttons/js/dataTables.buttons.min.js')}}"></script>
<script src="{{url('/assets/vendor/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js')}}"></script>
<script src="{{url('/assets/vendor/datatables.net-buttons/js/buttons.html5.min.js')}}"></script>
<script src="{{url('/assets/vendor/datatables.net-buttons/js/buttons.flash.min.js')}}"></script>
<script src="{{url('/assets/vendor/datatables.net-buttons/js/buttons.print.min.js')}}"></script>
<script src="{{url('/assets/vendor/datatables.net-select/js/dataTables.select.min.js')}}"></script>
<!-- Argon JS -->
<script src="{{url('/assets/js/argon.js')}}"></script>
<!-- Demo JS - remove this in your project -->
<script src="{{url('/assets/js/demo.min.js')}}"></script>
<script src="{{ url('/assets/vendor/toastr/jquery.min.js') }}"></script>
<script src="{{ url('/assets/vendor/toastr/toastr.min.js') }}"></script>
</body>

</html>