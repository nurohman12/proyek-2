<body>
    <!-- Sidenav -->
    <nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
        <div class="scrollbar-inner">
            <!-- Brand -->
            <div class="sidenav-header d-flex align-items-center">
                <a class="navbar-brand">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{url('/assets/img/logo.png')}}" alt="..." style="width: 60px;">
                        </div>
                        <div class="col-8">
                            <h3 class="heading font-weight-bold mb-0">POLINEMA</h3>
                            <small class="text-muted font-weight-bold">PSDKU Kediri</small>
                        </div>
                    </div>
                </a>
                <div class="ml-auto">
                    <!-- Sidenav toggler -->
                    <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
                        <div class="sidenav-toggler-inner">
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                            <i class="sidenav-toggler-line"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="navbar-inner">
                <!-- Collapse -->
                <div class="collapse navbar-collapse" id="sidenav-collapse-main">
                    <!-- Nav items -->
                    <ul class="navbar-nav">
                        <li class="nav-item">
                            <a class="nav-link{{ request()->is('dashboard') ? ' active' : '' }}" href="/dashboard">
                                <i class="ni ni-shop text-orange"></i>
                                <span class="nav-link-text">Dashboard</span>
                            </a>
                        </li>
                        @if(Auth::user()->roles == 'KTU' | Auth::user()->roles == 'Koordinator')
                        <li class="nav-item">
                            <a class="nav-link{{ request()->is('before_validasi') ? ' active' : '' }}" href="/before_validasi">
                                <i class="ni ni-ungroup text-orange"></i>
                                <span class="nav-link-text">Sebelum Validasi</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link{{ request()->is('after_validasi') ? ' active' : '' }}" href="/after_validasi">
                                <i class="ni ni-single-copy-04 text-orange"></i>
                                <span class="nav-link-text">Setelah Validasi</span>
                            </a>
                        </li>
                        @endif
                        @if(Auth::user()->roles == 'BMN')
                        <li class="nav-item">
                            <a class="nav-link{{ request()->is('user') ? ' active' : '' }}" href="/user">
                                <i class="ni ni-circle-08 text-orange"></i>
                                <span class="nav-link-text">Pengguna</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link collapsed" href="#navbar-sarpras" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-maps">
                                <i class="ni ni-ui-04 text-orange"></i>
                                <span class="nav-link-text">Sarpras</span>
                            </a>
                            <div class="collapse" id="navbar-sarpras">
                                <ul class="nav nav-sm flex-column">
                                    <li class="nav-item">
                                        <a href="/sarpras" class="nav-link">Sarpras</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/sarpras_masuk" class="nav-link">Sarpras Masuk</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/sarpras_keluar" class="nav-link">Sarpras Keluar</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link collapsed" href="#navbar-validasi" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-maps">
                                <i class="ni ni-archive-2 text-orange"></i>
                                <span class="nav-link-text">Validasi</span>
                            </a>
                            <div class="collapse" id="navbar-validasi">
                                <ul class="nav nav-sm flex-column">
                                    <li class="nav-item">
                                        <a href="/pinjam/before" class="nav-link">Belum Validasi</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/pinjam/after" class="nav-link">Hasil Validasi</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/pinjam" class="nav-link">Semua Validasi</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        @endif
                        @if(Auth::user()->roles == 'BMN'||'KTU'||'Koordinator')
                        <li class="nav-item">
                            <a class="nav-link collapsed" href="#navbar-laporan" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-maps">
                                <i class="ni ni-book-bookmark text-orange"></i>
                                <span class="nav-link-text">Laporan</span>
                            </a>
                            <div class="collapse" id="navbar-laporan">
                                <ul class="nav nav-sm flex-column">
                                    <li class="nav-item">
                                        <a href="/ketersediaan" class="nav-link">Ketersediaan</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/dipinjam" class="nav-link">Peminjam</a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="/hilang" class="nav-link">Rusak</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                        @endif
                    </ul>
                    <!-- Divider -->
                    @if(Auth::user()->roles == 'BMN')
                    <hr class="my-3">
                    <!-- Heading -->
                    <h6 class="navbar-heading p-0 text-muted">Data Master</h6>
                    <!-- Navigation -->
                    <ul class="navbar-nav mb-md-3">
                        <li class="nav-item">
                            <a class="nav-link{{ request()->is('kembali') ? ' active' : '' }}" href="/kembali">
                                <i class="ni ni-delivery-fast"></i>
                                <span class="nav-link-text">Peminjaman</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link{{ request()->is('pengembalian') ? ' active' : '' }}" href="/pengembalian">
                                <i class="ni ni-collection"></i>
                                <span class="nav-link-text">Pengembalian</span>
                            </a>
                        </li>
                    </ul>
                    @endif
                </div>
            </div>
        </div>
    </nav>