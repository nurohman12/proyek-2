<footer class="oleez-footer wow fadeInUp">
    <div class="container">
        <!-- <div class="footer-content">
            <div class="row">
                <div class="col-md-6">
                    <img src="{{ url('/menu/images/LOGO-POLINEMA-word-transparent.png') }}" alt="Polinema" class="footer-logo">
                    <p class="footer-intro-text">Jangan malu, hubungi kami!</p>
                    <nav class="footer-social-links">
                        <a href="#!">Facebook</a>
                        <a href="#!">Twitter</a>
                        <a href="#!">Instagram</a>
                        <a href="#!">Youtube</a>
                    </nav>
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-6 footer-widget-text">
                            <h6 class="widget-title">PHONE</h6>
                            <p class="widget-content">0896-0900-4444</p>
                        </div>
                        <div class="col-md-6 footer-widget-text">
                            <h6 class="widget-title">Email</h6>
                            <p class="widget-content">psdkukediri@polinema.ac.id</p>
                        </div>
                        <div class="col-md-6 footer-widget-text">
                            <h6 class="widget-title">ADDRESS</h6>
                            <p class="widget-content">Kampus 1 :<br> Jl. Mayor Bismo No. 27, Semampir, Kota Kediri, 64121</p>
                            <p class="widget-content">Kampus 2 :<br> Jl. Lingkar Maskumambang, No. 1, Sukorame, Mojoroto, Kota Kediri, 64114</p>
                        </div>
                        <div class="col-md-6 footer-widget-text">
                            <h6 class="widget-title">WORK HOURS</h6>
                            <p class="widget-content">Weekdays: 07:00 - 15:00 <br> Weekends: 07:00 - 12:00</p>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="footer-text">
            <p class="mb-md-0">Copyright 2021 © Theme By <a href="https://www.bootstrapdash.com" target="_blank" rel="noopener noreferrer" class="text-reset">Pengelola BMN</a>.</p>
            <p class="mb-0">All right reserved.</p>
        </div>
    </div>
</footer>
<script src="{{ url('/menu/vendors/popper.js/popper.min.js') }}">
</script>
<script src="{{ url('/menu/vendors/wowjs/wow.min.js') }}"></script>
<script src="{{ url('/menu/vendors/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ url('/menu/vendors/slick-carousel/slick.min.js') }}"></script>
<script src="{{ url('/menu/js/main.js') }}"></script>
<script src="{{ url('/menu/js/landing.js') }}"></script>
<script src="{{ url('/assets/plugins/jquery.min.js')}}"></script>
<script src="{{ url('/assets/plugins/toastr.min.js')}}"></script>
<script>
    new WOW({
        mobile: false
    }).init();
</script>
</body>


</html>