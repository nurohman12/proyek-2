<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>PSDKU Kediri :: @stack('title')</title>
    <link rel="stylesheet" href="{{ url('/menu/vendors/animate.css/animate.min.css') }}">
    <link rel="stylesheet" href="{{ url('/menu/vendors/slick-carousel/slick.css') }}">
    <link rel="stylesheet" href="{{ url('/menu/vendors/slick-carousel/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ url('/menu/css/style.css') }}">
    <link rel="stylesheet" href="{{ url('/menu/css/themify-icons/themify-icons.css') }}">
    <script src="{{ url('/menu/vendors/jquery/jquery.min.js') }}"></script>
    <script src="{{ url('/menu/js/loader.js') }}"></script>
    @stack('style')
    <link rel="stylesheet" href="{{ url('/assets/plugins/toastr.min.css')}}">
</head>