<body>
    <div class="oleez-loader"></div>
    <header class="oleez-header">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="/"><img style="margin-top:-8px; margin-left:100px; width: 80px;" src="{{ url('/menu/images/logo.png') }}" alt="Polinema PSDKU Kediri">Polinema</a>
            <ul class="nav nav-actions d-lg-none ml-auto">
                @auth
                <li class="nav-item nav-item-cart">
                    <a class="nav-link" href="/draf">
                        <span class="cart-item-count">
                            <p style="font-size: 12px;" class="count_draf"></p>
                        </span>
                        <img src="{{ url('/menu/images/hard-drive.svg') }}" alt="cart">
                    </a>
                </li>
                @endauth
            </ul>
            <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#oleezMainNav" aria-controls="oleezMainNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="oleezMainNav">
                <ul class="navbar-nav mx-auto mt-2 mt-lg-0">
                    <li class="nav-item {{ request()->is('/') ? ' active' : '' }}">
                        <a class="nav-link" href="/">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#!" id="pagesDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Peminjaman</a>
                        <div class="dropdown-menu" aria-labelledby="pagesDropdown">
                            <a class="dropdown-item {{ request()->is('/barang') ? ' active' : '' }}" href="/barang">Barang</a>
                            <a class="dropdown-item {{ request()->is('/ruangan') ? ' active' : '' }}" href="/ruangan">Ruangan</a>
                            <!-- <a class="dropdown-item" href="404.html">404</a> -->
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#!" id="blogDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">About</a>
                        <div class="dropdown-menu" aria-labelledby="blogDropdown">
                            <a class="dropdown-item {{ request()->is('/persiapan') ? ' active' : '' }}" href="/persiapan">Periapan Peminjaman</a>
                            <a class="dropdown-item {{ request()->is('/alur') ? ' active' : '' }}" href="/alur">Alur</a>
                        </div>
                    </li>
                </ul>
                <ul class="navbar-nav d-none d-lg-flex">
                    @auth
                    <li class="nav-item nav-item-cart">
                        <a class="nav-link nav-link-btn" href="/draf">
                            <span class="cart-item-count">
                                <p style="font-size: 12px;" class="count_draf"></p>
                            </span>
                            <img src="{{ url('/menu/images/hard-drive.svg') }}" alt="cart">
                        </a>
                    </li>
                    @endauth

                    @auth
                    @if(Auth::user()->roles == 'Mahasiswa')
                    <li style="margin-right: 75px;" class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle " href="#!" id="languageDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i style="margin-top: 2px; font-size: 21px; color: black;" class="ti-user feature-icon"></i> {{Auth::user()->name}}</a>
                        <div class="dropdown-menu" aria-labelledby="languageDropdown">
                            <a class="dropdown-item" href="/profile">My Profile</a>
                            <a class="dropdown-item" href="#!">Edit Profile</a>
                            <a class="dropdown-item" href="#!">Change Password</a>
                            <a class="dropdown-item" href="{{route('logout')}}" onclick=" event.preventDefault(); document.getElementById('formLogout').submit();">
                                <i class="mdi mdi-logout me-1 ms-1"></i>
                                Log out
                                <form id="formLogout" action="{{ route('logout') }}" method="post">
                                    @csrf
                                </form>
                            </a>
                        </div>
                    </li>
                </ul>
                @else
                <li style="margin-right: 75px;" class="nav-item">
                    <a class=" nav-link dropdown-toggle " href="/dashboard">
                        <i style="font-size: 20px; color: black;" class="ti-home feature-icon"></i> Dashboard
                    </a>
                </li>
                </ul>
                @endif
                @else
                <li style="margin-right: 75px;" class="nav-item">
                    <a class=" nav-link dropdown-toggle " href="/login">
                        <i style="font-size: 20px; color: black;" class="ti-lock feature-icon"></i> Login
                    </a>
                </li>
                </ul>
                @endauth
            </div>
        </nav>
    </header>

    @push('header')
    <script>
        function totalDraf() {
            $.ajax({
                type: 'GET',
                url: 'count_draf',
                success: function(response) {
                    var response = JSON.parse(response);
                    $('.count_draf').text(response);
                }
            })
        }
        $(document).ready(function() {
            $.ajax({
                type: 'GET',
                url: 'count_draf',
                success: function(response) {
                    var response = JSON.parse(response);
                    $('.count_draf').text(response);
                }
            })
        })
    </script>
    @endpush