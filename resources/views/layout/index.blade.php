@include('layout.header')

@include('layout.navbar')

@yield('content')

@include('layout.footer')

@stack('header')

@stack('script')