@extends('layouts.index')

@push('title', 'Dash | Polinema PSDKU Kediri')

@section('content')
<div class="container-fluid page-body-wrapper">
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row">
                <div class="col">
                    <h3 class="text-dark font-weight-bold mb-2">Hi, welcome back! {{Auth::user()->name}}</h3>
                    <h6 class="font-weight-normal mb-2">Last login was 23 hours ago. View details</h6>
                </div>
                <div class="col-auto">
                    <div class="form-group shadow">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Recipient's username" aria-label="Recipient's username">
                            <div class="input-group-append">
                                <button class="btn btn-sm btn-primary" type="button">Search</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-3 d-flex grid-margin stretch-card">
                    <div class="card shadow">
                        <div class="innerr">
                            <img src="{{ url('/menu/images/Standard_list_blog/Standard_9@2x.jpg') }}" class="card-img-top" alt="width: 20ps;">
                        </div>
                        <div class="card-body text-center">
                            <h5 class="card-title">Proyektor</h5>
                            <p class="card-text">Jumlah 10</p>
                            <a href="!#" class="btn btn-primary">Add Draf</a>
                            <a href="!#" class="btn btn-secondary">Detail</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 d-flex grid-margin stretch-card">
                    <div class="card shadow">
                        <div class="inner">
                            <img src="{{ url('/menu/images/Standard_list_blog/Standard_8@2x.jpg') }}" class="card-img-top" alt="" style="width: 20ps;">
                        </div>
                        <div class="card-body text-center">
                            <h5 class="card-title">Proyektor</h5>
                            <p class="card-text">Jumlah 10</p>
                            <a href="!#" class="btn btn-primary">Add Draf</a>
                            <a href="!#" class="btn btn-secondary">Detail</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 d-flex grid-margin stretch-card">
                    <div class="card shadow">
                        <div class="inner">
                            <img src="{{ url('/menu/images/Standard_list_blog/Standard_7@2x.jpg') }}" class="card-img-top" alt="" style="width: 20ps;">
                        </div>
                        <div class="card-body text-center">
                            <h5 class="card-title">Proyektor</h5>
                            <p class="card-text">Jumlah 10</p>
                            <a href="!#" class="btn btn-primary">Add Draf</a>
                            <a href="!#" class="btn btn-secondary">Detail</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 d-flex grid-margin stretch-card">
                    <div class="card shadow">
                        <div class="inner">
                            <img src="{{ url('/menu/images/Standard_list_blog/Standard_6@2x.jpg') }}" class="card-img-top" alt="" style="width: 20ps;">
                        </div>
                        <div class="card-body text-center">
                            <h5 class="card-title">Proyektor</h5>
                            <p class="card-text">Jumlah 10</p>
                            <a href="!#" class="btn btn-primary">Add Draf</a>
                            <a href="!#" class="btn btn-secondary">Detail</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 d-flex grid-margin stretch-card">
                    <div class="card shadow">
                        <div class="inner">
                            <img src="{{ url('/menu/images/Standard_list_blog/Standard_5@2x.jpg') }}" class="card-img-top" alt="" style="width: 20ps;">
                        </div>
                        <div class="card-body text-center">
                            <h5 class="card-title">Proyektor</h5>
                            <p class="card-text">Jumlah 10</p>
                            <a href="!#" class="btn btn-primary">Add Draf</a>
                            <a href="!#" class="btn btn-secondary">Detail</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection