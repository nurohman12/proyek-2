@include('layouts.header')
<div class="container-fluid page-body-wrapper">
    <div class="main-panel">
        <div class="content-wrapper">
            <div class="row justify-content-center">
                <div class="col-md-11">
                    <style>
                        .card {
                            position: relative;
                            display: block;
                        }

                        .card::after {
                            content: '';
                            background: url(../../menu/images/logo.png);
                            opacity: 0.1;
                            position: absolute;
                            top: 0;
                            right: 0;
                            bottom: 0;
                            left: 0;
                            background-size: cover;
                        }
                    </style>
                    <div class="card">
                        <div class="card-body shadow">
                            <div class="row">
                                <div class="col-md-12">
                                    <h2 class="page-header">
                                        <img src="{{ url('/menu/images/logo.png') }}" style="width: 80px;" alt="">
                                        <p style="color:black;font-size:30px; font-weight: 500; display: inline;">Polinema</p>
                                        <small class="float-right">{{ date("d/m/Y") }}</small>
                                    </h2>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 invoice-col">
                                    Dari
                                    <address>
                                        <strong>BMN PSDKU Kediri</strong><br>
                                        Jl. Lingkar Maskumambang No.1, Sukorame <br>
                                        Kec. Mojoroto, Kota Kediri <br>
                                        Jawa Timur 64119<br>
                                        Telepon: (0354) 683128<br>
                                        Email: psdkukediri@polinema.ac.id
                                    </address>
                                </div>
                                <div class="col-sm-4 invoice-col">
                                    Ke
                                    <address>
                                        <strong>{{ $pinjam->user->name }}</strong><br>
                                        {{ $pinjam->user->nim_nidn }}<br>
                                        {{ $pinjam->user->alamat }}<br>
                                        Rt: {{ $pinjam->user->rt }} Rw: {{ $pinjam->user->rw }}, {{ $pinjam->user->desa }}<br>
                                        {{ $pinjam->user->kota }}, Jawa Timur 64172<br>
                                        Telepon: {{ $pinjam->user->no_telp }}<br>
                                        Email: {{ $pinjam->user->email }}<br>
                                    </address>
                                </div>
                                <div class="col-sm-4 invoice-col">
                                    <b>Pinjam ID:</b> {{ $pinjam->id }}<br>
                                    <br>
                                    <b>Tanggal Kegiatan:</b> {{date('d F Y', strtotime($pinjam->tanggal_pinjam))}}<br>
                                    <b>Keperluan:</b> {{ $pinjam->keperluan }}
                                </div>
                            </div>
                            <div class="row">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered">
                                        <thead>
                                            <tr style="color: black;">
                                                <th>#</th>
                                                <th>Jenis</th>
                                                <th>Nama Sarpras</th>
                                                <th>Jumlah</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($pinjam->draf as $data)
                                            <tr>
                                                <th>{{ $loop->iteration }}</th>
                                                <td>{{ $data->sarpras->jenis }}</td>
                                                <td>{{ $data->sarpras->nama }}</td>
                                                <td>{{ $data->qty }}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="{{ url('/assets/vendors/base/vendor.bundle.base.js') }}"></script>
<!-- endinject -->
<!-- inject:js -->
<script src="{{ url('/assets/js/template.js') }}"></script>
<!-- endinject -->
<script>
    window.addEventListener("load", window.print());
</script>
</body>