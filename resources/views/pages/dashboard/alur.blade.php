@extends('layout.index')

@push('title', 'Alur')

@section('content')
<main class="portfolio-list">
    <div class="container">
        <h1 class="oleez-page-title wow fadeInUp">Alur</h1>
        <article class="project">
            <div class="row">
                <div class="col-md-4 mb-5 md-mb-0 project-content wow fadeInLeft">
                    <a href="#!" class="project-link">Langkah pertama</a>
                    <h2 class="project-title">Permohonan</h2>
                    <p class="project-content">Melakukan permohonan pada sebuah barang / ruangan melalui website dan mengupload beberapa data seprti proposal.</p>
                </div>
                <div class="col-md-7 mb-5">
                    <div class="project-thumbnail-wrapper wow fadeInRight">
                        <img src="{{ url('/menu/images/langkah1.jpg') }}" alt="project">
                    </div>
                </div>
            </div>
        </article>
        <article class="project">
            <div class="row">
                <div class="col-md-4 mb-5 md-mb-0 project-content wow fadeInLeft">
                    <a href="#!" class="project-link">Langkah kedua</a>
                    <h2 class="project-title">Seleksi</h2>
                    <p class="project-content">Data permohonan akan diseleksi oleh pihak pihak yang terkait, di setujui atau tidak.</p>
                </div>
                <div class="col-md-7 wow fadeInRight">
                    <div class="project-thumbnail-wrapper">
                        <img src="{{ url('/menu/images/langkah2.jpg') }}" alt="project">
                    </div>
                </div>
            </div>
        </article>
        <article class="project">
            <div class="row">
                <div class="col-md-4 mb-5 md-mb-0 project-content wow fadeInLeft">
                    <a href="#!" class="project-link">langkah ketiga</a>
                    <h2 class="project-title">Hasil</h2>
                    <p class="project-content">Melihat hasil seleksi di profile kamu jika di setujui silahkan mengambil barang ke BMN.</p>
                </div>
                <div class="col-md-7 wow fadeInRight">
                    <div class="project-thumbnail-wrapper">
                        <img src="{{ url('/menu/images/langkah3.jpg') }}" alt="project">
                    </div>
                </div>
            </div>
        </article>
        <article class="project">
            <div class="row">
                <div class="col-md-4 mb-5 md-mb-0 project-content wow fadeInLeft">
                    <a href="#!" class="project-link">langkah keempat</a>
                    <h2 class="project-title">Pengembalian</h2>
                    <p class="project-content">Cetak draf pinjaman barang / ruangan pada menu profile kamu, pilih tombol cetak pada barang / ruangan yang ingin kamu kembalikan</p>
                </div>
                <div class="col-md-7 wow fadeInRight">
                    <div class="project-thumbnail-wrapper">
                        <img src="{{ url('/menu/images/langkah4.jpg') }}" alt="project">
                    </div>
                </div>
            </div>
        </article>
        <article class="project">
            <div class="row">
                <div class="col-md-4 mb-5 md-mb-0 project-content wow fadeInLeft">
                    <a href="#!" class="project-link">langkah kelima</a>
                    <h2 class="project-title">Cek</h2>
                    <p class="project-content">Saat pengembalian barang yang kamu kembalikan akan di data oleh BMN, akan di cek oleh BMN bagaimana kondisi barang, jika rusak kamu diharuskan ganti rugi.</p>
                </div>
                <div class="col-md-7 wow fadeInRight">
                    <div class="project-thumbnail-wrapper">
                        <img src="{{ url('/menu/images/langkah5.jpg') }}" alt="project">
                    </div>
                </div>
            </div>
        </article>
    </div>
</main>
@endsection