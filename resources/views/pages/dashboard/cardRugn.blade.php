<div class="row">
    @foreach($ruangan as $data)
    <div class="col-md-4 sarpras_data portfolio-card wow fadeInUp">
        <div class="project-thumbnail-wrapper">
            <img src="{{ url('/storage/'. $data->photo) }}" alt="portffolio" class="project-thumbnail">
        </div>
        <h5 class="project-name">{{ $data->nama }}</h5>
        <p class="project-category">Jumlah {{ $data->jumlah }}</p>
        <div class="btn-wrapper text-center">
            <input type="hidden" value="{{$data->id}}" class="sarpras_id">
            <input type="hidden" value="1" class="qty-input">
            <a class="btn btn-primary addToDraf mr-2">Add to Draf</a>
            <a href="/sarpras_show/{{$data->id}}" class="btn btn-secondary">Detail</a>
        </div>
    </div>
    @endforeach
</div>
{!! $ruangan->links() !!}

<script>
    $(document).ready(function() {
        $('.addToDraf').click(function(e) {
            e.preventDefault();
            var sarpras_id = $(this).closest('.sarpras_data').find('.sarpras_id').val();
            var sarpras_qty = $(this).closest('.sarpras_data').find('.qty-input').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: "POST",
                url: "/draf",
                data: {
                    'sarpras_id': sarpras_id,
                    'sarpras_qty': sarpras_qty,
                },
                success: function(response) {
                    alert(response.status);
                    totalDraf();
                }
            });
        });
    });
</script>