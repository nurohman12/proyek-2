<div class="row">
    @foreach($barang as $data)
    <div class="col-md-4 portfolio-card wow fadeInUp">
        <div class="project-thumbnail-wrapper">
            <img src="{{ url('/storage/'. $data->photo) }}" alt="portffolio" class="project-thumbnail">
        </div>
        <h5 class="project-name">{{ $data->nama }}</h5>
        <p class="project-category">Jumlah {{ $data->jumlah }}</p>
        <div class="btn-wrapper text-center sarpras_data">
            <div class="input-group mb-3 mt-3 " style="margin-left: 110px; width: 130px;">
                <div class="input-group quantity">
                    <div class="input-group-prepend decrement-btn" style="cursor: pointer">
                        <span class="input-group-text">-</span>
                    </div>
                    <input type="hidden" value="{{$data->id}}" class="sarpras_id">
                    <input type="hidden" value="{{$data->jumlah}}" class="qty-max">
                    <input type="text" readonly class="qty-input form-control" value="1">
                    <div class="input-group-append increment-btn" style="cursor: pointer">
                        <span class="input-group-text">+</span>
                    </div>
                </div>
            </div>
            <a class="btn btn-primary addToDraf mr-2">Add to Draf</a>
            <a href="/sarpras_show/{{$data->id}}" class="btn btn-secondary">Detail</a>
        </div>
    </div>
    @endforeach
</div>
{!! $barang->links() !!}
<script>
    $(document).ready(function() {
        $('.addToDraf').click(function(e) {
            e.preventDefault();
            var sarpras_id = $(this).closest('.sarpras_data').find('.sarpras_id').val();
            var sarpras_qty = $(this).closest('.sarpras_data').find('.qty-input').val();

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $.ajax({
                method: "POST",
                url: "/draf",
                data: {
                    'sarpras_id': sarpras_id,
                    'sarpras_qty': sarpras_qty,
                },
                success: function(response) {
                    alert(response.status);
                    totalDraf();
                }
            });
        });

        $('.increment-btn').click(function(e) {
            e.preventDefault();
            var incre_value = $(this).parents('.quantity').find('.qty-input').val();
            var max_value = $(this).parents('.quantity').find('.qty-max').val();
            var value = parseInt(incre_value, max_value);
            value = isNaN(value) ? 0 : value;
            if (value < max_value) {
                value += 1;
                $(this).parents('.quantity').find('.qty-input').val(value);
            }

        });

        $('.decrement-btn').click(function(e) {
            e.preventDefault();
            var decre_value = $(this).parents('.quantity').find('.qty-input').val();
            var max_value = $(this).parents('.quantity').find('.qty-max').val();
            var value = parseInt(decre_value, max_value);
            value = isNaN(value) ? 0 : value;
            if (value > 1) {
                value--;
                $(this).parents('.quantity').find('.qty-input').val(value);
            }
        });
    });
</script>