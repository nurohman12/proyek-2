@extends('layout.index')

@push('title', 'Detail')

@section('content')
<main class="blog-post-single">
    <div class="container">
        <h1 class="post-title wow fadeInUp">Detail {{ $sarpras->jenis }}</h1>
        <div class="row">
            <div class="col-md-8 blog-post-wrapper">
                <div class="post-header wow fadeInUp">
                    <img src="{{ url('/storage/'.$sarpras->photo) }}" alt="blog post" class="post-featured-image">
                </div>
                <div class="post-content wow fadeInUp">
                    <h4>{{ $sarpras->nama }}</h4>
                    <p>{{ $sarpras->deskripsi }}</p>
                    <blockquote class="blockquote wow fadeInUp">
                        <p>{{ $sarpras->fungsi }}</p>
                    </blockquote>
                </div>
                <div class="comment-section wow fadeInUp">
                    <h5 class="section-title">Tinggalkan Komentar</h5>
                    <form action="POST" class="oleez-comment-form">
                        <div class="row">
                            <div class="form-group col-md-6">
                                <input type="text" class="oleez-input" id="fullName" name="fullName" required>
                                <label for="fullName">*Full name</label>
                            </div>
                            <div class="form-group col-md-6">
                                <input type="email" class="oleez-input" id="fullName" name="email" required>
                                <label for="email">*Email</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-12">
                                <label for="message">*Message</label>
                                <textarea name="message" id="message" rows="10" class="oleez-textarea" required></textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-12">
                                <button type="submit" class="btn btn-submit">Send</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-4">
                <div class="sidebar-widget wow fadeInUp">
                    <h5 class="widget-title">Share</h5>
                    <div class="widget-content">
                        <nav class="social-links">
                            <a href="#!">Fb</a>
                            <a href="#!">Tw</a>
                            <a href="#!">In</a>
                            <a href="#!">Be</a>
                        </nav>
                    </div>
                </div>
                <div class="sidebar-widget wow fadeInUp">
                    <h5 class="widget-title">Kategori</h5>
                    <div class="widget-content">
                        <a href="#!" class="post-tag">Barang</a>
                        <a href="#!" class="post-tag">Ruangan</a>
                        <a href="#!" class="post-tag">Persiapan</a>
                        <a href="#!" class="post-tag">Alur</a>
                        <a href="#!" class="post-tag">Team</a>
                        <a href="#!" class="post-tag">Project</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection