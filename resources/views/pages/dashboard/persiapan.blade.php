@extends('layout.index')

@push('title', 'Persiapan')

@section('content')
<main class="about-page">
    <div class="container">
        <h1 class="oleez-page-title wow fadeInUp">Persiapan Peminjaman</h1>
        <p class="oleez-page-header-content wow fadeInUp">Hal-hal yang perlu kamu siapkan sebelum melakukan peminjaman seperti berikut:</p>
        <img src="{{ url('/menu/images/prepare.jpg') }}" alt="about" class="w-100 wow fadeInUp">
        <section class="oleez-about-features">
            <div class="row">
                <div class="col-md-4 mb-5 mb-md-0 feature-card wow fadeInUp">
                    <h5 class="feature-card-title">Proposal</h4>
                        <p class="feature-card-content">Ya, tentunya ketika kamu ingin meminjam sebuah barang / ruangan buatlah sebuah proposal yang didalamnya untuk apa melakukan peminjaman</p>
                </div>
                <div class="col-md-4 mb-5 mb-md-0 feature-card wow fadeInUp">
                    <h5 class="feature-card-title">Login</h4>
                        <p class="feature-card-content">Untuk melakukan peminjaman kamu diharuskan untuk melakukan login terlebih dahulu, dan pastikan email maupun password yang di masukkan valid</p>
                </div>
                <div class="col-md-4 mb-5 mb-md-0 feature-card wow fadeInUp">
                    <h5 class="feature-card-title">Pinjam</h4>
                        <p class="feature-card-content">Menentukan barang / ruangan apa saja yang ingin di pinjam agar tidak melakukan permohonan peminjaman dua kali atau lebih</p>
                </div>
            </div>
        </section>
        <section class="oleez-what-we-do">
            <div class="row">
                <div class="col-lg-10 offset-lg-1">
                    <h2 class="section-title wow fadeInUp">Apa yang bisa kamu pinjam?</h2>
                    <h4 class="section-subtitle wow fadeInUp">Kami memberikan beberapa barang / ruangan yang bisa di pinjam seperti berikut:</h4>
                    <div class="row">
                        <div class="col-md-4 mb-5 mb-md-0 wow fadeInUp">
                            <h5 class="what-we-do-list-title" style="cursor: pointer;" onclick="window.location.href='/barang'">Barang</h5>
                            <ul class="what-we-do-list">
                                @foreach($barang as $data)
                                <li style="cursor: pointer;" onclick="window.location.href='/sarpras_show/{{$data->id}}'">{{ $data->nama }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <div class="col-md-4 mb-5 mb-md-0 wow fadeInUp">
                            <h5 class="what-we-do-list-title" style="cursor: pointer;" onclick="window.location.href='/ruangan'">Ruangan</h5>
                            <ul class="what-we-do-list">
                                @foreach($ruangan as $data)
                                <li style="cursor: pointer;" onclick="window.location.href='/sarpras_show/{{$data->id}}'">{{ $data->nama }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
@endsection