@extends('layout.index')

@push('title', 'Ruangan')

@section('content')
<main class="portfolio-grid-page">
    <div class="container">
        <div class="row mt-2 mb-3">
            <div class="col">
                <h4 class="card-title">
                    Daftar Ruangan »
                </h4>
            </div>
            <div class="col-auto">
                <input class="form-control" type="text" placeholder="Search" id="find" onkeyup="find()">
            </div>
        </div>
        <div id="content">
            @include('pages.dashboard.cardRugn')
        </div>
    </div>
</main>
@endsection

@push('script')
<script>
    function find() {
        const CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        const value = $('#find').val();
        $.ajax({
            url: '/ruangan/search',
            type: 'POST',
            data: {
                _token: CSRF_TOKEN,
                value: value,
            },
            success: function(data) {
                $('#content').html(data);
            }
        })
    }
    $(document).ready(function() {
        $(document).on('click', '.pagination a', function(e) {
            e.preventDefault();
            var page = $(this).attr('href').split('page=')[1];

            $.ajax({
                type: "GET",
                url: "/ruangan_fatch_data" + "?page=" + page,
                success: function(data) {
                    $('#content').html(data);
                }
            })
        });
    });
</script>
@endpush