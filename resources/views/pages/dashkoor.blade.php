@extends('layouts.index')

@push('title', 'Dash | Polinema PSDKU Kediri')

@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">Koordinator</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Dashboard</a></li>
                            <li class="breadcrumb-item active" aria-current="page">dashboard</li>
                        </ol>
                    </nav>
                </div>
            </div>
            <!-- Card stats -->
            <div class="row">
                <div class="col-xl-3 col-md-6">
                    <div class="card card-stats">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Menunggu Validasi</h5>
                                    <span class="h2 font-weight-bold mb-0">{{$count_tunggu}}</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow">
                                        <i class="ni ni-active-40"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-stats">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Setuju</h5>
                                    <span class="h2 font-weight-bold mb-0">{{$count_setuju}}</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-gradient-orange text-white rounded-circle shadow">
                                        <i class="ni ni-chart-pie-35"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-md-6">
                    <div class="card card-stats">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0">Tolak</h5>
                                    <span class="h2 font-weight-bold mb-0">{{$count_tolak}}</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-gradient-green text-white rounded-circle shadow">
                                        <i class="ni ni-shop"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">
                                Data Validasi {{Auth::user()->roles}}
                            </h4>
                        </div>
                    </div>
                    <p class="card-description">
                        Daftar pinjaman menunggu validasi {{Auth::user()->roles}}
                    </p>
                </div>
                <div class="table-responsive py-4">
                    <table class="table table-flush" id="datatable-basic">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Nama Peminjam</th>
                                <th>Keperluan</th>
                                <th>Tanggal Kegiatan</th>
                                <th>Status</th>
                                <th>Validasi</th>
                                <th></th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pinjam as $data)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $data->user->name }}</td>
                                <td>{{ $data->keperluan }}</td>
                                <td>{{ date('d F Y', strtotime($data->tanggal_pinjam)) }}</td>
                                <td>
                                    @if($data->validasi_koor == 0)
                                    <label class="badge badge-warning shadow">Pendding</label>
                                    @elseif($data->validasi_koor == 1)
                                    <label class="badge badge-success shadow">Lolos</label>
                                    @elseif($data->validasi_koor == 2)
                                    <label class="badge badge-danger shadow">Tolak</label>
                                    @endif
                                </td>
                                <td>
                                    @if($data->validasi_bmn != 0)
                                </td>
                                <td>
                                    @else
                                    @if($data->validasi_koor == 0)
                                    <form action="/pinjam/{{$data->id}}" method="post">
                                        @csrf
                                        @method('put')
                                        <input type="hidden" name="validasi_koor" value="2">
                                        <button type="submit" class="btn btn-danger btn-sm">Tolak</button>
                                    </form>
                                    @elseif($data->validasi_koor == 1)
                                    <form action="/pinjam/{{$data->id}}" method="post">
                                        @csrf
                                        @method('put')
                                        <input type="hidden" name="validasi_koor" value="2">
                                        <button type="submit" class="btn btn-danger btn-sm">Tolak</button>
                                    </form>
                                    @endif
                                </td>
                                <td>
                                    @if($data->validasi_koor == 0)
                                    <form action="/pinjam/{{$data->id}}" method="post">
                                        @csrf
                                        @method('put')
                                        <input type="hidden" name="validasi_koor" value="1">
                                        <button type="submit" class="btn btn-success btn-sm">Setuju</button>
                                    </form>
                                    @elseif($data->validasi_koor == 2)
                                    <form action="/pinjam/{{$data->id}}" method="post">
                                        @csrf
                                        @method('put')
                                        <input type="hidden" name="validasi_koor" value="1">
                                        <button type="submit" class="btn btn-success btn-sm">Setuju</button>
                                    </form>
                                    @endif
                                    @endif
                                </td>
                                <td>
                                    <a href='/pinjam/{{$data->id}}'" class=" btn btn-primary btn-sm">Detail</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('script')
    {!! Toastr::message() !!}
    @endpush
    @push('style')
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">
    @endpush