@extends('layouts.index')
@push('title', 'Profile | PSDKU Kota Kediri')
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">BMN</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Pengguna</a></li>
                            <li class="breadcrumb-item active" aria-current="page">details</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-md-4">
            <div class="card card-profile">
                <img src="{{url('assets/img/theme/img-1-1000x600.jpg')}}" alt="Image placeholder" class="card-img-top">
                <div class="row justify-content-center">
                    <div class="col-lg-4 order-lg-4">
                        <div class="card-profile-image">
                            <a href="#">
                                <img src="https://ui-avatars.com/api/?name={{$user->name}}" class="rounded-circle">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-4">
                    <div class="text-center">
                        <h5 class="h3">
                            {{ $user->name }}
                        </h5>
                        <div class="h5 font-weight-300">
                            <i class="ni location_pin mr-2"></i>
                            <h6>{{$user->alamat}}</h6>
                        </div>
                        <div class="h5 mt-4">
                            <i class="ni business_briefcase-24 mr-2"></i>
                            <h6>{{$user->roles}}</h6>
                        </div>
                        <div>
                            <i class="ni education_hat mr-2"></i>{{$user->no_telp}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card bg-gradient-info border-0">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0 text-white">Total Permohonan</h5>
                                    <span class="h2 font-weight-bold mb-0 text-white">{{count( App\Models\Pinjam::where('user_id', $user->id)->get() )}}</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                        <i class="ni ni-active-40"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="card bg-gradient-danger border-0">
                        <!-- Card body -->
                        <div class="card-body">
                            <div class="row">
                                <div class="col">
                                    <h5 class="card-title text-uppercase text-muted mb-0 text-white">Total Peminjaman</h5>
                                    <span class="h2 font-weight-bold mb-0 text-white">{{count( App\Models\Kembali::where('user_id', $user->id)->get() )}}</span>
                                </div>
                                <div class="col-auto">
                                    <div class="icon icon-shape bg-white text-dark rounded-circle shadow">
                                        <i class="ni ni-spaceship"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-sm mb-0">
                        Histori Pengajuan Pinjaman
                    </h3>
                </div>
                <div class="table-responsive py-4">
                    <table class="table table-flush" id="datatable-basic">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Keperluan</th>
                                <th>Tanggal Kegiatan</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($user->pinjam as $data)
                            <tr>
                                <th>{{ $loop->iteration }}</th>
                                <td>{{ $data->keperluan }}</td>
                                <td>{{ date('d F Y', strtotime($data->tanggal_pinjam)) }}</td>
                                <td>
                                    @if( $data->validasi_ktu == 1 && $data->validasi_koor == 1 && $data->validasi_bmn == 1 && $data->status == 0 )
                                    <label class="badge badge-success shadow">Disetujui</label>
                                    @elseif( $data->validasi_ktu == 1 && $data->validasi_koor == 1 && $data->validasi_bmn == 1 && $data->status == 1)
                                    <label class="badge badge-warning shadow">Dibawa</label>
                                    @elseif( $data->validasi_ktu == 1 && $data->validasi_koor == 1 && $data->validasi_bmn == 0 )
                                    <label class="badge badge-info shadow">Seleksi BMN</label>
                                    @elseif( $data->validasi_ktu == 1 && $data->validasi_koor == 0 && $data->validasi_bmn == 0 )
                                    <label class="badge badge-info shadow">Seleksi Koordinator</label>
                                    @elseif( $data->validasi_ktu == 0 && $data->validasi_koor == 0 && $data->validasi_bmn == 0 )
                                    <label class="badge badge-info shadow">Seleksi TU</label>
                                    @elseif( $data->validasi_ktu == 1 && $data->validasi_koor == 1 && $data->validasi_bmn == 2 )
                                    <label class="badge badge-danger shadow">Tolak BMN</label>
                                    @elseif( $data->validasi_ktu == 1 && $data->validasi_koor == 2 && $data->validasi_bmn == 0 )
                                    <label class="badge badge-danger shadow">Tolak Koornonator</label>
                                    @elseif( $data->validasi_ktu == 2 && $data->validasi_koor == 0 && $data->validasi_bmn == 0 )
                                    <label class="badge badge-danger shadow">Tolak TU</label>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-sm mb-0">
                        Histori Peminjaman / Pengembalian
                    </h3>
                </div>
                <div class="table-responsive py-4">
                    <table class="table table-flush dataTable" id="datatable-buttons">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Keperluan</th>
                                <th>Tanggal Ambil</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($user->kembali as $item)
                            <tr>
                                <th>{{ $loop->iteration }}</th>
                                <td>{{ $item->pinjam->keperluan }}</td>
                                <td>{{ date('d F Y', strtotime($item->date_ambil)) }}</td>
                                <td>
                                    @if( $item->status == 1 )
                                    <label class="badge badge-success shadow">Dikembalikan</label>
                                    @elseif( $item->status == 0 )
                                    <label class="badge badge-light shadow">Dipinjam</label>
                                    @elseif( $item->status == 2 )
                                    <label class="badge badge-danger shadow">Tanggungan</label>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('style')
    <!-- DataTables -->
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">
    @endpush