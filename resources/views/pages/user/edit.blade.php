@extends('layouts.index')
@push('title', 'User | Polinema PSDKU Kediri')
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">BMN</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Pengguna</a></li>
                            <li class="breadcrumb-item active" aria-current="page">edit</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-md-12">
            <div class="card-wrapper">
                <!-- Input groups -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">Ubah Data Pengguna</h3>
                    </div>
                    <!-- Card body -->
                    <div class="card-body">
                        <form class="form-horizontal mt-4" method="POST" action="/user/{{$user->id}}">
                            @csrf
                            @method('put')
                            <!-- Input groups with icon -->
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Nama</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-user"></i></span>
                                            </div>
                                            <input class="form-control @error('name') is-invalid @enderror" name="name" type="text" value="{{ $user->name }}" placeholder="Nama Penguna">
                                        </div>
                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">E-Mail</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-envelope"></i></span>
                                            </div>
                                            <input class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $user->email }}" type="email" placeholder="user@example.com">
                                        </div>
                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">NIM / Nidn</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-id-card"></i></i></span>
                                            </div>
                                            <input class="form-control @error('nim_nidn') is-invalid @enderror" value="{{ $user->nim_nidn }}" name="nim_nidn" placeholder="Nidn / Password" type="number">
                                        </div>
                                        @error('nim_nidn')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Role</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-layer-group"></i></span>
                                            </div>
                                            <select name="roles" id="roles" class="form-control @error('roles') is-invalid @enderror">
                                                <option value="" selected disabled>Pilih Role</option>
                                                <option value="BMN" {{ $user->roles == 'BMN' ? 'selected' : null }}>BMN</option>
                                                <option value="Koordinator" {{ $user->roles == 'Koordinator' ? 'selected' : null }}>Koordinator</option>
                                                <option value="KTU" {{ $user->roles == 'KTU' ? 'selected' : null }}>Ketua TU</option>
                                                <option value="Mahasiswa" {{ $user->roles == 'Mahasiswa' ? 'selected' : null }}>Dosen / Mahasiswa</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Jenis Kelamin</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-venus-mars"></i></span>
                                            </div>
                                            <select name="jenis_kelamin" id="jk" class="form-control">
                                                <option value="" selected disabled>Jenis Kelamin</option>
                                                <option value="Laki-laki" {{ $user->jenis_kelamin == 'Laki-Laki' ? 'selected' : null }}>Pria</option>
                                                <option value="Perempuan" {{ $user->jenis_kelamin == 'Perempuan' ? 'selected' : null }}>Wanita</option>
                                                <option value="Not Found" {{ $user->jenis_kelamin == 'Not Found' ? 'selected' : null }}>Not Found</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Status</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-toggle-on"></i></span>
                                            </div>
                                            <select name="status_mhs" id="status" class="form-control">
                                                <option value="" selected disabled>Status</option>
                                                <option value="Aktif" {{ $user->status_mhs == 'Aktif' ? 'selected' : null }}>Aktif</option>
                                                <option value="Non-Aktif" {{ $user->status_mhs == 'Non-Aktif' ? 'selected' : null }}>Non Aktif</option>
                                                <option value="Lainnya" {{ $user->status_mhs == 'Lainnya' ? 'selected' : null }}>Lainnya</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Jalan</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-road"></i></span>
                                            </div>
                                            <input class="form-control" name="alamat" type="text" value="{{ $user->alamat }}" placeholder="Alamat..">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Rt</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-address-card"></i></span>
                                            </div>
                                            <input class="form-control" name="rt" value="{{ $user->rt }}" type="number" placeholder="Rt">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Rw</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-address-card"></i></i></span>
                                            </div>
                                            <input class="form-control" name="rw" type="number" value="{{ $user->rw }}" placeholder="Rw">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Desa</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-address-card"></i></span>
                                            </div>
                                            <input class="form-control" name="desa" value="{{ $user->desa }}" type="text" placeholder="desa">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Kota</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-globe-europe"></i></span>
                                            </div>
                                            <input class="form-control" name="kota" type="text" value="{{ $user->kota }}" placeholder="Kota">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">No Telpon</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                            </div>
                                            <input class="form-control" name="no_telp" value="{{ $user->no_telp }}" type="number" placeholder="08xxxxx">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-icon btn-primary">
                                <span class="btn-inner--icon"><i class="ni ni-briefcase-24"></i></span>
                                <span class="btn-inner--text">Simpan</span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection