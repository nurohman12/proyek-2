@extends('layouts.index')
@push('title', 'User | Polinema PSDKU Kediri')
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">BMN</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Pengguna</a></li>
                            <li class="breadcrumb-item active" aria-current="page">user</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col">
            <div class="card">
                <!-- Card header -->
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">
                                Data Pengguna »
                            </h4>
                        </div>
                        <div class="col-auto">
                            <a href="/user/create" class="btn btn-primary btn-sm">
                                Add Users
                            </a>
                        </div>
                        <div class="col-auto">
                            <a href="#" data-toggle="modal" data-target="#importModal" class="btn btn-success btn-sm">
                                Import Users
                            </a>
                        </div>
                        <div class="col-auto">
                            <a href="/user/export" class="btn btn-danger btn-sm">
                                Export Users
                            </a>
                        </div>
                    </div>
                    <p class="text-sm mb-0">
                        Daftar pengguna pada sistem
                    </p>
                </div>
                <div class="table-responsive py-4">
                    <table class="table table-flush" id="datatable-basic">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Nama</th>
                                <th>Nim/Nidn</th>
                                <th>Email</th>
                                <th>Level</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($user as $data)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$data->name}}</td>
                                <td>{{$data->nim_nidn}}</td>
                                <td>{{$data->email}}</td>
                                <td>
                                    @if($data->roles == 'Mahasiswa')
                                    <label class="badge badge-success rounded">{{$data->roles}}</label>
                                    @elseif($data->roles == 'KTU')
                                    <label class="badge badge-info rounded">{{$data->roles}}</label>
                                    @elseif($data->roles == 'Koordinator')
                                    <label class="badge badge-warning rounded">{{$data->roles}}</label>
                                    @elseif($data->roles == 'BMN')
                                    <label class="badge badge-primary rounded">{{$data->roles}}</label>
                                    @endif
                                </td>
                                <td>
                                    <a href="/user/{{$data->id}}" class="btn btn-success btn-sm">Detail</a>
                                    <a href="/user/{{$data->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                    <form action="/user/{{$data->id}}" method="post" style="display: inline;">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin hapus data ini?')">
                                            Hapus
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @endsection
    @push('style')
    <!-- DataTables -->
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">
    @endpush