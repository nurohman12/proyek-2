@extends('layouts.index')
@push('title', 'Detail | PSDKU Kota Kediri')
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">BMN</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Peminjaman</a></li>
                            <li class="breadcrumb-item active" aria-current="page">details</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-md-4">
            <div class="card card-profile">
                <img src="{{url('assets/img/theme/img-1-1000x600.jpg')}}" alt="Image placeholder" class="card-img-top">
                <div class="row justify-content-center mb-4">
                    <div class="col-lg-4 order-lg-4">
                        <div class="card-profile-image">
                            <a href="#">
                                <img src="https://ui-avatars.com/api/?name={{$tanggungan->user->name}}" style="width: 10rem;" class="rounded-circle">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-4">
                    <div class="text-center">
                        <h5 class="h3">
                            {{ $tanggungan->user->name }}
                        </h5>
                        <div class="h5 font-weight-300">
                            <i class="ni location_pin mr-2"></i>
                            <h6>{{$tanggungan->user->roles}}</h6>
                        </div>
                        <p>
                            Daftar pinjaman di tampilkan dalam table,
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <small class="text-muted">Alamat email</small>
                            <h6>{{$tanggungan->user->email}}</h6>
                            <small class="text-muted">Nim/Nidn</small>
                            <h6>{{$tanggungan->user->nim_nidn}}</h6>
                            <small class="text-muted">Status</small>
                            <h6>{{$tanggungan->user->status_mhs}}</h6>
                            <small class="text-muted">Jenis Kelamin</small>
                            <h6>{{$tanggungan->user->jenis_kelamin}}</h6>
                        </div>
                        <div class="col-md-6">
                            <small class="text-muted">Jalan</small>
                            <h6>{{$tanggungan->user->alamat}}</h6>
                            <small class="text-muted pt-4 db">Rt</small>
                            <h6>{{$tanggungan->user->rt}}</h6>
                            <small class="text-muted pt-4 db">Rw</small>
                            <h6>{{$tanggungan->user->rw}}</h6>
                            <small class="text-muted pt-4 db">No Telepon</small>
                            <h6>{{$tanggungan->user->no_telp}}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label for="kep"> <code>*</code>Keperluan</label>
                        <h3 class="card-title">{{ $tanggungan->pinjam->keperluan}}</h3>
                    </div>
                    <div class="row form-group">
                        <div class="col-4">
                            <label for="tgl"> <code>*</code>Tanggal Ambil</label>
                            <h3 class="card-title">{{date('d F Y', strtotime($tanggungan->date_ambil))}}</h3>
                        </div>
                        <div class="col-4">
                            <label for="tgl"> <code>*</code>Tanggal Kegiatan</label>
                            <h3 class="card-title">{{date('d F Y', strtotime($tanggungan->pinjam->tanggal_pinjam))}}</h3>
                        </div>
                        @if($tanggungan->date_kembali)
                        <div class="col-4">
                            <label for="tgl"> <code>*</code>Tanggal Pengembalian</label>
                            <h3 class="card-title">{{date('d F Y', strtotime($tanggungan->date_kembali))}}</h3>
                        </div>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="pro"> <code>*</code>Proposal</label><br>
                        <div class="row">
                            <div class="col">
                                <a href="/storage/{{ $tanggungan->pinjam->proposal }}" target="_blank" rel="noopener noreferrer">
                                    <button type="button" class="btn btn-outline-success"> <i class="mdi mdi-file"></i> Proposal</button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive py-4" id="content">
                        @include('pages.kembali.table')
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection


    @push('script')
    {!! Toastr::message() !!}
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class=" nama-sarpras modal-title" style="color: black;" id="exampleModalLabel"> </h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img id="img" src="" style="width: 29rem;" alt="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Modal Sarpras Rusak  -->
    <div class="modal fade" id="ModalRusak" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title nama-sar" id="exampleModalLabel"> </h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="keterangan" style="color: black;">Keterangan</label>
                        <input type="hidden" class="draf_id" id="draf_id">
                        <input type="hidden" class="kembali_id" id="kembali_id">
                        <select class="form-control" id="keterangan">
                            <option disabled selected>Pilih Keterangan</option>
                            <option>Hilang</option>
                            <option>Rusak</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="" id="rusak">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Sarpras Rusak  -->
    <!-- Modal Verifikasi -->
    <div class="modal fade" id="ModalVerifikasi" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" id="exampleModalLabel">Verifikasi Sarpras</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="/verifikasi" method="post">
                    @csrf
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-sm-5">
                                <img id="img_sarpras" src="" style="width: 28rem;" alt="">
                            </div>
                            <div class="col-sm-7">
                                <h4 class="display-3" id="name"></h4>
                                <h4 class="display-5">Jumlah</h4>
                                <div class="row">
                                    <div class="col-6">
                                        <label for="">Pinjam</label>
                                        <p class="ml-1" id="jumlah_pinjam"></p>
                                    </div>
                                    <div class="col-6">
                                        <label for="">Dikembalikan</label>
                                        <p class="ml-1" id="jumlah_kembali"></p>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <label for="">Tanggungan</label>
                                        <p class="ml-1" id="jumlah_tanggungan"></p>
                                    </div>
                                    <div class="col-6">
                                        <label for="">Rusak</label>
                                        <p class="ml-1" id="rusak_"></p>
                                    </div>
                                </div>
                                <label for="">Tanggal Ambil</label>
                                <p class="ml-1">{{date('d F Y', strtotime($tanggungan->date_ambil))}}</p>
                                <div class="form-group">
                                    <label for="">Sesuai</label>
                                    <div class="input-group">
                                        <input type="number" name="sesuai" class="form-control shadow">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <label for="">Tidak Sesuai</label>
                                            <input type="number" name="tidak" class="form-control shadow bg-secondary">
                                        </div>
                                        <div class="col-md-9">
                                            <label for="">Keterangan</label>
                                            <input type="text" name="keterangan" class="form-control shadow">
                                            <input type="hidden" name="draf_id" class="draf_id">
                                            <input type="hidden" name="kembali_id" class="kembali_id">
                                        </div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn-icon-text shadow" style="text-align: right;">
                                    <i class="mdi mdi-file-check btn-icon-prepend"></i>
                                    Simpan
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Modal Verifikasi  -->

    <script>
        $(document).ready(function() {
            $(document).on('click', '#verifikasi', function() {
                var nama = $(this).data('nama');
                var id = $(this).data('id');
                var img = $(this).data('img_sarpras');
                var jml = $(this).data('jumlah_pinjam');
                var jml_kem = $(this).data('jumlah_kembali');
                var jml_tang = $(this).data('jumlah_tanggungan');
                var jml_rsk = $(this).data('rusak_');
                var kembali = $(this).data('kembali_id');

                $('#name').text(nama);
                $('#img_sarpras').attr("src", '/storage/' + img);
                $('#jumlah_pinjam').text(jml);
                $('#jumlah_kembali').text(jml_kem);
                $('#jumlah_tanggungan').text(jml_tang);
                $('#rusak_').text(jml_rsk);
                $('.draf_id').val(id);
                $('.kembali_id').val(kembali);
            });
            $(document).on('click', '#show', function() {
                var nama_sar = $(this).data('nama_item');
                var img = $(this).data('img');

                $('.nama-sarpras').text("Gambar " + nama_sar);
                $('#img').attr("src", '/storage/' + img);
            });
        });
    </script>
    @endpush
    @push('style')
    <!-- DataTables -->
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">
    @endpush