<table class="table table-flush" id="datatable-basic">
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Nama Sarpras</th>
            <th>Tanggungan</th>
            <th>Status</th>
            <th>Keterangan</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($tanggungan->pinjam->draf as $data)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $data->sarpras->nama }}</td>
            <td>{{ $data->qty }}</td>
            <td>
                @if($data->kondisi == 0)
                <span class="badge badge-warning">Dipinjam</span>
                @elseif($data->kondisi == 1)
                <span class="badge badge-success">Dikembalikan</span>
                @elseif($data->kondisi == 2)
                <span class="badge badge-danger">Tanggungan</span>
                @endif
            </td>
            <td>
                @if( App\Models\Sarpras_Keluar::where('draf_id', $data->id)->first()->rusak > 0 )
                <p class="text-lowercase">{{App\Models\Sarpras_Keluar::where('draf_id', $data->id)->first()->rusak}} {{$data->keterangan}}</p>
                @endif
            </td>
            <td>
                <button data-toggle="modal" id="show" data-nama_item="{{ $data->sarpras->nama }}" data-img="{{ $data->sarpras->photo }}" data-target="#exampleModal" class="btn btn-primary btn-icon-only rounded-circle">
                    <i class="ni ni-album-2" data-toggle="tooltip" title="Photo"></i>
                </button>
                <button class="btn btn-info btn-icon-only rounded-circle" data-id="{{$data->id}}" data-kembali_id="{{$tanggungan->id}}" data-nama="{{$data->sarpras->nama}}" data-img_sarpras="{{$data->sarpras->photo}}" data-jumlah_pinjam="{{ App\Models\Sarpras_Keluar::where('draf_id', $data->id)->sum('jumlah') }}" data-jumlah_kembali="{{ App\Models\Sarpras_Masuk::where('draf_id', $data->id)->sum('jumlah') }}" data-jumlah_tanggungan="{{ $data->qty }}" data-rusak_="{{  App\Models\Sarpras_Keluar::where('draf_id', $data->id)->sum('rusak') }}" id="verifikasi" data-toggle="modal" data-target="#ModalVerifikasi">
                    <i class="ni ni-vector" data-toggle="tooltip" title="Verifikasi"></i>
                </button>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>