@extends('layouts.index')
@push('title', 'Validasi | Polinema PSDKU Kediri')
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">BMN</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Validasi</a></li>
                            <li class="breadcrumb-item active" aria-current="page">semua</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">
                                Data Semua Permohonan
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="table-responsive py-4">
                    <table class="table table-flush" id="datatable-basic">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Nama Peminjam</th>
                                <th>Keperluan</th>
                                <th>Tanggal Kegiatan</th>
                                <th>Validasi KTU</th>
                                <th>Validasi KOOR</th>
                                <th>Validasi BMN</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($pinjam as $data)
                            <tr>
                                <td>{{ $loop->iteration}}</td>
                                <td>{{ $data->user->name }}</td>
                                <td>{{ $data->keperluan }}</td>
                                <td>{{ date('d F Y', strtotime($data->tanggal_pinjam)) }}</td>
                                <td>
                                    @if($data->validasi_ktu == 0)
                                    <label class="badge badge-warning shadow">Pendding</label>
                                    @elseif($data->validasi_ktu == 1)
                                    <label class="badge badge-success shadow">Lolos</label>
                                    @elseif($data->validasi_ktu == 2)
                                    <label class="badge badge-danger shadow">Tolak</label>
                                    @endif
                                </td>
                                <td>
                                    @if($data->validasi_koor == 0)
                                    <label class="badge badge-warning shadow">Pendding</label>
                                    @elseif($data->validasi_koor == 1)
                                    <label class="badge badge-success shadow">Lolos</label>
                                    @elseif($data->validasi_koor == 2)
                                    <label class="badge badge-danger shadow">Tolak</label>
                                    @endif
                                </td>
                                <td>
                                    @if($data->validasi_bmn == 0)
                                    <label class="badge badge-warning shadow">Pendding</label>
                                    @elseif($data->validasi_bmn == 1)
                                    <label class="badge badge-success shadow">Lolos</label>
                                    @elseif($data->validasi_bmn == 2)
                                    <label class="badge badge-danger shadow">Tolak</label>
                                    @endif
                                </td>
                                <td>
                                    <a href="/pinjam/{{$data->id}}" class="btn btn-primary btn-sm">Detail</a>
                                    @if($data->status == 0)
                                    <a href="/pinjam/{{$data->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                    <form action="/pinjam/{{$data->id}}" method="post" style="display: inline;">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin hapus data ini?')">
                                            Hapus
                                        </button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @endsection
    @push('script')
    {!! Toastr::message() !!}
    @endpush
    @push('style')
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">
    @endpush