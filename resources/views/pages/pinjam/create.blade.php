@extends('layouts.index')
@push('title', 'Create | Polinema PSDKU Kediri')
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">BMN</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Pinjam</a></li>
                            <li class="breadcrumb-item active" aria-current="page">create</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col">
            <div class="card">
                <!-- Card header -->
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">
                                Data Peminjam »
                            </h4>
                        </div>
                    </div>
                    <form method="POST" action="/pinjam" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="jns">Pengguna</label>
                            <select name="pengguna" id="jns" class="form-control @error('pengguna') is-invalid @enderror">
                                <option value="{{old('pengguna')}}" selected disabled>Pengguna</option>
                                @foreach($user as $data)
                                <option value="{{$data->id}}" {{old('pengguna') == '$data->id' ? 'selected' : null }}>{{$data->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="fungsi">Tanggal Kegiatan</label>
                            <input type="date" value="{{old('tanggal_kegiatan') }}" class="form-control @error('tanggal_kegiatan') is-invalid @enderror" id="fungsi" name="tanggal_kegiatan">
                            @error('tanggal_kegiatan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="fung">Keperluan</label>
                            <input type="text" class="form-control @error('keperluan') is-invalid @enderror" value="{{ old('keperluan') }}" id="fung" name="keperluan">
                            @error('keperluan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label>Proposal</label>
                            <input type="file" name="proposal" class="form-control @error('proposal') is-invalid @enderror">
                            @error('proposal')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-primary mr-2">Tambah</button>
                        <button type="reset" class="btn btn-secondary mr-2">Batal</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <p class="card-description">
                                Draf peminjaman
                            </p>
                        </div>
                        <div class="col-auto">
                            <button onclick="window.location.href='#list_sarpras'" type="button" class="btn btn-outline-primary btn-icon-text">
                                <i class="mdi mdi-arrow-all btn-icon-prepend"></i>
                                Pilih Sarpras
                            </button>
                        </div>
                    </div>
                    <div class="table-responsive py-4">
                        <table class="table table-flush" id="datatable-basic">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Nama</th>
                                    <th>Jumlah</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($draf as $data)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $data->sarpras->nama }}</td>
                                    <td>{{ $data->qty }}</td>
                                    <td>
                                        <button class="btn btn-warning btn-rounded btn-icon" id="edit" data-id="{{$data->id}}" data-sarpras="{{$data->sarpras->id}}" data-img="{{$data->sarpras->photo}}" data-jumlah="{{$data->sarpras->jumlah}}" data-nama="{{$data->sarpras->nama}}" data-qty="{{$data->qty}}" data-toggle="modal" data-target="#exampleModal">
                                            <i class="mdi mdi-table-edit" data-toggle="tooltip" title="Edit"></i>
                                        </button>
                                        <form action="/peminjamandraf/{{$data->id}}" method="post" style="display: inline;">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger btn-rounded btn-icon">
                                                <i class="mdi mdi-delete-variant" onclick="return confirm('Yakin hapus data ini?')" data-toggle="tooltip" title="Delete"></i>
                                            </button>
                                        </form>

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <style>
        .card-img-top {
            height: 30vh;
        }

        .car {
            margin-bottom: 3vh;
        }

        .card-title {
            margin-top: -2vh;
        }

        .card-text {
            margin-top: -2vh;
        }

        .btn-wrapper {
            text-align: center;
        }

        .quantity {
            margin-left: 10vh;
            margin-right: 10vh;
        }

        .qty-input {
            text-align: center;
        }
    </style>
    <div class="row mt-5" id="list_sarpras">
        @foreach($sarpras as $data)
        <div class="col-md-4 car">
            <div class="card">
                <img src="{{ url('/storage/'. $data->photo) }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">{{ $data->nama }}</h5>
                    <p class="card-text">Jumlah {{ $data->jumlah }}</p>
                    <div class="btn-wrapper sarpras_data">
                        <div class="input-group mb-3 mt-3 ">
                            <div class="input-group quantity">
                                <div class="input-group-prepend decrement-btn" style="cursor: pointer">
                                    <span class="input-group-text">-</span>
                                </div>
                                <input type="hidden" value="{{$data->id}}" class="sarpras_id">
                                <input type="hidden" value="{{$data->jumlah}}" class="qty-max">
                                <input type="text" readonly class="qty-input form-control" value="1">
                                <div class="input-group-append increment-btn" style="cursor: pointer">
                                    <span class="input-group-text">+</span>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary addToDraf mr-2">Add to Draf</button>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @endsection
    @push('script')
    {!! Toastr::message() !!}
    <!-- Modal  -->
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Jumlah</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="car">
                        <img id="img" src="" style="width: 29rem;" alt="portffolio" class="project-thumbnail">
                        <div class="card-body">
                            <h5 class="project-name mt-4" id="nama"></h5>
                            <p class="project-category" id="qty-jumlah"></p>
                            <div class="btn-wrapper sarpras_data">
                                <form id="draf_id" action="" method="post">
                                    @csrf
                                    @method('put')
                                    <div class="input-group mb-3 mt-3 ">
                                        <div class="input-group quantity">
                                            <div class="input-group-prepend decrement-btn" style="cursor: pointer">
                                                <span class="input-group-text">-</span>
                                            </div>
                                            <input type="hidden" name="sarpras_id" id="sarpras_id">
                                            <input type="hidden" id="qty-max" class="qty-max">
                                            <input type="hidden" name="draf_id" id="draf_id">
                                            <input type="text" name="qty" readonly class="qty-input form-control" id="qty-modal">
                                            <div class="input-group-append increment-btn" style="cursor: pointer">
                                                <span class="input-group-text">+</span>
                                            </div>
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $(document).on('click', '#edit', function() {
                var id = $(this).data('id');
                var sarpras_id = $(this).data('sarpras');
                var nama = $(this).data('nama');
                var jumlah = $(this).data('jumlah');
                var img = $(this).data('img');
                var qty = $(this).data('qty');

                $('#draf_id').val(id);
                $('#draf_id').attr("action", '/peminjaman/' + id);
                $('#sarpras_id').val(sarpras_id);
                $('#nama').text(nama);
                $('#qty-jumlah').text('Jumlah ' + jumlah);
                $('#qty-max').val(jumlah);
                $('#img').attr("src", '/storage/' + img);
                $('#qty-modal').val(qty);
            });

            $('.addToDraf').click(function(e) {
                e.preventDefault();
                var sarpras_id = $(this).closest('.sarpras_data').find('.sarpras_id').val();
                var sarpras_qty = $(this).closest('.sarpras_data').find('.qty-input').val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    method: "POST",
                    url: "/pinjaman/draf",
                    data: {
                        'sarpras_id': sarpras_id,
                        'sarpras_qty': sarpras_qty,
                    },
                    success: function(response) {
                        alert(response.status);
                    }
                });
            });

            $('.increment-btn').click(function(e) {
                e.preventDefault();
                var incre_value = $(this).parents('.quantity').find('.qty-input').val();
                var max_value = $(this).parents('.quantity').find('.qty-max').val();
                var value = parseInt(incre_value, max_value);
                value = isNaN(value) ? 0 : value;
                if (value < max_value) {
                    value += 1;
                    $(this).parents('.quantity').find('.qty-input').val(value);
                }

            });

            $('.decrement-btn').click(function(e) {
                e.preventDefault();
                var decre_value = $(this).parents('.quantity').find('.qty-input').val();
                var max_value = $(this).parents('.quantity').find('.qty-max').val();
                var value = parseInt(decre_value, max_value);
                value = isNaN(value) ? 0 : value;
                if (value > 1) {
                    value--;
                    $(this).parents('.quantity').find('.qty-input').val(value);
                }
            });

        });
    </script>
    @endpush

    @push('style')
    <!-- DataTables -->
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">
    @endpush