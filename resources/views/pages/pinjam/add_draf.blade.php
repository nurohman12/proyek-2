@extends('layouts.index')
@push('title', 'Add Draf Pinjam | Polinema PSDKU Kediri')
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">BMN</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Pinjam</a></li>
                            <li class="breadcrumb-item"><a href="#">draf</a></li>
                            <li class="breadcrumb-item active" aria-current="page">add draf</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
        <style>
            .card-img-top {
                height: 30vh;
            }

            .car {
                margin-bottom: 3vh;
            }

            .card-title {
                margin-top: -2vh;
            }

            .card-text {
                margin-top: -2vh;
            }

            .btn-wrapper {
                text-align: center;
            }

            .quantity {
                margin-left: 10vh;
                margin-right: 10vh;
            }

            .qty-input {
                text-align: center;
            }
        </style>
        @foreach($sarpras as $data)
        <div class="col-md-4 car">
            <div class="card">
                <img src="{{ url('/storage/'. $data->photo) }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-title">{{ $data->nama }}</h5>
                    <p class="card-text">Jumlah {{ $data->jumlah }}</p>
                    <div class="btn-wrapper sarpras_data">
                        <div class="input-group mb-3 mt-3 ">
                            <div class="input-group quantity">
                                <div class="input-group-prepend decrement-btn" style="cursor: pointer">
                                    <span class="input-group-text">-</span>
                                </div>
                                <input type="hidden" class="user_id" value="{{$pinjam->user_id}}">
                                <input type="hidden" class="pinjam_id" value="{{$id}}">
                                <input type="hidden" value="{{$data->id}}" class="sarpras_id">
                                <input type="hidden" value="{{$data->jumlah}}" class="qty-max">
                                <input type="text" readonly class="qty-input form-control" value="1">
                                <div class="input-group-append increment-btn" style="cursor: pointer">
                                    <span class="input-group-text">+</span>
                                </div>
                            </div>
                        </div>
                        <button class="btn btn-primary addToDraf mr-2">Add to Draf</button>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
    @endsection

    @push('script')
    <script>
        $(document).ready(function() {

            $('.addToDraf').click(function(e) {
                e.preventDefault();
                var sarpras_id = $(this).closest('.sarpras_data').find('.sarpras_id').val();
                var sarpras_qty = $(this).closest('.sarpras_data').find('.qty-input').val();
                var pinjam_id = $(this).closest('.sarpras_data').find('.pinjam_id').val();
                var user_id = $(this).closest('.sarpras_data').find('.user_id').val();

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $.ajax({
                    method: "POST",
                    url: "/pinjam/draf",
                    data: {
                        'sarpras_id': sarpras_id,
                        'sarpras_qty': sarpras_qty,
                        'pinjam_id': pinjam_id,
                        'user_id': user_id,
                    },
                    success: function(response) {
                        alert(response.status);
                    }
                });
            });

            $('.increment-btn').click(function(e) {
                e.preventDefault();
                var incre_value = $(this).parents('.quantity').find('.qty-input').val();
                var max_value = $(this).parents('.quantity').find('.qty-max').val();
                var value = parseInt(incre_value, max_value);
                value = isNaN(value) ? 0 : value;
                if (value < max_value) {
                    value += 1;
                    $(this).parents('.quantity').find('.qty-input').val(value);
                }

            });

            $('.decrement-btn').click(function(e) {
                e.preventDefault();
                var decre_value = $(this).parents('.quantity').find('.qty-input').val();
                var max_value = $(this).parents('.quantity').find('.qty-max').val();
                var value = parseInt(decre_value, max_value);
                value = isNaN(value) ? 0 : value;
                if (value > 1) {
                    value--;
                    $(this).parents('.quantity').find('.qty-input').val(value);
                }
            });

        });
    </script>
    @endpush