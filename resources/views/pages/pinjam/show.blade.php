@extends('layouts.index')
@push('title', 'Detail | PSDKU Kota Kediri')
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">BMN</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Pinjam</a></li>
                            <li class="breadcrumb-item active" aria-current="page">details</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-md-4">
            <div class="card card-profile">
                <img src="{{url('assets/img/theme/img-1-1000x600.jpg')}}" alt="Image placeholder" class="card-img-top">
                <div class="row justify-content-center mb-4">
                    <div class="col-lg-4 order-lg-4">
                        <div class="card-profile-image">
                            <a href="#">
                                <img src="https://ui-avatars.com/api/?name={{$pinjam->user->name}}" style="width: 10rem;" class="rounded-circle">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-4">
                    <div class="text-center">
                        <h5 class="h3">
                            {{ $pinjam->user->name }}
                        </h5>
                        <div class="h5 font-weight-300">
                            <i class="ni location_pin mr-2"></i>
                            <h6>{{$pinjam->user->roles}}</h6>
                        </div>
                        <p>
                            Daftar pinjaman di tampilkan dalam table,
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <small class="text-muted">Alamat email</small>
                            <h6>{{$pinjam->user->email}}</h6>
                            <small class="text-muted">Nim/Nidn</small>
                            <h6>{{$pinjam->user->nim_nidn}}</h6>
                            <small class="text-muted">Status</small>
                            <h6>{{$pinjam->user->status_mhs}}</h6>
                            <small class="text-muted">Jenis Kelamin</small>
                            <h6>{{$pinjam->user->jenis_kelamin}}</h6>
                        </div>
                        <div class="col-md-6">
                            <small class="text-muted">Jalan</small>
                            <h6>{{$pinjam->user->alamat}}</h6>
                            <small class="text-muted pt-4 db">Rt</small>
                            <h6>{{$pinjam->user->rt}}</h6>
                            <small class="text-muted pt-4 db">Rw</small>
                            <h6>{{$pinjam->user->rw}}</h6>
                            <small class="text-muted pt-4 db">No Telepon</small>
                            <h6>{{$pinjam->user->no_telp}}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label for="kep"> <code>*</code>Keperluan</label>
                        <h3 class="card-title">{{ $pinjam->keperluan}}</h3>
                    </div>
                    <div class="row form-group">
                        <div class="col-6">
                            <label for="tgl"> <code>*</code>Tanggal Kegiatan</label>
                            <h3 class="card-title">{{date('d F Y', strtotime($pinjam->tanggal_pinjam))}}</h3>
                        </div>
                        <div class="col-6">
                            <label for="tgl"> <code>*</code>Tanggal Permohonan</label>
                            <h3 class="card-title">{{date('d F Y', strtotime($pinjam->created_at))}}</h3>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="pro"> <code>*</code>Proposal</label><br>
                        <a href="/storage/{{ $pinjam->proposal }}" target="_blank" rel="noopener noreferrer">
                            <button type="button" class="btn btn-outline-success"> <i class="mdi mdi-file"></i> Proposal</button>
                        </a>
                        @if(Auth::user()->roles == 'BMN' && $pinjam->validasi_ktu == 1 && $pinjam->validasi_koor == 1 && $pinjam->validasi_bmn == 1 && $pinjam->status == 0)
                        <form action="/tanggungan" method="post" style="display: inline;">
                            @csrf
                            <input type="hidden" name="pinjam_id" value="{{$pinjam->id}}">
                            <input type="hidden" name="user_id" value="{{$pinjam->user_id}}">
                            <button type="submit" class="btn btn-warning btn-icon-text">
                                <i class="mdi mdi-airplane-takeoff btn-icon-prepend"></i>Ambil Sarpras
                            </button>
                        </form>
                        @endif
                    </div>
                    <div class="table-responsive py-4">
                        <table class="table table-flush" id="datatable-basic">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Nama Sarpras</th>
                                    <th>Jumlah</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pinjam->draf as $data)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $data->sarpras->nama }}</td>
                                    <td>
                                        @if($data->qty == 0)
                                        {{
                                                App\Models\Sarpras_Keluar::where('draf_id', $data->id)->sum('jumlah')
                                                }}
                                        @else
                                        {{$data->qty}}
                                        @endif
                                    </td>
                                    <td>
                                        <button data-toggle="modal" id="show" data-nama_item="{{ $data->sarpras->nama }}" data-img="{{ $data->sarpras->photo }}" data-target="#exampleModal" class="btn btn-primary btn-icon-only rounded-circle">
                                            <i class="ni ni-album-2" data-toggle="tooltip" title="Photo"></i>
                                        </button>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection


    @push('script')
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" style="color: black;" id="exampleModalLabel"> </h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img id="img" src="" style="width: 29rem;" alt="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        $(document).ready(function() {
            $(document).on('click', '#show', function() {
                var nama = $(this).data('nama_item');
                var img = $(this).data('img');

                $('.modal-title').text("Gambar " + nama);
                $('#img').attr("src", '/storage/' + img);
            });
        });
    </script>
    @endpush
    @push('style')
    <!-- DataTables -->
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">
    @endpush