@extends('layouts.index')
@push('title', 'Validasi | Polinema PSDKU Kediri')
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">{{Auth::user()->roles}}</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Validasi</a></li>
                            <li class="breadcrumb-item active" aria-current="page">sebelum</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">
                                Daftar pinjaman Disetujui
                            </h4>
                        </div>
                    </div>
                    <p class="card-description">
                        Daftar pinjaman menunggu validasi {{Auth::user()->roles}}
                    </p>
                </div>
                <div class="table-responsive py-4">
                    <table class="table table-flush" id="datatable-buttons">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Nama Peminjam</th>
                                <th>Keperluan</th>
                                <th>Tanggal Kegiatan</th>
                                <th>Status</th>
                                <th>Validasi</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($lolos as $data)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $data->user->name }}</td>
                                <td>{{ $data->keperluan }}</td>
                                <td>{{ date('d F Y', strtotime($data->tanggal_pinjam)) }}</td>
                                <td>
                                    @if(Auth::user()->roles == 'KTU')

                                    @if($data->validasi_ktu == 0)
                                    <label class="badge badge-warning shadow">Pendding</label>
                                    @elseif($data->validasi_ktu == 1)
                                    <label class="badge badge-success shadow">Lolos</label>
                                    @elseif($data->validasi_ktu == 2)
                                    <label class="badge badge-danger shadow">Tolak</label>
                                    @endif

                                    @elseif(Auth::user()->roles == 'Koordinator')

                                    @if($data->validasi_koor == 0)
                                    <label class="badge badge-warning shadow">Pendding</label>
                                    @elseif($data->validasi_koor == 1)
                                    <label class="badge badge-success shadow">Lolos</label>
                                    @elseif($data->validasi_koor == 2)
                                    <label class="badge badge-danger shadow">Tolak</label>
                                    @endif

                                    @endif
                                </td>
                                <td>
                                    @if(Auth::user()->roles == 'KTU')
                                    @if($data->validasi_koor != 0)
                                    @else
                                    <form action="/pinjam/{{$data->id}}" method="post">
                                        @csrf
                                        @method('put')
                                        <input type="hidden" name="after_validasi_ktu" value="2">
                                        <button type="submit" class="btn btn-danger btn-sm">Tolak</button>
                                    </form>
                                    @endif
                                    @elseif(Auth::user()->roles == 'Koordinator')
                                    @if($data->validasi_bmn != 0)
                                    @else
                                    <form action="/pinjam/{{$data->id}}" method="post">
                                        @csrf
                                        @method('put')
                                        <input type="hidden" name="after_validasi_koor" value="2">
                                        <button type="submit" class="btn btn-danger btn-sm">Tolak</button>
                                    </form>
                                    @endif
                                    @endif
                                </td>
                                <td>
                                    <a href="/pinjam/{{$data->id}}" class="btn btn-primary btn-sm">Detail</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <p class="card-description">
                        Daftar pinjaman Ditolak
                    </p>
                    <div class="table-responsive py-4">
                        <table class="table table-flush" id="datatable-basic">
                            <thead class="thead-light">
                                <tr>
                                    <th>#</th>
                                    <th>Nama Peminjam</th>
                                    <th>Keperluan</th>
                                    <th>Tanggal Kegiatan</th>
                                    <th>Status</th>
                                    <th>Validasi</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($tidak as $data)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $data->user->name }}</td>
                                    <td>{{ $data->keperluan }}</td>
                                    <td>{{ date('d F Y', strtotime($data->tanggal_pinjam)) }}</td>
                                    <td>
                                        @if(Auth::user()->roles == 'KTU')

                                        @if($data->validasi_ktu == 0)
                                        <label class="badge badge-warning shadow">Pendding</label>
                                        @elseif($data->validasi_ktu == 1)
                                        <label class="badge badge-success shadow">Lolos</label>
                                        @elseif($data->validasi_ktu == 2)
                                        <label class="badge badge-danger shadow">Tolak</label>
                                        @endif

                                        @elseif(Auth::user()->roles == 'Koordinator')

                                        @if($data->validasi_koor == 0)
                                        <label class="badge badge-warning shadow">Pendding</label>
                                        @elseif($data->validasi_koor == 1)
                                        <label class="badge badge-success shadow">Lolos</label>
                                        @elseif($data->validasi_koor == 2)
                                        <label class="badge badge-danger shadow">Tolak</label>
                                        @endif

                                        @endif
                                    </td>
                                    <td>
                                        @if(Auth::user()->roles == 'KTU')
                                        <form action="/pinjam/{{$data->id}}" method="post">
                                            @csrf
                                            @method('put')
                                            <input type="hidden" name="after_validasi_ktu" value="1">
                                            <button type="submit" class="btn btn-success btn-sm">Setuju</button>
                                        </form>
                                        @elseif(Auth::user()->roles == 'Koordinator')
                                        <form action="/pinjam/{{$data->id}}" method="post">
                                            @csrf
                                            @method('put')
                                            <input type="hidden" name="after_validasi_koor" value="1">
                                            <button type="submit" class="btn btn-success btn-sm">Setuju</button>
                                        </form>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="/pinjam/{{$data->id}}" class="btn btn-primary btn-sm">Detail</a>
                                    </td>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @endsection
    @push('script')
    {!! Toastr::message() !!}
    @endpush
    @push('style')
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">
    @endpush