@extends('layouts.index')
@push('title', 'Edit | PSDKU Kota Kediri')
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">BMN</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Pinjam</a></li>
                            <li class="breadcrumb-item active" aria-current="page">details</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-md-4">
            <div class="card card-profile">
                <img src="{{url('assets/img/theme/img-1-1000x600.jpg')}}" alt="Image placeholder" class="card-img-top">
                <div class="row justify-content-center mb-4">
                    <div class="col-lg-4 order-lg-4">
                        <div class="card-profile-image">
                            <a href="#">
                                <img src="https://ui-avatars.com/api/?name={{$pinjam->user->name}}" style="width: 10rem;" class="rounded-circle">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="card-body mt-4">
                    <div class="text-center">
                        <h5 class="h3">
                            {{ $pinjam->user->name }}
                        </h5>
                        <div class="h5 font-weight-300">
                            <i class="ni location_pin mr-2"></i>
                            <h6>{{$pinjam->user->roles}}</h6>
                        </div>
                        <p>
                            Daftar pinjaman di tampilkan dalam table,
                        </p>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <small class="text-muted">Alamat email</small>
                            <h6>{{$pinjam->user->email}}</h6>
                            <small class="text-muted">Nim/Nidn</small>
                            <h6>{{$pinjam->user->nim_nidn}}</h6>
                            <small class="text-muted">Status</small>
                            <h6>{{$pinjam->user->status_mhs}}</h6>
                            <small class="text-muted">Jenis Kelamin</small>
                            <h6>{{$pinjam->user->jenis_kelamin}}</h6>
                        </div>
                        <div class="col-md-6">
                            <small class="text-muted">Jalan</small>
                            <h6>{{$pinjam->user->alamat}}</h6>
                            <small class="text-muted pt-4 db">Rt</small>
                            <h6>{{$pinjam->user->rt}}</h6>
                            <small class="text-muted pt-4 db">Rw</small>
                            <h6>{{$pinjam->user->rw}}</h6>
                            <small class="text-muted pt-4 db">No Telepon</small>
                            <h6>{{$pinjam->user->no_telp}}</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-8 mb-3 mb-lg-0">
            <div class="card">
                <div class="card-body">
                    <form action="/pinjam/update/{{$pinjam->id}}" method="post" enctype="multipart/form-data">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="kep"> <code>*</code>Keperluan</label>
                            <input type="text" name="keperluan" class="form-control @error('keperluan') is-invalid @enderror" value="{{ $pinjam->keperluan}}" id="kep">
                            @error('keperluan')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="tgl"> <code>*</code>Tanggal Kegatan</label>
                            <input type="date" name="tanggal_pinjam" class="form-control @error('tanggal_pinjam') is-invalid @enderror" value="{{$pinjam->tanggal_pinjam}}" id="tgl">
                            @error('tanggal_pinjam')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>Proposal Lama</label><br>
                                    <input type="hidden" name="old_proposal" value="{{ $pinjam->proposal }}">
                                    <a href="/storage/{{$pinjam->proposal}}" target="_blank" rel="noopener noreferrer">
                                        <button type="button" class="btn btn-outline-success"> <i class="mdi mdi-file"></i> Proposal</button>
                                    </a>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Proposal Baru</label>
                                        <div class="custom-file">
                                            <input type="file" name="proposal" class="form-control @error('proposal') is-invalid @enderror">
                                        </div>
                                        @error('proposal')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary mt-2">Simpan</button>
                        <hr>
                    </form>
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">
                                Daftar Sarpras »
                            </h4>
                        </div>
                        <div class="col-auto">
                            <a href="/pinjam/{{$pinjam->id}}/draf" class="btn btn-primary">
                                <i class="mdi mdi-calendar-plus"></i>
                                Add Sarpras
                            </a>
                        </div>
                    </div>
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr style="font-weight: bold; color: black;">
                                    <th>#</th>
                                    <th>Nama Sarpras</th>
                                    <th>Jumlah</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pinjam->draf as $data)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $data->sarpras->nama }}</td>
                                    <td>{{ $data->qty }}</td>
                                    <td>
                                        <button data-toggle="modal" id="show" data-nama_item="{{ $data->sarpras->nama }}" data-img="{{ $data->sarpras->photo }}" data-target="#exampleModal" class="btn btn-primary btn-icon-only rounded-circle">
                                            <i class="ni ni-album-2" data-toggle="tooltip" title="Photo"></i>
                                        </button>
                                        <button data-toggle="modal" data-target="#ModalEdit" data-pinjam_id="{{$data->pinjam->id}}" data-id="{{$data->id}}" data-sarpras="{{$data->sarpras->id}}" data-img="{{$data->sarpras->photo}}" data-jumlah="{{$data->sarpras->jumlah}}" data-nama="{{$data->sarpras->nama}}" data-qty="{{$data->qty}}" id="edit" class="btn btn-warning btn-icon-only rounded-circle">
                                            <i class="ni ni-settings" data-toggle="tooltip" title="Edit"></i>
                                        </button>
                                        <form action="/draf/{{$data->id}}/pinjam" method="post" style="display: inline;">
                                            @csrf
                                            @method('delete')
                                            <input type="hidden" name="pinjam_id" value="{{$pinjam->id}}">
                                            <button type="submit" class="btn btn-danger btn-icon-only rounded-circle">
                                                <i class="ni ni-button-power" onclick="return confirm('Yakin hapus data ini?')" data-toggle="tooltip" title="Delete"></i>
                                            </button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection


    @push('script')
    {!! Toastr::message() !!}
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" style="color: black;" id="exampleModalLabel"> </h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img id="img" src="" style="width: 29rem;" alt="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="ModalEdit" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title judul" style="color: black;" id="exampleModalLabel"> </h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <img id="img_sar" src="" style="width: 29rem;" alt="">
                    <h5 class="project-name mt-4" id="nama"></h5>
                    <p class="project-category" id="qty-jumlah"></p>

                    <div class="btn-wrapper text-center sarpras_data">
                        <form id="pinjam_id" action="" method="post">
                            @csrf
                            @method('put')
                            <div class="input-group mb-3 mt-3 " style="margin-left: 155px; width: 130px;">
                                <div class="input-group quantity">
                                    <div class="input-group-prepend decrement-btn" style="cursor: pointer">
                                        <span class="input-group-text">-</span>
                                    </div>
                                    <input type="hidden" name="sarpras_id" id="sarpras_id">
                                    <input type="hidden" id="qty-max" class="qty-max">
                                    <input type="hidden" name="draf_id" id="draf_id">
                                    <input type="text" name="qty" readonly class="qty-input form-control text-center" id="qty-modal">
                                    <div class="input-group-append increment-btn" style="cursor: pointer">
                                        <span class="input-group-text">+</span>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary EditQty mr-2">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $(document).on('click', '#show', function() {
                var nama = $(this).data('nama_item');
                var img = $(this).data('img');
                var img = $(this).data('img');

                $('.modal-title').text("Gambar " + nama);
                $('#img').attr("src", '/storage/' + img);
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $(document).on('click', '#edit', function() {
                var id = $(this).data('id');
                var pinjam_id = $(this).data('pinjam_id');
                var sarpras_id = $(this).data('sarpras');
                var nama = $(this).data('nama');
                var jumlah = $(this).data('jumlah');
                var img = $(this).data('img');
                var qty = $(this).data('qty');

                $('#draf_id').val(id);
                $('#pinjam_id').attr("action", '/pinjam/' + pinjam_id + '/draf');
                $('#sarpras_id').val(sarpras_id);
                $('#nama').text(nama);
                $('#qty-jumlah').text('Jumlah ' + jumlah);
                $('#qty-max').val(jumlah);
                $('#img_sar').attr("src", '/storage/' + img);
                $('#qty-modal').val(qty);
            });
        });
        $('.increment-btn').click(function(e) {
            e.preventDefault();
            var incre_value = $(this).parents('.quantity').find('.qty-input').val();
            var max_value = $(this).parents('.quantity').find('.qty-max').val();
            var value = parseInt(incre_value, max_value);
            value = isNaN(value) ? 0 : value;
            if (value < max_value) {
                value += 1;
                $(this).parents('.quantity').find('.qty-input').val(value);
            }

        });

        $('.decrement-btn').click(function(e) {
            e.preventDefault();
            var decre_value = $(this).parents('.quantity').find('.qty-input').val();
            var max_value = $(this).parents('.quantity').find('.qty-max').val();
            var value = parseInt(decre_value, max_value);
            value = isNaN(value) ? 0 : value;
            if (value > 1) {
                value--;
                $(this).parents('.quantity').find('.qty-input').val(value);
            }
        });
    </script>
    @endpush