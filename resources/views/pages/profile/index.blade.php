@extends('layout.index')
@push('title', 'Profile')
@section('content')
<main class="about-page">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <style>
                    .banner {
                        position: absolute;
                        top: 0;
                        left: 0;
                        width: 100%;
                        height: 125px;
                        background-image: url("../menu/images/banner.jpg");
                        background-position: center;
                        background-size: cover;
                    }

                    .img-circle {
                        height: 150px;
                        width: 150px;
                        border-radius: 150px;
                        border: 3px solid #fff;
                        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
                        z-index: 1;
                    }

                    .edit-links a {
                        transition: all 0.2s;
                    }

                    .edit-links a:hover {
                        transform: translateY(-3px);
                    }
                </style>
                <div class="profile-card card rounded-lg shadow p-4 p-xl-5 mb-4 text-center position-relative overflow-hidden">
                    <div class="banner"></div>
                    <img src="https://ui-avatars.com/api/?name={{ Auth::user()->name }}" alt="" class="img-circle mx-auto mb-3">
                    <h3 class="mb-4">{{ Auth::user()->name }}</h3>
                    <div class="text-left mb-4">
                        <p class="mb-2"><i class="ti-email mr-2"></i> {{ Auth::user()->email }}</p>
                        <p class="mb-2"><i class="ti-id-badge mr-2"></i> {{ Auth::user()->no_telp }}</p>
                        <p class="mb-2"><i class="ti-settings mr-2"></i> {{ Auth::user()->roles }}</p>
                        <p class="mb-2"><i class="ti-map-alt mr-2"></i> {{ Auth::user()->alamat }}</p>
                    </div>
                    <div class="edit-links d-flex justify-content-center">
                        <a href="#!" class="btn btn-warning mx-2">Edit Profile</a>
                        <a href="#!" class="btn btn-danger">Edit Password</a>
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body shadow">
                        <p class="card-description">
                            Data Permohonan Pinjam
                        </p>
                        <div class="table-responsive">
                            <table class="table table-hover" id="example1">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Keperluan</th>
                                        <th>Jumlah Sarpras</th>
                                        <th>Status</th>
                                        <th colspan="3">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($permohonan as $data)
                                    <tr>
                                        <th>{{ $loop->iteration + $permohonan->firstItem() - 1 }}</th>
                                        <td>{{ $data->keperluan }}</td>
                                        <td>{{ count($data->draf) }}</td>
                                        <td>
                                            @if( $data->validasi_ktu == 1 && $data->validasi_koor == 1 && $data->validasi_bmn == 1 && $data->status == 0 )
                                            <label class="badge badge-success shadow">Disetujui</label>
                                            @elseif( $data->validasi_ktu == 1 && $data->validasi_koor == 1 && $data->validasi_bmn == 1 && $data->status == 1)
                                            <label class="badge badge-warning shadow">Dibawa</label>
                                            @elseif( $data->validasi_ktu == 1 && $data->validasi_koor == 1 && $data->validasi_bmn == 0 )
                                            <label class="badge badge-light shadow">Seleksi BMN</label>
                                            @elseif( $data->validasi_ktu == 1 && $data->validasi_koor == 0 && $data->validasi_bmn == 0 )
                                            <label class="badge badge-light shadow">Seleksi Koordinator</label>
                                            @elseif( $data->validasi_ktu == 0 && $data->validasi_koor == 0 && $data->validasi_bmn == 0 )
                                            <label class="badge badge-light shadow">Seleksi TU</label>
                                            @elseif( $data->validasi_ktu == 1 && $data->validasi_koor == 1 && $data->validasi_bmn == 2 )
                                            <label class="badge badge-danger shadow">Tolak BMN</label>
                                            @elseif( $data->validasi_ktu == 1 && $data->validasi_koor == 2 && $data->validasi_bmn == 0 )
                                            <label class="badge badge-danger shadow">Tolak Koornonator</label>
                                            @elseif( $data->validasi_ktu == 2 && $data->validasi_koor == 0 && $data->validasi_bmn == 0 )
                                            <label class="badge badge-danger shadow">Tolak TU</label>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="/profile/{{$data->id}}" class="btn btn-info">Detail</a>
                                        </td>
                                        @if($data->validasi_ktu == 1 && $data->validasi_koor == 1 && $data->validasi_bmn == 1)
                                        <td>
                                            <form action="/draf/print" method="post" target="_blank" rel="noopener noreferrer">
                                                @csrf
                                                <input type="hidden" name="id" value="{{$data->id}}">
                                                <button type="submit" class="btn btn-success">Print</button>
                                            </form>
                                        </td>
                                        @endif
                                        @if($data->validasi_ktu == 0)
                                        <td>
                                            <a href="/profile/{{$data->id}}/edit" class="btn btn-warning">Edit</a>
                                        </td>
                                        <td>
                                            <form action="/profile/{{$data->id}}" method="post">
                                                @csrf
                                                @method('delete')
                                                <input type="hidden" name="proposal" value="{{$data->proposal}}">
                                                <input type="submit" onclick="return confirm('Yakin ingin Hapus ini?')" class="btn btn-danger" value="Hapus">
                                            </form>
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!! $permohonan->links() !!}
                        </div>
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-body shadow">
                        <p class="card-description">
                            Data Pinjam
                        </p>
                        <div class="table-responsive">
                            <table class="table table-hover" id="example2">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Keperluan</th>
                                        <th>Jumlah Sarpras</th>
                                        <th>Status</th>
                                        <th colspan="2">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($bawa as $data)
                                    <tr>
                                        <td>{{ $loop->iteration + $bawa->firstItem() - 1 }}</td>
                                        <td>{{ $data->pinjam->keperluan }}</td>
                                        <td>{{ count($data->pinjam->draf) }}</td>
                                        <td>
                                            @if($data->status == 0)
                                            <label class="badge badge-primary shadow">Tanggungan</label>
                                            @elseif($data->status == 1)
                                            <label class="badge badge-success shadow">Success</label>
                                            @elseif($data->status == 2)
                                            <label class="badge badge-danger shadow">Kerusakan</label>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="/tanggungan/{{$data->id}}" class="btn btn-info">Detail</a>
                                        </td>
                                        @if($data->status==2)
                                        <td>
                                            <!-- <a href="/kembali/print" class="btn btn-success" target="_blank" rel="noopener noreferrer">Print</a> -->
                                            <a href="/storage/tanggungan/SURAT%20PERBAIKAN%20ALAT.pdf" class="btn btn-success" target="_blank" rel="noopener noreferrer">Unduh</a>
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            {!! $bawa->links() !!}
                        </div>
                    </div>
                </div>
                <br>
                <div class="card">
                    <div class="card-body shadow">
                        <p class="card-description">
                            Data Pengembalian
                        </p>
                        <div class="table-responsive">
                            <table class="table table-hover" id="example2">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Keperluan</th>
                                        <th>Jumlah Sarpras</th>
                                        <th>Status</th>
                                        <th colspan="2">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pengembalian as $data)
                                    <tr>
                                        <td>{{ $loop->iteration+ $pengembalian->firstItem() - 1  }}</td>
                                        <td>{{ $data->pinjam->keperluan }}</td>
                                        <td>{{ count($data->pinjam->draf) }}</td>
                                        <td>
                                            @if($data->status == 0)
                                            <label class="badge badge-primary shadow">Tanggungan</label>
                                            @elseif($data->status == 1)
                                            <label class="badge badge-success shadow">Success</label>
                                            @elseif($data->status == 2)
                                            <label class="badge badge-danger shadow">Kerusakan</label>
                                            @endif
                                        </td>
                                        <td>
                                            <a href="/dikembalikan/{{$data->id}}" class="btn btn-info">Detail</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<br>
@endsection
@push('script')
{!! Toastr::message() !!}
@endpush