@extends('layout.index')
@push('title', 'Profile')
@section('content')
<main class="about-page">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body shadow">
                        <p class="card-description">
                            Daftar permohonan sarpras yang dipinjam
                        </p>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Sarpras</th>
                                        <th>Jumlah</th>
                                        @if($pinjam->validasi_ktu == 0)
                                        <th colspan="3">Action</th>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($pinjam->draf as $data)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $data->sarpras->nama }}</td>
                                        <td>{{ $data->qty }}</td>
                                        @if($pinjam->validasi_ktu == 0)
                                        <td>
                                            <a class="btn btn-primary" id="edit" data-pinjam_id="{{$data->pinjam->id}}" data-id="{{$data->id}}" data-sarpras="{{$data->sarpras->id}}" data-img="{{$data->sarpras->photo}}" data-jumlah="{{$data->sarpras->jumlah}}" data-nama="{{$data->sarpras->nama}}" data-qty="{{$data->qty}}" data-toggle="modal" data-target="#exampleModal">
                                                Edit
                                            </a>
                                            <form action="/profile/{{$data->id}}/draf" method="post" style="display: inline;">
                                                @csrf
                                                @method('delete')
                                                <input type="hidden" name="pinjam_id" value="{{$data->pinjam->id}}">
                                                <input type="submit" onclick="return confirm('Yakin hapus draf ini?')" class="btn btn-danger" value="Hapus" />
                                            </form>
                                        </td>
                                        @endif
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body shadow">
                        <p class="card-description">Data Permohonan</p>
                        <div class="form-group">
                            <h6 class="oles-input">{{date('d F Y', strtotime($pinjam->tanggal_pinjam))}}</h6>
                        </div>
                        <div class="form-group">
                            <label> <b style="color: brown;">*</b> Keperluan</label>
                            <h6 class="oles-input">{{ $pinjam->keperluan }}</h6>
                        </div>
                        <div class="form-group">
                            <label> <b style="color: brown;">*</b> Proposal</label><br>
                            <a href="/storage/{{ $pinjam->proposal }}" target="_blank" rel="noopener noreferrer">
                                <button type="button" class="btn btn-outline-success"> <i class="ti-file"></i> Proposal</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<br>
@endsection

@push('script')
<!-- Modal  -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Jumlah</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="portfolio-card wow fadeInUp">
                    <div class="project-thumbnail-wrapper">
                        <img id="img" src="" style="width: 29rem;" alt="portffolio" class="project-thumbnail">
                    </div>
                    <h5 class="project-name mt-4" id="nama"></h5>
                    <p class="project-category" id="qty-jumlah"></p>

                    <div class="btn-wrapper text-center sarpras_data">
                        <form id="pinjam_id" action="" method="post">
                            @csrf
                            @method('put')
                            <div class="input-group mb-3 mt-3 " style="margin-left: 165px; width: 130px;">
                                <div class="input-group quantity">
                                    <div class="input-group-prepend decrement-btn" style="cursor: pointer">
                                        <span class="input-group-text">-</span>
                                    </div>
                                    <input type="hidden" name="sarpras_id" id="sarpras_id">
                                    <input type="hidden" id="qty-max" class="qty-max">
                                    <input type="hidden" name="draf_id" id="draf_id">
                                    <input type="text" name="qty" readonly class="qty-input form-control" id="qty-modal">
                                    <div class="input-group-append increment-btn" style="cursor: pointer">
                                        <span class="input-group-text">+</span>
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary EditQty mr-2">Simpan</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $(document).on('click', '#edit', function() {
            var id = $(this).data('id');
            var pinjam_id = $(this).data('pinjam_id');
            var sarpras_id = $(this).data('sarpras');
            var nama = $(this).data('nama');
            var jumlah = $(this).data('jumlah');
            var img = $(this).data('img');
            var qty = $(this).data('qty');

            $('#draf_id').val(id);
            $('#pinjam_id').attr("action", '/profile/' + pinjam_id + '/draf');
            $('#sarpras_id').val(sarpras_id);
            $('#nama').text(nama);
            $('#qty-jumlah').text('Jumlah ' + jumlah);
            $('#qty-max').val(jumlah);
            $('#img').attr("src", '/storage/' + img);
            $('#qty-modal').val(qty);
        });

        $('.increment-btn').click(function(e) {
            e.preventDefault();
            var incre_value = $(this).parents('.quantity').find('.qty-input').val();
            var max_value = $(this).parents('.quantity').find('.qty-max').val();
            var value = parseInt(incre_value, max_value);
            value = isNaN(value) ? 0 : value;
            if (value < max_value) {
                value += 1;
                $(this).parents('.quantity').find('.qty-input').val(value);
            }

        });

        $('.decrement-btn').click(function(e) {
            e.preventDefault();
            var decre_value = $(this).parents('.quantity').find('.qty-input').val();
            var max_value = $(this).parents('.quantity').find('.qty-max').val();
            var value = parseInt(decre_value, max_value);
            value = isNaN(value) ? 0 : value;
            if (value > 1) {
                value--;
                $(this).parents('.quantity').find('.qty-input').val(value);
            }
        });
    });
</script>

@endpush