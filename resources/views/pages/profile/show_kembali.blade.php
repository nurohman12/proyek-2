@extends('layout.index')
@push('title', 'Profile')
@section('content')
<main class="about-page">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-body shadow">
                        <p class="card-description">
                            Daftar sarpras yang sudah dikembalikan
                        </p>
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Nama Sarpras</th>
                                        <th>Jumlah</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($dikembalikan->pinjam->draf as $data)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $data->sarpras->nama }}</td>
                                        <td>{{
                                            App\Models\Sarpras_Masuk::where('draf_id', $data->id)->sum('jumlah')
                                         }}</td>
                                        <td>
                                            @if($data->kondisi == 0)
                                            <span class="badge badge-warning">Dibawa</span>
                                            @elseif($data->kondisi == 1)
                                            <span class="badge badge-success">Dikembalikan</span>
                                            @elseif($data->kondisi == 2)
                                            <span class="badge badge-danger">{{$data->keterangan}}</span>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-body shadow">
                        <p class="card-description">Data Permohonan</p>
                        <label for=""> <b style="color: brown;">*</b>Tanggal</label>
                        <div class="row form-group">
                            <div class="col-md-6">
                                <label for=""> Permohonan</label>
                                <h6 class="oles-input">{{date('d F Y', strtotime($dikembalikan->pinjam->tanggal_pinjam))}}</h6>
                            </div>
                            <div class="col-md-6">
                                <label for=""> Ambil</label>
                                <h6 class="oles-input">{{date('d F Y', strtotime($dikembalikan->date_ambil))}}</h6>
                            </div>
                        </div>
                        <div class="form-group">
                            <label> <b style="color: brown;">*</b> Keperluan</label>
                            <h6 class="oles-input">{{ $dikembalikan->pinjam->keperluan }}</h6>
                        </div>
                        <div class="form-group">
                            <label> <b style="color: brown;">*</b> Proposal</label><br>
                            <a href="/storage/{{ $dikembalikan->pinjam->proposal }}" target="_blank" rel="noopener noreferrer">
                                <button type="button" class="btn btn-outline-success"> <i class="ti-file"></i> Proposal</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<br>
@endsection