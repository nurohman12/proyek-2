@extends('layouts.index')
@push('title', 'Sarpras | Polinema PSDKU Kediri')
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">BMN</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Sarpras</a></li>
                            <li class="breadcrumb-item active" aria-current="page">sarpras</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">
                                Data Sarpras »
                            </h4>
                        </div>
                        <div class="col-auto">
                            <a href="/sarpras/create" class="btn btn-primary btn-sm">
                                Tambah Sarpras
                            </a>
                        </div>
                    </div>
                </div>
                <div class="table-responsive py-4">
                    <table class="table table-flush" id="datatable-basic">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Jenis</th>
                                <th>Nama</th>
                                <th>Jumlah</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($sarpras as $data)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $data->jenis }}</td>
                                <td>{{ $data->nama }}</td>
                                <td>{{ $data->jumlah }}</td>
                                <td>
                                    <a href="/sarpras/{{$data->id}}" class="btn btn-success btn-sm">Detail</a>
                                    <a href="/sarpras/{{$data->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
                                    <form action="/sarpras/{{$data->id}}" method="post" style="display: inline;">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin hapus data ini?')">
                                            Hapus
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('script')
    {!! Toastr::message() !!}
    @endpush
    @push('style')
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">
    @endpush