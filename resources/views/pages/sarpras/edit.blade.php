@extends('layouts.index')
@push('title', 'Sarpras | Polinema PSDKU Kediri')
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">BMN</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Pengguna</a></li>
                            <li class="breadcrumb-item active" aria-current="page">edit</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-md-12">
            <div class="card-wrapper">
                <!-- Input groups -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">Ubah Data Sarpras</h3>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="/sarpras/{{$sarpras->id}}" enctype="multipart/form-data">
                            @csrf
                            @method('put')
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Nama</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-project-diagram"></i></span>
                                            </div>
                                            <input class="form-control @error('nama') is-invalid @enderror" value="{{ $sarpras->nama }}" name="nama" type="text">
                                        </div>
                                        @error('nama')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Jenis</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-filter"></i></i></span>
                                            </div>
                                            <select name="jenis" id="jenis" class="form-control @error('jenis') is-invalid @enderror">
                                                <option value="" selected disabled>Jenis</option>
                                                <option value="Barang" {{ $sarpras->jenis == 'Barang' ? 'selected' : null }}>Barang</option>
                                                <option value="Ruangan" {{ $sarpras->jenis == 'Ruangan' ? 'selected' : null }}>Ruangan</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Kegunaan</label>
                                        <div class="input-group input-group-merge">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-funnel-dollar"></i></span>
                                            </div>
                                            <input class="form-control @error('fungsi') is-invalid @enderror" value="{{ $sarpras->fungsi }}" name="fungsi" type="text">
                                        </div>
                                        @error('fungsi')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Deskripsi</label>
                                        <textarea name="deskripsi" rows="8" class="form-control @error('deskripsi') is-invalid @enderror">{{ $sarpras->deskripsi }}</textarea>
                                        @error('deskripsi')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="">Photo</label>
                                        <input type="hidden" name="old_photo" value="{{ $sarpras->photo }}">
                                        <input type="file" class="form-control @error('photo') is-invalid @enderror" id="photo" name="photo" onchange="previewImage(this)">
                                        <img src="{{  url('/storage/'. $sarpras->photo) }}" id="previewkk" style="max-width: 250px; margin-top: 10px;">
                                        @error('photo')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary mr-2">Simpan</button>
                            <button type="reset" class="btn btn-secondary mr-2">Batal</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('script')
    <script>
        function previewImage(input) {
            var file = $("input[type=file]").get(0).files[0];
            if (file) {
                var reader = new FileReader();
                reader.onload = function() {
                    $('#previewkk').attr("src", reader.result);
                }
                reader.readAsDataURL(file);
            }
        }
    </script>
    @endpush