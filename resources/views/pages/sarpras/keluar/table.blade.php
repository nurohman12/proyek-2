<table class="table table-flush" id="datatable-buttons">
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>Nama Sarpras</th>
            <!-- <th>Pengguna</th> -->
            <th>Jumlah</th>
            <th>Tanggal</th>
            <th>Keterangan</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($sarpras_keluar as $data)
        <tr>
            <td>{{ $loop->iteration}}</td>
            <td>{{ $data->sarpras->nama }}</td>
            <!-- <td>{{ $data->user->name }}</td> -->
            <td>{{ $data->jumlah }}</td>
            <td>{{date('d F Y', strtotime($data->tanggal_keluar))}}</td>
            <td>
                @if($data->draf->keterangan != null)
                <span class="badge badge-info">
                    {{$data->draf->keterangan}}
                </span>
                @else
                <span class="badge badge-warning">
                    Dipinjam
                </span>
                @endif
            </td>
            <td>
                <a href="/sarpras_keluar/{{$data->id}}" class="btn btn-success btn-sm">Detail</a>
                <a class="btn btn-warning btn-rounded btn-sm" id="edit" data-id="{{$data->id}}" data-draf_id="{{$data->draf_id}}" data-tanggal="{{$data->tanggal_keluar}}" data-jumlah="{{$data->jumlah}}" data-keterangan="{{$data->draf->keterangan}}" data-sarpras_id="{{ $data->sarpras_id }}" data-nama="{{ $data->sarpras->nama }}" data-img="{{ $data->sarpras->photo }}" data-toggle="modal" data-target="#exampleModal">
                    Edit
                </a>
                <form action="/sarpras_keluar/{{$data->id}}" method="post" style="display: inline;">
                    @csrf
                    @method('delete')
                    <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin hapus data ini?')" data-toggle="tooltip" title="Delete">
                        Hapus
                    </button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

{!! Toastr::message() !!}