@extends('layouts.index')
@push('title', 'Sarpras Masuk | Polinema PSDKU Kediri')
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">BMN</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Sarpras</a></li>
                            <li class="breadcrumb-item active" aria-current="page">masuk</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <img src="{{ url('/storage/'. $sarpras_masuk->sarpras->photo) }}" id="previewkk" style="max-width: 350px; margin-top: 5px;">
                            </div>
                        </div>
                        <div class="col-md-8">
                            <ul class="list-arrow">
                                <h4 class="display-3">{{ $sarpras_masuk->sarpras->nama }}</h4>
                                <li>
                                    <label for="">Pengguna</label>
                                    <p class="ml-1">{{ $sarpras_masuk->user->name }}</p>
                                </li>
                                <li>
                                    <label for="">Tanggal masuk</label>
                                    <p class="ml-1">{{ date('d F Y', strtotime($sarpras_masuk->tanggal_masuk)) }}</p>
                                </li>
                                <li>
                                    <label for="">Jumlah</label>
                                    <p class="ml-1">{{ $sarpras_masuk->jumlah }}</p>
                                </li>
                                @if($sarpras_masuk->draf->keterangan != '')
                                <li>
                                    <label for="">Keterangan</label>
                                    <p class="ml-1">{{ $sarpras_masuk->draf->keterangan }}</p>
                                </li>
                                @else
                                <li>
                                    <label for="">Keterangan</label>
                                    <p class="ml-1">Dikembalikan</p>
                                </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection