@extends('layouts.index')
@push('title', 'Sarpras Masuk | Polinema PSDKU Kediri')
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">BMN</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Sarpras</a></li>
                            <li class="breadcrumb-item active" aria-current="page">masuk</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">
                                Data Sarpras Masuk »
                            </h4>
                        </div>
                        <div class="col-auto">
                            <a href="/sarpras_masuk/create" class="btn btn-primary btn-sm">
                                Tambah Sarpras Masuk
                            </a>
                        </div>
                    </div>
                    <p class="card-description">
                        Daftar sarpras masuk yang ada pada sistem
                    </p>
                </div>
                <div class="table-responsive py-4" id="content">
                    @include('pages.sarpras.masuk.table')
                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('script')
    {!! Toastr::message() !!}
    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h3 class="modal-title" style="color: black;" id="exampleModalLabel"> </h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-6">
                            <img id="img" src="" style="width: 29rem;" alt="">
                        </div>
                        <div class="col-md-5">
                            <div class="form-group">
                                <label for="tanggal">Tanggal</label>
                                <input type="date" class="form-control tanggal" id="tanggal">
                                <input type="hidden" class="id" id="id">
                                <input type="hidden" class="sarpras_id" id="sarpras_id">
                                <input type="hidden" class="draf_id" id="draf_id">
                            </div>
                            <div class="form-group">
                                <label for="jumlah">Jumlah</label>
                                <input type="number" class="form-control jumlah" id="jumlah">
                                <input type="hidden" class="jumlah" id="old_jumlah">
                            </div>
                            <div class="form-group">
                                <label for="keterangan">Keterangan</label>
                                <input type="text" class="form-control keterangan" id="keterangan">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="update">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $(document).on('click', '#update', function() {
                const id = $('#id').val();
                const sarpras_id = $('#sarpras_id').val();
                const draf_id = $('#draf_id').val();
                const tanggal = $('#tanggal').val();
                const jumlah = $('#jumlah').val();
                const old_jumlah = $('#old_jumlah').val();
                const keterangan = $('#keterangan').val();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: '/sarpras_masuk',
                    type: 'PUT',
                    data: {
                        id: id,
                        sarpras_id: sarpras_id,
                        draf_id: draf_id,
                        tanggal: tanggal,
                        jumlah: jumlah,
                        old_jumlah: old_jumlah,
                        keterangan: keterangan,
                    },
                    success: function(data) {
                        $('#content').html(data);
                    }
                })
            });
            $(document).on('click', '#edit', function() {
                var nama_sarpras = $(this).data('nama');
                var img_sarpras = $(this).data('img');
                var keterangan_draf = $(this).data('keterangan');
                var id = $(this).data('id');
                var sarpras_id = $(this).data('sarpras_id');
                var draf_id = $(this).data('draf_id');
                var jumlah = $(this).data('jumlah');
                var tanggal = $(this).data('tanggal');

                $('.modal-title').text("Edit " + nama_sarpras);
                $('#img').attr('src', '/storage/' + img_sarpras);
                $('.keterangan').val(keterangan_draf);
                $('.id').val(id);
                $('.sarpras_id').val(sarpras_id);
                $('.draf_id').val(draf_id);
                $('.jumlah').val(jumlah);
                $('.tanggal').val(tanggal);
            });

            $(document).on('click', '.pagination a', function(e) {
                e.preventDefault();
                var page = $(this).attr('href').split('page=')[1];

                $.ajax({
                    type: "GET",
                    url: "/sarpras_masuk_get" + "?page=" + page,
                    success: function(data) {
                        $('#content').html(data);
                    }
                })
            });

        });
    </script>
    @endpush
    @push('style')
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">
    @endpush