@extends('layouts.index')
@push('title', 'Sarpras Masuk | Polinema PSDKU Kediri')
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">BMN</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Sarpras</a></li>
                            <li class="breadcrumb-item active" aria-current="page">masuk</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-md-6">
            <div class="card-wrapper">
                <!-- Input groups -->
                <div class="card">
                    <div class="card-header">
                        <h3 class="mb-0">Tambah Data Sarpras Masuk</h3>
                    </div>
                    <div class="card-body">
                        <form class="form-horizontal mt-4" method="POST" action="/sarpras_masuk">
                            @csrf
                            <div class="form-group">
                                <label for="jns">Sarpras</label>
                                <select name="sarpras" id="jns" class="form-control @error('sarpras') is-invalid @enderror">
                                    <option selected disabled>Sapras</option>
                                    @foreach($sarpras as $data)
                                    <option value="{{$data->id}}" data-stok="{{$data->jumlah}}" {{old('sarpras') == ' $data->id' ? 'selected' : null }}>{{$data->nama}}</option>
                                    @endforeach
                                </select>
                                @error('sarpras')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="rgl">Tanggal</label>
                                <input type="date" class="form-control @error('tanggal') is-invalid @enderror" value="{{ old('tanggal') }}" id="rgl" name="tanggal">
                                @error('tanggal')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="stok">Stok</label>
                                <input type="number" class="form-control" readonly id="stok">
                            </div>
                            <div class="form-group">
                                <label for="jum">Jumlah Masuk</label>
                                <input type="number" class="form-control @error('jumlah') is-invalid @enderror" id="jum" name="jumlah">
                                @error('jumlah')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="total_stok">Total Stok</label>
                                <input type="number" class="form-control" readonly id="total_stok">
                            </div>
                            <div class="form-group">
                                <label for="jumm">Keterangan</label>
                                <input type="text" class="form-control" value="{{ old('keterangan') }}" id="jumm" name="keterangan">
                            </div>
                            <button type="submit" class="btn btn-primary mr-2">Tambah</button>
                            <button type="reset" class="btn btn-secondary mr-2">Batal</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">
                                <u>
                                    Catatan »
                                </u>
                            </h4>
                        </div>
                    </div>
                    <blockquote class="blockquote blockquote-primary">
                        <p>Halaman ini digunakan mengatur jumlah sarpras apabila ada barang baru.</p>
                        <footer class="blockquote-footer">Bukan untuk mencatat pengembalian</footer>
                    </blockquote>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('script')
    <script type="text/javascript">
        $(document).ready(function() {
            $("#jns").change(function() {
                var stok = $(this).find(':selected').data("stok");

                $('#stok').val(stok);
                $('#jum').val('');
                $('#total_stok').val('');
            })
        });
        $('#jum').keyup(function() {
            var a = parseInt($('#stok').val());
            var b = parseInt($('#jum').val());
            var c = a + b;
            $('#total_stok').val(c);
        });
        $('#jum').click(function() {
            var a = parseInt($('#stok').val());
            var b = parseInt($('#jum').val());
            var c = a + b;
            $('#total_stok').val(c);
        });
    </script>
    @endpush