 <table class="table table-flush" id="datatable-basic">
     <thead class="thead-light">
         <tr>
             <th>#</th>
             <th>Nama Sarpras</th>
             <!-- <th>Pengguna</th> -->
             <th>Jumlah</th>
             <th>Tanggal</th>
             <th>Keterangan</th>
             <th>Action</th>
         </tr>
     </thead>
     <tbody>
         @foreach($sarpras_masuk as $data)
         <tr>
             <td>{{ $loop->iteration }}</td>
             <td>{{ $data->sarpras->nama }}</td>
             <!-- <td>{{ $data->user->name }}</td> -->
             <td>{{ $data->jumlah }}</td>
             <td>{{date('d F Y', strtotime($data->tanggal_masuk))}}</td>
             <td>
                 @if($data->draf->keterangan != '' && $data->draf->user->roles == Auth::user()->roles)
                 <span class="badge badge-warning">
                     {{$data->draf->keterangan}}
                 </span>
                 @elseif($data->draf->keterangan)
                 <span class="badge badge-success">
                     Dikembalikan
                 </span>
                 @else
                 <span class="badge badge-success">
                     Dikembalikan
                 </span>
                 @endif
             </td>
             <td>
                 <a href="/sarpras_masuk/{{$data->id}}" class="btn btn-success btn-sm">Detail</a>
                 <a class="btn btn-warning btn-rounded btn-sm" id="edit" data-id="{{$data->id}}" data-draf_id="{{$data->draf_id}}" data-tanggal="{{$data->tanggal_masuk}}" data-jumlah="{{$data->jumlah}}" data-keterangan="{{$data->draf->keterangan}}" data-sarpras_id="{{ $data->sarpras_id }}" data-nama="{{ $data->sarpras->nama }}" data-img="{{ $data->sarpras->photo }}" data-toggle="modal" data-target="#exampleModal">
                     Edit
                 </a>
                 <form action="/sarpras_masuk/{{$data->id}}" method="post" style="display: inline;">
                     @csrf
                     @method('delete')
                     <button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Yakin hapus data ini?')" data-toggle="tooltip" title="Delete">
                         Hapus
                     </button>
                 </form>
             </td>
         </tr>
         @endforeach
     </tbody>
 </table>
 <br>

 {!! Toastr::message() !!}