@extends('layouts.index')
@push('title', 'Sarpras | Polinema PSDKU Kediri')
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">BMN</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Pengguna</a></li>
                            <li class="breadcrumb-item active" aria-current="page">details</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="card">
            <div class="card-header">
                <h3 class="text-sm mb-0">
                    Detail Draf
                </h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <img src="{{ url('/storage/'. $sarpras->photo) }}" id="previewkk" style="max-width: 350px; margin-top: 5px;">
                        </div>
                    </div>
                    <div class="col-md-8">
                        <ul class="list-arrow">
                            <h4 class="display-3">{{ $sarpras->nama }}</h4>
                            <li>
                                <label for="">Jenis</label>
                                <p class="ml-1">{{ $sarpras->jenis }}</p>
                            </li>
                            @if($sarpras->jenis == 'Barang')
                            <li>
                                <label for="">Jumlah</label>
                                <p class="ml-1">{{ $sarpras->jumlah }}</p>
                            </li>
                            @endif
                            <li>
                                <label for="">Fungsi</label>
                                <p class="ml-1">{{ $sarpras->fungsi }}</p>
                            </li>
                            <li>
                                <label for="">Deskripsi</label>
                                <p class="ml-1">{{ $sarpras->deskripsi }}</p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col">
                        <h4 class="card-title mt-4">
                            <u>Keterangan »</u>
                        </h4>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-flush" id="datatable-basic">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Tanggal Pinjam</th>
                                <th>Pengguna</th>
                                <th>Keterangan</th>
                                <th>Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($sarpras_detail as $data)
                            <tr>
                                <th>{{$loop->iteration}}</th>
                                <td>
                                    @if($data->sarpras_masuk_id != '')
                                    {{date('d F Y', strtotime($data->sarpras_masuk->tanggal_masuk))}}
                                    @elseif($data->sarpras_keluar_id != '')
                                    {{date('d F Y', strtotime($data->sarpras_keluar->tanggal_keluar))}}
                                    @endif
                                </td>
                                <td>
                                    {{$data->name}}
                                </td>
                                <td>
                                    @if($data->sarpras_masuk_id != '')

                                    @if($data->sarpras_masuk->keterangan == null && $data->sarpras_masuk->user->roles=='Mahasiswa' && $data->sarpras_masuk->draf->qty == 0)
                                    <span class="badge badge-success">
                                        Dikembalikan
                                    </span>
                                    @elseif($data->sarpras_masuk->user->roles == 'BMN')
                                    <span class="badge badge-info">
                                        {{$data->sarpras_masuk->draf->keterangan}}
                                    </span>
                                    @else
                                    <span class="badge badge-info">
                                        {{$data->sarpras_masuk->keterangan}}
                                    </span>
                                    @endif

                                    @elseif($data->sarpras_keluar_id != '')

                                    @if($data->sarpras_keluar->draf->kondisi == 2 && $data->sarpras_keluar->user->roles =='Mahasiswa' && $data->sarpras_keluar->draf->qty > 0)
                                    <span class="badge badge-danger">
                                        {{$data->sarpras_keluar->draf->keterangan}}
                                    </span>
                                    @elseif($data->sarpras_keluar->draf->pinjam_id == 0 && $data->sarpras_keluar->user->roles =='BMN')
                                    <span class="badge badge-danger">
                                        {{$data->sarpras_keluar->keterangan}}
                                    </span>
                                    @elseif($data->sarpras_keluar->draf->kondisi == 0 && $data->sarpras_keluar->user->roles =='Mahasiswa' && $data->sarpras_keluar->draf->qty > 0)
                                    <span class="badge badge-warning">
                                        Dipinjam
                                    </span>
                                    @else
                                    <span class="badge badge-warning">
                                        Dipinjam
                                    </span>
                                    @endif

                                    @endif
                                </td>
                                @if($data->sarpras_masuk_id != '')
                                @if($data->sarpras_masuk->draf->keterangan != null)
                                <td class="text-info">
                                    {{$data->sarpras_masuk->jumlah}}
                                    <i class="fas fa-arrow-up text-info mr-3"></i>
                                </td>
                                @else
                                <td class="text-success">
                                    {{$data->sarpras_masuk->jumlah}}
                                    <i class="fas fa-arrow-up text-success mr-3"></i>
                                </td>
                                @endif
                                @elseif($data->sarpras_keluar_id != '')

                                @if($data->sarpras_keluar->draf->kondisi == 2)
                                <td class="text-danger">
                                    {{$data->sarpras_keluar->jumlah}}
                                    <i class="fas fa-arrow-down text-warning mr-3"></i>
                                </td>
                                @else
                                <td class="text-warning">
                                    {{$data->sarpras_keluar->jumlah}}
                                    <i class="fas fa-arrow-down text-warning mr-3"></i>
                                </td>
                                @endif

                                @endif
                            </tr>
                            @empty
                            <tr>
                                <td colspan="5" class="text-center">Tidak Ada</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    <br>
                    <input type="hidden" id="sarpras_id" value="{{$sarpras->id}}">
                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('style')
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">
    @endpush