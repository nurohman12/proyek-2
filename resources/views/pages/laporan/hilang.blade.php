@extends('layouts.index')
@push('title', 'Laporan Hilang | Polinema PSDKU Kediri')
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">{{Auth::user()->roles}}</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                            <li class="breadcrumb-item active" aria-current="page">hilang</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">
                                Data Sarpras Rusak / Hilang
                            </h4>
                            <p class="text-sm mb-0">
                                Daftar sarpras yang hilang / rusak dipinjam oleh mahasiswa / dosen
                            </p>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-flush" id="datatable-buttons">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Pengguna</th>
                                <th>Jumlah</th>
                                <th>Sarpras</th>
                                <th>Keterangan</th>
                                <th>Terakhir Diubah</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($hilang as $data)
                            <tr>
                                <th>{{$loop->iteration}}</th>
                                <td>{{$data->user->name}}</td>
                                <td>{{ $data->rusak }}</td>
                                <td>{{$data->sarpras->nama}}</td>
                                <td>{{$data->keterangan}}</td>
                                <td>{{ date('d F Y', strtotime($data->updated_at)) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('style')
    <!-- DataTables -->
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">
    @endpush