@extends('layouts.index')
@push('title', 'Laporan Hilang | Polinema PSDKU Kediri')
@section('content')
<div class="header bg-primary pb-6">
    <div class="container-fluid">
        <div class="header-body">
            <div class="row align-items-center py-4">
                <div class="col-lg-6 col-7">
                    <h6 class="h2 text-white d-inline-block mb-0">{{Auth::user()->roles}}</h6>
                    <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                        <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                            <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                            <li class="breadcrumb-item"><a href="#">Laporan</a></li>
                            <li class="breadcrumb-item active" aria-current="page">dipinjam</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt--6">
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <div class="row">
                        <div class="col">
                            <h4 class="card-title">
                                Data Peminjam
                            </h4>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table class="table table-flush">
                        <thead class="thead-light">
                            <tr>
                                <th>#</th>
                                <th>Pengguna</th>
                                <th>Keperluan</th>
                                <th>Jumlah</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($kembali as $data)

                            @if($data->kembali->status == 0 | $data->kembali->status == 2)
                            <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>
                                    <div class="media align-items-center">
                                        <a href="#" class="avatar rounded-circle mr-3">
                                            <img alt="Image placeholder" src="https://ui-avatars.com/api/?background=random&name={{$data->user->name}}">
                                        </a>
                                        <div class="media-body">
                                            <span class="name mb-0 text-sm">{{$data->user->name}}</span>
                                        </div>
                                    </div>
                                </td>
                                <td>{{$data->keperluan}}</td>
                                <td>{{count($data->draf)}}</td>
                                <td>
                                    <button type="button" class="btn btn-slack btn-icon-only rounded-circle" data-toggle="collapse" href="#collapseExample-{{$data->id}}" role="button" aria-expanded="false" aria-controls="collapseExample">
                                        <span class="btn-inner--icon"><i class="fab fa-slack"></i></span>
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div class="collapse" id="collapseExample-{{$data->id}}">
                                        <div class="card bg-default shandow">
                                            <div class="table-responsive">
                                                <table class="table align-items-center table-dark table-flush">
                                                    <thead class="thead-dark">
                                                        <tr>
                                                            <th>#</th>
                                                            <th>Nama Sarpras</th>
                                                            <th>Jumlah</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach($data->draf as $item)
                                                        <tr>
                                                            <td>{{$loop->iteration}}</td>
                                                            <td>{{$item->sarpras->nama}}</td>
                                                            <td>{{$item->qty}}</td>
                                                        </tr>
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endif

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('style')
    <!-- DataTables -->
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href=" {{url('assets/vendor/datatables.net-select-bs4/css/select.bootstrap4.min.css')}}">
    @endpush