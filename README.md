<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## About Program

Program ini kami buat untuk memenuhi tugas pada mata kuliah proyek 2, kesimpulan pada program ini sebagai berikut:<br>

- **Terdiri dari 4 role**
    - Admin 
    - Koordinator
    - KTU
    - Mahasiswa

- **Fungsi**<br>
    Membantu pihak mahasiswa atau pihak BMN dalam melakukan peminjaman, BMN bisa dengan mudah mengelola barang, mahasiswa bisa mudah melakukan peminjaman dan meminimalisair terjadinya hilangnya draf pinjaman.

- **Anggota**
    - Nur Rohman
    - Ghifari Nur Juwinho
